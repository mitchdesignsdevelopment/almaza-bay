<?php
add_action('wp_ajax_customized_product_add_to_cart', 'mitch_customized_product_add_to_cart');
add_action('wp_ajax_nopriv_customized_product_add_to_cart', 'mitch_customized_product_add_to_cart');
function mitch_customized_product_add_to_cart(){
  $added              = array();
  $parent_id          = intval($_POST['parent_id']);
  $variations_ids     = (array)$_POST['variations_ids'];
  $visit_type         = sanitize_text_field($_POST['visit_type']);
  $visit_branch       = sanitize_text_field($_POST['visit_branch']);
  $visit_home         = sanitize_text_field($_POST['visit_home']);
  $custom_cart_data   = array(
    'custom_cart_data' => array(
      'attributes_keys' => (array)$_POST['attributes_keys'],
      'attributes_vals' => (array)$_POST['attributes_vals'],
      'variations_ids'  => $variations_ids,
      'visit_type'      => $visit_type,
      'visit_branch'    => $visit_branch,
      'visit_home'      => $visit_home
    )
  );
  // mitch_test_vars(array($visit_type, $visit_branch, $visit_home));
  // exit;
  // $product_attributes = array_keys(get_post_meta($parent_id, '_product_attributes', true));
  if(!empty($variations_ids)){
    //$i = 0;
    $total_price = 0;
    foreach($variations_ids as $variation_id){
      $total_price = $total_price + (float)get_post_meta($variation_id, '_price', true);
      // $product_attributes = wc_get_product_variation_attributes($variation_id);
      // $variation_attributes = array();
      // if(!empty($product_attributes)){
      //   foreach($product_attributes as $attribute_key){
      //     if($attribute_key == $attributes_keys[$i]){
      //       $variation_attributes['attribute_'.$attribute_key] = $attributes_vals[$i];
      //     }else{
      //       $variation_attributes['attribute_'.$attribute_key] = 'none';
      //     }
      //   }
      // }
      // echo '<pre>';
      // var_dump($variation_attributes);
      // echo '</pre>';
      //if(!empty($variation_attributes)){
      //  $added[] = WC()->cart->add_to_cart($parent_id, 1, $variation_id, wc_get_product_variation_attributes($variation_id));//14, 1,$variation_attributes
      //}
      //$i++;
    }
  }

  $custom_cart_data['custom_cart_data']['custom_total'] = $total_price;
  $cart_item_key = WC()->cart->add_to_cart($parent_id, 1, $variation_id, wc_get_product_variation_attributes($variation_id), $custom_cart_data); //
  WC()->cart->calculate_totals();

  if($cart_item_key){
    $response = array(
      'status'       => 'success',
      'cart_count'   => WC()->cart->get_cart_contents_count(),
      'cart_content' => mitch_get_cart_content(),
      'redirect_to'  => home_url('cart'),
      'msg'          => 'Added To Cart Successfully.',
    );
  }else{
    $response = array(
      'status' => 'error',
      'msg'    => wc_print_notices(),
    );
  }
  // var_dump($response);
  // exit;
  echo json_encode($response);
  wp_die();
}

add_action('woocommerce_before_calculate_totals', 'mitch_recalculate_cart_item_price');
function mitch_recalculate_cart_item_price($cart_object){
	foreach($cart_object->get_cart() as $hash => $values){
    if(!empty($values['custom_cart_data'])){
      $values['data']->set_price($values['custom_cart_data']['custom_total']);
    }
	}
}

add_action('wp_ajax_simple_product_add_to_cart', 'mitch_simple_product_add_to_cart');
add_action('wp_ajax_nopriv_simple_product_add_to_cart', 'mitch_simple_product_add_to_cart');
function mitch_simple_product_add_to_cart(){
  $product_id      = intval($_POST['product_id']);
  $quantity_number = intval($_POST['quantity_number']);
  $billing_date_time = $_POST['date_time'];
  $billing_unit_number = $_POST['unit_number'];
  $msg = '';
  if(isset($billing_unit_number)):
    $added_to_cart   = WC()->cart->add_to_cart($product_id, $quantity_number,'','',array('billing_date_time' => $billing_date_time, 'billing_unit_number' => $billing_unit_number ));
      else:
    $added_to_cart   = WC()->cart->add_to_cart($product_id, $quantity_number,'','',array('billing_date_time' => $billing_date_time ));
      endif;
  if($added_to_cart){
    $response = array(
      'status'       => 'success',
      'cart_count'   => WC()->cart->get_cart_contents_count(),
      'cart_content' => mitch_get_cart_content(),
      'msg'          => 'Added To Cart Successfully.',
    );
  }else{
    $errors = WC()->session->get('wc_notices', array())['error'];
    $count  = count($errors);
    if(isset($errors) && !empty($errors)){
      foreach($errors as $key => $error_data){
        $msg .= $error_data['notice'];
        if($count > 1){
          $msg = $msg.', ';
        }
      }
    }
    $response = array(
      'status'  => 'error',
      'code'    => 401,
      'msg'     => $msg,
    );
    wc_clear_notices();
  }
  // WC()->cart->set_shipping_total(50);
  // WC()->cart->calculate_totals();
  // WC()->cart->calculate_shipping();
  echo json_encode($response);
  wp_die();
}

add_action('wp_ajax_get_availablility_variable_product', 'mitch_get_availablility_variable_product');
add_action('wp_ajax_nopriv_get_availablility_variable_product', 'mitch_get_availablility_variable_product');
function mitch_get_availablility_variable_product(){
  $attributes      = array();
  $quantity      = array();
  $variation_id    = 0;
  $product_id      = intval($_POST['product_id']);
  $product_form      = intval($_POST['product_form']);
  $selected_items  = $_POST['selected_items'];
  // $quantity_number = intval($_POST['quantity_number']);
  // var_dump($quantity_number);
  // exit;
  $product_obj     = wc_get_product($product_id);
  if(!empty($selected_items)){
    foreach($selected_items as $key => $value){
      foreach($value as $arr_k => $arr_v){
        $attributes[$arr_k] = urldecode($arr_v);
      }
    }
  }
  if(!empty($product_obj->get_available_variations())){
    $time_slots_html = '';
    $time_slots_check = false;
    $count=0;
    // print_r($attributes);
    foreach($product_obj->get_available_variations() as $variation_obj){
      if($attributes['attribute_pa_fields'] || $attributes['attribute_pa_court']):

      // print_r($variation_obj['attributes']);
      // echo $variation_obj['attributes']['attribute_pa_court'].' // '.$attributes['attribute_pa_court'].' ';
      if((isset($variation_obj['attributes']['attribute_pa_fields']) && $variation_obj['attributes']['attribute_pa_fields'] == $attributes['attribute_pa_fields']) || (isset($variation_obj['attributes']['attribute_pa_court']) && $variation_obj['attributes']['attribute_pa_court'] == $attributes['attribute_pa_court'])){
        // echo 'hena1';
        $term = get_term_by('slug',$variation_obj['attributes']['attribute_pa_time-slots'],'pa_time-slots');
        $variation_id_tmp = $variation_obj['variation_id'];
        if($count==0){
          $selected = 'select';
          $active = 'active';
        }
        else{
          $selected = '';
          $active = '';
        }
        $time_slots_html .= ' <div class="single '.$selected.'"> <div class="box">
				<input type="checkbox" class="variation_option new '.$active.'" data-pid="'.$product_id.'" data-variation_id="'.$variation_id_tmp.'" data-name="'.$term->name.'" id="'.$term->term_id.'" name="time-slots-radio" value="'.$term->slug.'" data-value="'.$variation_id.'" data-key="attribute_pa_time-slots" >
				<label for="'.$term->term_id.'">'.$term->name.'</label> </div>
			  </div>';
        $time_slots_check = true;
      }
    endif;
      
      if($variation_obj['attributes'] == $attributes){
        $variation_id = $variation_obj['variation_id'];
      }
      $count++;
    }
  }
  if(!empty($variation_id) || $time_slots_check){
    // if($product_form==80 || $product_form==78):
    //   $billing_date_time = $_POST['date_time']; 
    //   global $wpdb;
    //   $sql = $wpdb->get_var("SELECT date_time from wp_product_date_time WHERE product_id = ".$variation_id." limit 0,100");
    //   $sql = (strpos($sql, ',') !== false)? explode(",",$sql): $sql;
    //   if(is_array($sql)){
    //     foreach($sql as $single_id){
    //       $date_time[$single_id] = $single_id;
    //     }
    //   }
    //   else{
    //     $date_time[$sql] = $sql;
    //   }
    //   $date_time_unique = array_unique($date_time);
    //   $test_str = '';
    //   // if(in_array($billing_date_time,$date_time_unique)):
    //     if(!isset($date_time_unique[$billing_date_time])):
    //     $test_str = $variation_id;
    //   endif;
    // endif;
    if(!empty($variation_id)){
    $variation = new WC_Product_Variation($variation_id);
    $quantity[] = $variation->get_stock_quantity();

    $response = array(
      'status'       => 'success',
      'quantity'   => array_sum($quantity),
      'price'   => number_format($variation->get_price(),2,'.',',').' EGP',
      'msg'          => 'Added To Cart Successfully.',
      'time_slots_html' => $time_slots_html,
    );
  }else{
    $response = array(
      'status'       => 'success',
      'msg'          => 'Added To Cart Successfully.',
      'time_slots_html' => $time_slots_html,
    );
  }
  }
  else{
    $response = array(
      'status' => 'error',
      'msg'    => wc_print_notices()
    );
  }
  echo json_encode($response);
  wp_die();
}

add_action('wp_ajax_variable_product_add_to_cart', 'mitch_variable_product_add_to_cart');
add_action('wp_ajax_nopriv_variable_product_add_to_cart', 'mitch_variable_product_add_to_cart');
function mitch_variable_product_add_to_cart(){
  $attributes      = array();
  $attributes_others      = array();
  $variation_id    = array();
  $product_id      = intval($_POST['product_id']);
  $selected_items  = $_POST['selected_items'];
  $quantity_number = intval($_POST['quantity_number']);
  $billing_date_time = $_POST['date_time'];
  $billing_unit_number = $_POST['unit_number'];
  // var_dump($selected_items);
  // exit;
  $product_obj     = wc_get_product($product_id);
  if(!empty($selected_items)){
    $key_v=0;
    $key_p=0;
  // print_r($selected_items);
    $const_attr_key = array_keys($selected_items[0]);
    $const_attr_value = array_values($selected_items[0]);
    // print_r($const_attr_key);
    foreach($selected_items as $key => $value){
      // print_r($value);
      // echo $key;
      foreach($value as $arr_k => $arr_v){
        // print_r($selected_items[0]);
        if($key_v!==0 && isset($selected_items[$key_p][$arr_k]) && $product_id !== 1589 && $product_id !== 1586 && $product_id !== 1585 && $product_id !== 213 && $product_id !== 1580 && $product_id !== 2864 && $product_id !== 2901){
          $attributes_others[] = [$const_attr_key[0]=>$const_attr_value[0],$arr_k=>urldecode($arr_v)];
        }
        else{
          // $attributes[] = [$arr_k=>urldecode($arr_v)];
          $attributes[$arr_k] = urldecode($arr_v);
        }
        //urldecode($arr_v);
        $key_v++;
      }
      $key_p++;
    }
  }

  // $new_attributes = array();
  // $new_key = 0;
  // foreach($attributes as $value){
  //   array_push($attributes[0],$value);
  //  array_push($new_attributes,$attributes[0]);
  //   // $new_attributes[$new_key] .= $value;
    
  //   $new_key++;
  // }
  // if(count($attributes_others)>1){
  // print_r($attributes_others);
  //     $attributes_others  = array_merge($attributes_others[0],$attributes_others[1]);
  //   // $attributes_others  = array_merge($attributes_others[0],$attributes_others[1]);
  // }
  if(!empty($product_obj->get_available_variations())){
    foreach($product_obj->get_available_variations() as $variation_obj){
      // print_r($variation_obj['attributes']);
      if(!empty($attributes_others)){
      // print_r($attributes_others);
      // echo '///<br>';
      // print_r($variation_obj['attributes']);
      // echo '/**<br>';
        foreach($attributes_others as $attribute){
          if($variation_obj['attributes'] == $attribute){
            // echo 'jims';
            // $variation_id[] = $variation_obj['variation_id'];
            $variation_id[] = $variation_obj['variation_id'];
          }
        }
      }else{
    //   $attributes = array_map(function($value) {
    //     return $value === "" ? NULL : $value;
    //  }, $attributes); // array_map should walk through $array
    //  foreach ($attributes as $i => $value) {
    //   if (empty($value)){  $attributes[$i] = NULL;
    //   }
    //   }
    //   foreach ($variation_obj['attributes'] as $i => $value) {
    //     if (empty($value)){  $variation_obj['attributes'][$i] = NULL;
    //     }
    //     }
    //   print_r($attributes);
    //   print_r($variation_obj['attributes']);
        if($variation_obj['attributes'] == $attributes){
          $variation_id[] = $variation_obj['variation_id'];
        }
      }
    }
  }
  // print_r($variation_id);
  // $variation_id[0] = 2397;
  if(!empty($billing_date_time)){
    $cart_id        = WC()->cart->generate_cart_id($product_id);
    $cart_item_id   = WC()->cart->find_product_in_cart($cart_id);
    $product_time = array();
    if(woo_in_cart($product_id)):
      foreach( WC()->cart->get_cart() as $cart_item ){
        // print_r($cart_item);
          if(isset($cart_item['billing_date_time'])):
            $product_time[$cart_item['billing_date_time']] = $cart_item['billing_date_time']; // For version 3 or more
          elseif(isset($cart_item['variation']['billing_date_time'])):
            $product_time[$cart_item['variation']['billing_date_time']] = $cart_item['variation']['billing_date_time']; // For version 3 or more
          endif;
    }
    endif;
    // echo $billing_date_time;
    // print_r($producst_time);
  }
  // print_r($variation_id);
  if(!empty($variation_id)){
    if(!empty($billing_date_time)){
      $billing_date_time_arr = explode(',',$billing_date_time);
      $billing_date_time_arr = array_filter($billing_date_time_arr);
      $in_cart_check = false;
      foreach($billing_date_time_arr as $billing_date_time){
      if(isset($product_time[$billing_date_time])):
        $in_cart_check = true;
        break;
        endif;
      }
      if(!$in_cart_check){
        $count=0;
        foreach($variation_id as $variation){
          $test = wc_get_product_variation_attributes($variation);
          if(isset($test['attribute_pa_private-sessions-adults']) && empty($test['attribute_pa_private-sessions-adults'])):
          $test['attribute_pa_private-sessions-adults'] = '1-on-1';
          endif;
          $added_to_cart   = WC()->cart->add_to_cart($product_id, '1', $variation, $test,array('billing_date_time' => $billing_date_time_arr[$count] ));
          // print_r(wc_get_product_variation_attributes($variation));
          // echo 'jims'.$variation;
          // print_r($added_to_cart);
          $count++;
        }
      }
    }
    else{
      // print_r(wc_get_product_variation_attributes($variation_id[0]));
      foreach($variation_id as $variation){
        $test = wc_get_product_variation_attributes($variation);
        if(isset($test['attribute_pa_week-reservation']) && empty($test['attribute_pa_week-reservation'])):
        $test['attribute_pa_week-reservation'] = 'week-1-july-3rd-july-7th';
        endif;
        $added_to_cart   = WC()->cart->add_to_cart($product_id, '1', $variation,$test);
      }
    }
  }
  else{
    if(!empty($billing_date_time)){
      if(!isset($product_time[$billing_date_time])):
        if(isset($billing_unit_number)):
      $added_to_cart   = WC()->cart->add_to_cart($product_id, '1','','',array('billing_date_time' => $billing_date_time, 'billing_unit_number' => $billing_unit_number ));
        else:
      $added_to_cart   = WC()->cart->add_to_cart($product_id, '1','','',array('billing_date_time' => $billing_date_time ));
        endif;
      endif;
    }
  }
  if($added_to_cart){
    $response = array(
      'status'       => 'success',
      'cart_count'   => WC()->cart->get_cart_contents_count(),
      'cart_content' => mitch_get_cart_content(),
      'msg'          => 'Added To Cart Successfully.'
    );
  }else{
    $response = array(
      'status' => 'error',
      'msg'    => wc_print_notices()
    );
  }
  echo json_encode($response);
  wp_die();
}
function woo_in_cart($product_id) {
  global $woocommerce;

  foreach($woocommerce->cart->get_cart() as $key => $val ) {
      $_product = $val['data'];

      if($product_id == $_product->id ) {
          return true;
      }
  }

  return false;
}
add_action('wp_ajax_cart_remove_item', 'mitch_cart_remove_item');
add_action('wp_ajax_nopriv_cart_remove_item', 'mitch_cart_remove_item');
function mitch_cart_remove_item(){
  $product_id = intval($_POST['product_id']);
  if(!empty($product_id)){
    foreach(WC()->cart->get_cart() as $cart_item_key => $cart_item){
      if($cart_item['product_id'] == $product_id){
        WC()->cart->remove_cart_item($cart_item_key);
      }
    }
  }else{
    WC()->cart->remove_cart_item(sanitize_text_field($_POST['cart_item_key']));
  }
  echo json_encode(array(
    'success'      => true,
    'cart_count'   => WC()->cart->get_cart_contents_count(),
    'cart_total'   => WC()->cart->cart_contents_total,
    'cart_content' => mitch_get_cart_content()
    )
  );
  wp_die();
}

add_action('wp_ajax_update_cart_items', 'mitch_update_cart_items');
add_action('wp_ajax_nopriv_update_cart_items', 'mitch_update_cart_items');
function mitch_update_cart_items(){
  $item_total      = 0;
  $post_cart_key   = sanitize_text_field($_POST['cart_item_key']);
  $quantity_number = intval($_POST['quantity_number']);
  // var_dump($_POST['quantity_number']);
  // exit;
  if(!empty($quantity_number)){
    WC()->cart->set_quantity($post_cart_key, $quantity_number);
    foreach(WC()->cart->get_cart() as $cart_item_key => $cart_item){
      if($cart_item_key == $post_cart_key){
        $item_total = $cart_item['line_subtotal'];
        break;
      }
    }
    echo json_encode(array(
      'success'      => true,
      'cart_count'   => WC()->cart->get_cart_contents_count(),
      'cart_total'   => WC()->cart->cart_contents_total,
      'cart_content' => mitch_get_cart_content(),
      'item_total'   => $item_total)
    );
  }
  wp_die();
}

add_action('wp_ajax_mitch_apply_coupon', 'mitch_apply_coupon');
add_action('wp_ajax_nopriv_mitch_apply_coupon', 'mitch_apply_coupon');
function mitch_apply_coupon(){
  global $fixed_string;
  $cart_discount_div = '';
  $msg = '';
  $coupon_code       = sanitize_text_field($_POST['coupon_code']);
  $coupon_id         = wc_get_coupon_id_by_code($coupon_code);
  //$coupon_data = new WC_Coupon($coupon_code);
  if(!empty($coupon_id)){
    WC()->cart->apply_coupon($coupon_code);
    WC()->cart->calculate_totals();
    $errors = WC()->session->get('wc_notices', array())['error'];
    $count  = count($errors);
    if(isset($errors) && !empty($errors)){
      foreach($errors as $key => $error_data){
        $error_trans = $fixed_string[$error_data['notice']];
        if(!empty($error_trans)){
          $msg .= $error_trans;
        }else{
          $msg .= $error_data['notice'];
        }
        if($count > 1){
          $msg = $msg.', ';
        }
      }
      $response = array(
        'status' => 'error',
        'code'   => 401,
        'msg'    => $msg
      );
      wc_clear_notices();
    }else{
      // echo '<pre>';
      // var_dump();
      // echo '</pre>';
      if($_POST['coupon_from'] == 'checkout'){
        global $theme_settings;
        $shipping_data     = mitch_get_shipping_data()['shipping_methods'][$theme_settings['default_shipping_method']];
        $shipping_cost     = $shipping_data['cost'];
        $coupon_discount   = WC()->cart->coupon_discount_totals[sanitize_title($coupon_code)];
        $cart_discount_div = '<div class="list_pay discount">
             <p>'.$fixed_string['checkout_page_discount'].'</p>
             <p><span id="cart_discount">- '.$coupon_discount.'</span> '.$theme_settings['current_currency'].'</p>
         </div>';
      }else{
        $shipping_cost = 0;
      }
      $response = array('status' => 'success', 'cart_total' => number_format(WC()->cart->cart_contents_total + $shipping_cost,2,'.',',').' EGP', 'cart_discount_div' => $cart_discount_div);
    }
  }else{
    $response = array('status' => 'error', 'code' => 401, 'msg' => 'Invalid discount coupon!');
  }
  echo json_encode($response);
  wp_die();
}

add_action('wp_ajax_mitch_remove_coupon', 'mitch_remove_coupon');
add_action('wp_ajax_nopriv_mitch_remove_coupon', 'mitch_remove_coupon');
function mitch_remove_coupon(){
  $coupon_code   = sanitize_text_field($_POST['coupon_code']);
  if($_POST['coupon_from'] == 'checkout'){
    global $theme_settings;
    $shipping_data = mitch_get_shipping_data()['shipping_methods'][$theme_settings['default_shipping_method']];
    $shipping_cost = $shipping_data['cost'];
  }else{
    $shipping_cost = 0;
  }
  WC()->cart->remove_coupon($coupon_code);
  WC()->cart->calculate_totals();
  $response        = array(
    'status'       => 'success',
    'cart_total'   => number_format(WC()->cart->cart_contents_total + $shipping_cost,2).' EGP',
    'cart_count'   => WC()->cart->get_cart_contents_count(),
    'cart_content' => mitch_get_cart_content(),
  );
  echo json_encode($response);
  wp_die();
}

add_action('wp_ajax_mitch_bought_together_products', 'mitch_bought_together_products');
add_action('wp_ajax_nopriv_mitch_bought_together_products', 'mitch_bought_together_products');
function mitch_bought_together_products(){
  $form_data = $_POST['form_data'];
  $products_ids = (array)$form_data['products_ids'];
  foreach($products_ids as $product_id){
    $added_to_cart   = WC()->cart->add_to_cart($product_id, 1);
  }
  if($added_to_cart){
    $response = array(
      'status'       => 'success',
      'cart_count'   => WC()->cart->get_cart_contents_count(),
      'cart_content' => mitch_get_cart_content(),
      'msg'          => 'اضافة المنتجات الي سلة المشتريات',
    );
  }else{
    $errors = WC()->session->get('wc_notices', array())['error'];
    $count  = count($errors);
    if(isset($errors) && !empty($errors)){
      foreach($errors as $key => $error_data){
        $msg .= $error_data['notice'];
        if($count > 1){
          $msg = $msg.', ';
        }
      }
    }
    $response = array(
      'status'  => 'error',
      'code'    => 401,
      'msg'     => $msg,
    );
    wc_clear_notices();
  }
  echo json_encode($response);
  // if(!empty($products_ids)){
  //   echo '<pre>';
  //   var_dump($products_ids);
  //   echo '</pre>';
  // }
  wp_die();
}


function mitch_get_cart_content(){
  global $fixed_string, $theme_settings;
  $items      = WC()->cart->get_cart();
  $cart_total = WC()->cart->cart_contents_total;
  if(!empty(WC()->cart->applied_coupons)){
    $coupon_code    = WC()->cart->applied_coupons[0];
    $active         = 'active';
    $dis_form_style = 'display:block;';
    $dis_abtn_style = 'display:none;';
    $dis_rbtn_style = 'display:block;';
  }else{
    $coupon_code    = '';
    $active         = '';
    $dis_form_style = '';
    $dis_abtn_style = 'display:block;';
    $dis_rbtn_style = 'display:none;';
  }
  if(WC()->cart->get_cart_contents_count() == 0){
    $mini_class = 'empty_min_cart';
  }else{
    $mini_class = 'min_cart';
  }
  $subtotal = number_format(WC()->cart->subtotal,2,'.',',');
  $cart_content = '
  <div id="mini_cart" class="'.$mini_class.'">
    <div class="top">
      <div class="cart_info">
        <div class="title_min_cart">
          <img class="cart_hover" src="'.$theme_settings['theme_url'].'/assets/img/icons/cart_min.png" alt="">
          <h2>Your Cart 
              <span class="count qun">'.WC()->cart->get_cart_contents_count().'</span> 
          </h2>
        </div>
        <div class="info_min_cart">
          <h4>TOTAL
            <p class="price cart_total" id="cart_total">'.$subtotal.'</span> '.$theme_settings['current_currency'].'</p>
          </h4>
        </div>
      </div>
      <div class="cart_action">
          <a class="open_checkout" href="'.home_url('checkout').'">
            <button type="button">Checkout</button>
          </a>
          <a class="open_cart" href="'.home_url('cart').'">
            <button type="button">View Cart</button>
          </a>
      </div>
    </div>
    <div class="all_item">';

      if(!empty($items)){
        $count_items = 0;
        foreach($items as $item => $values){
          $products_ids[]    = $values['product_id'];

          $cart_item_meta = $values['billing_date_time'];
          $cart_product_data = mitch_get_short_product_data($values['product_id']);
          $order             = $cart_items_count - $count_items;
          $cart_content .= '
          <div id="mini_cart_'.$item.'" class="single_item" style="order: '.$order.';">
              <div class="sec_item">
                  <div class="img">
                      <img height="100" src="'.$cart_product_data['product_image'].'" alt="'.$cart_product_data['product_title'].'">
                  </div>
                  <div class="info">
                      <div class="info_top">
                        <div class="text">
                            <a href="'.$cart_product_data['product_url'].'">
                            <h4>'.$cart_product_data['product_title'].'</h4></a>';
                            // print_r($values);
                            if(!empty($values['billing_date_time'])){
                              $cart_content .= '<ul>';
                              $cart_content .= '<li>'.$values['billing_date_time'].'</li>';
                              $cart_content .= '</ul>';
                            }
                            if(!empty($values['billing_unit_number'])){
                              $cart_content .= '<ul>';
                              $cart_content .= '<li>'.$values['billing_unit_number'].'</li>';
                              $cart_content .= '</ul>';
                            }
                            if(!empty($values['custom_cart_data'])){
                              $cart_content .= '<ul>';
                              foreach($values['custom_cart_data']['attributes_vals'] as $attr_val){
                                $cart_content .= '<li>'.mitch_get_product_attribute_name($attr_val).'</li>';
                              }
                              $cart_content .= '</ul>';
                            }elseif(!empty($values['variation'])){
                              $cart_content .= '<ul>';
                              foreach($values['variation'] as $key => $value){
                                $tax = str_replace('attribute_','',$key);
                                if(isset($values['variation']['attribute_pa_tennis-academy-groups']) && $values['variation']['attribute_pa_tennis-academy-groups']!=='private-sessions-adults-only' && $value=='1-on-1'){
                                }
                                else{
                                $cart_content .= '<li>'.get_term_by('slug',$value,$tax)->name.'</li>';
                                }
                              }
                              $cart_content .= '</ul>';
                            }
                            $line_subtotal = number_format($values['line_subtotal'],2,'.',',');
                            if(!isset($values['billing_date_time'])):
                            $cart_content .= '
                        </div>
                        <p class="total_price"><span id="line_subtotal_'.$item.'">'.$line_subtotal.'</span> '.$theme_settings['current_currency'].'</p>
                      </div>
                      <div class="info_bottom">
                        <a class="remove_min_cart href="javascript:void(0);" onclick="cart_remove_item(\''.$item.'\', \'\', \'mini_cart\');">Remove</a>
                      </div>
                  </div>
              </div>
          </div>
          ';
                            else:
                              $cart_content .= '
                              </div>
                              <p class="total_price"><span id="line_subtotal_'.$item.'">'.$line_subtotal.'</span> '.$theme_settings['current_currency'].'</p>
                            </div>
                            <div class="info_bottom">
                              <a class="remove_min_cart href="javascript:void(0);" onclick="cart_remove_item(\''.$item.'\', \'\', \'mini_cart\');">Remove</a>
                            </div>
                        </div>
                    </div>
                </div>
                ';
                            endif;
                            $count_items++;
        }
        //"'.$item.'", ''
      }else{
        $cart_content .= '
        <div class="section_emty">
            <img src="'.$theme_settings['theme_url'].'/assets/img/icons/cart_min.png" alt="">
            <p>There are currently no products to be added to the shopping bag</p>
            <a class="shop" href="'.home_url('shop').'">
              <button type="button">Shop Now</button>
            </a>
        </div>';
      }
      $cart_content .= '
    </div>

    <div class="bottom">
      <div class="bottom_info">
      <div class="coupon_cart">
      <h4 class="open-coupon "'.$active.'">Do you have a promocode?</h4>
      <div class="discount-form" style="'.$dis_form_style.'">
          <button class="close-coupon"><i class="material-icons">close</i></button>
          <div class="coupon">
            <label for="coupon_code">Enter Promocode</label>
            <input type="text" name="coupon_code" class="input-text" id="coupon_code" value=""'.$coupon_code.'" placeholder="Code" />
            <button style="'.$dis_abtn_style.'" id="apply_coupon" type="submit" class="button btn">
              Apply
            </button>
            <button style="'.$dis_rbtn_style.'" id="remove_coupon" type="submit" class="button btn">
              Remove Coupon
            </button>
            <input type="hidden" name="lang" id="lang" value="">
          </div>
      </div>
  </div>
        <div class="info_min_cart">
          <h4>TOTAL
            <p class="price cart_total">'.$subtotal.'</span> '.$theme_settings['current_currency'].'</p>
          </h4>
        </div>
      </div>
      <a class="open_checkout" href="'.home_url('checkout').'">
        <button type="button">Checkout</button>
      </a>
  </div>
  </div>
  ';
  return $cart_content;
}

function mitch_repeat_order(){
  if($_SERVER['REQUEST_METHOD'] == 'POST'){
    if($_POST['action'] == 'repeat_order'){
      // $new_order_id = mitch_create_order_from(intval($_POST['order_id']));
      // if($new_order_id){
      //   wp_redirect(home_url('my-account/orders-list/?order_id='.$new_order_id.''));
      //   exit;
      // }
      // var_dump($_POST);
      // exit;
      if($_POST['repeat_action'] == 'no_items'){
        WC()->cart->empty_cart();
      }
      $custom_cart_data = array();
      $r_order_obj      = wc_get_order(intval($_POST['order_id']));
      foreach($r_order_obj->get_items() as $cart_item_key => $values){
        if(!empty($values['custom_cart_data'])){
  				$items_data = $values['custom_cart_data'];
  				if(!empty($items_data['visit_type'])){
            $custom_cart_data['visit_type'] = $items_data['visit_type'];
  				}
  				if(!empty($items_data['visit_branch'])){
            $custom_cart_data['visit_branch'] = $items_data['visit_branch'];
  				}
  				if(!empty($items_data['visit_home'])){
            $custom_cart_data['visit_home'] = $items_data['visit_home'];
  				}
          if(!empty($items_data['attributes_keys'])){
            $custom_cart_data['attributes_keys'] = $items_data['attributes_keys'];
  				}
          if(!empty($items_data['attributes_vals'])){
            $custom_cart_data['attributes_vals'] = $items_data['attributes_vals'];
  				}
          $total_price = 0;
          // echo '<pre>';
          // var_dump($values);
          // echo '</pre>';
          // exit;
          if(!empty($items_data['variations_ids'])){
            foreach($items_data['variations_ids'] as $variation_id){
              $total_price = $total_price + (float)get_post_meta($variation_id, '_price', true);
            }
          }
          $custom_cart_data['custom_total'] = number_format($total_price,2,'.',',');
          $custom_cart_data_arr             = array('custom_cart_data' => $custom_cart_data);
          // echo '<pre>';
          // var_dump($custom_cart_data_arr);
          // echo '</pre>';
          // exit;
          $added_to_cart = WC()->cart->add_to_cart($values['product_id'], 1, $variation_id, wc_get_product_variation_attributes($variation_id), $custom_cart_data_arr);
  			}else{
          if(!empty($values['variation_id'])){
            $product_id = $values['variation_id'];
          }else{
            $product_id = $values['product_id'];
          }
          $added_to_cart   = WC()->cart->add_to_cart($product_id, $values['quantity']);
        }
      }
      if($added_to_cart){
        wp_redirect(home_url('cart'));
        exit;
      }
    }
    wp_redirect(home_url('my-account/orders-list/?order_id='.intval($_POST['order_id']).'&response=error'));
    exit;
  }
}

//update cart prices to rate price
// add_action( 'woocommerce_before_calculate_totals', 'add_custom_item_price', 10 );
// function add_custom_item_price($cart_object){
//   foreach($cart_object->get_cart() as $item_values){
//     ## Set the new item price in cart
//     // $item_values['data']->set_price(mitch_get_product_price_after_rate($item_values['data']->price));
//     $item_values['data']->set_price($item_values['data']->price);
//   }
// }

add_action( 'woocommerce_add_to_cart_validation', 'validate_name_on_tshirt_field' );
function validate_name_on_tshirt_field( $passed ) {
    $error_notice = array(); // Initializing
    $passed = true;
    
    return $passed;
}