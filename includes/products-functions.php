<?php
function mitch_get_product_id_by_slug($product_slug){
  global $wpdb;
  return $wpdb->get_row("SELECT ID FROM wp_posts WHERE post_name = '$product_slug'")->ID;
}

function mitch_validate_single_product($product_obj){
  if(empty($product_obj->get_price()) || $product_obj->get_status() != 'publish'){
    wp_redirect(home_url());
    exit;
  }
}

function mitch_validate_customized_product($product_data){
  if(
    empty($product_data['main_data']->get_price())        ||
    $product_data['main_data']->get_type() != 'variable'  ||
    $product_data['main_data']->get_status() != 'publish' ||
    $product_data['extra_data']['product_customized'] == false
  ){
    wp_redirect(home_url());
    exit;
    // global $wp_query;
    // $wp_query->set_404();
    // status_header(404);
    // get_template_part(404);
    //exit();
  }
}

function mitch_get_product_data($product_id){
  $main_data = wc_get_product($product_id);
  return array(
    'main_data'  => $main_data,
    'product_form' => get_the_terms($product_id,'product_form')[0],
    'images'     => mitch_get_product_images($main_data->get_image_id(), $main_data->get_gallery_image_ids())
  );
}

function mitch_get_product_images($featured_image_id, $gallery_images_ids){
  // var_dump(wp_get_attachment_image($featured_image_id, array(100, 100)));
  // exit;
  $product_full_images   = array();
  $product_thum_images   = array();
  // $product_full_images[] = wp_get_attachment_image($featured_image_id, array(600, 600));//wp_get_attachment_image_src($featured_image_id, 'full');
  // $product_thum_images[] = wp_get_attachment_image($featured_image_id, array(100, 100));//wp_get_attachment_image_src($featured_image_id, 'thumbnail');
  if(!empty($gallery_images_ids)){
    foreach($gallery_images_ids as $gallery_image_id){
      $product_full_images[] = wp_get_attachment_image_src($gallery_image_id, 'full')[0];//wp_get_attachment_image($gallery_image_id, array(600, 600));
      $product_thum_images[] = wp_get_attachment_image_src($gallery_image_id, 'thumbnail')[0];//wp_get_attachment_image($gallery_image_id, array(100, 100));
    }
  }
  return array(
    'full'  => $product_full_images,
    'thumb' => $product_thum_images
  );
}

function mitch_get_product_attribute_name($attribute_slug){
  global $wpdb;
  return $wpdb->get_row("SELECT name FROM wp_terms WHERE slug = '$attribute_slug'")->name;
}

function mitch_get_product_attribute_name_by_id($attribute_id){
  global $wpdb;
  return $wpdb->get_row("SELECT name FROM wp_terms WHERE term_id = $attribute_id")->name;
}

function mitch_get_customized_product_variations_steps($product_variations, $attr_key, $step_number_name, $step_number_value){
  if(!empty($product_variations)){
    foreach($product_variations as $variation_obj){
      if(!empty($variation_obj['attributes']["attribute_{$attr_key}"]) && $variation_obj['attributes']["attribute_{$attr_key}"] != 'none'){
        $variation_img = wp_get_attachment_image_src($variation_obj['image_id'], 'medium')[0]; //full
        ?>
        <div class="single_box"
        data-variation-id="<?php echo $variation_obj['variation_id'];?>"
        data-variation-price="<?php echo $variation_obj['display_price'];?>"
        data-variation-attribute-key="<?php echo $attr_key;?>"
        data-variation-attribute-val="<?php echo $variation_obj['attributes']["attribute_{$attr_key}"];?>"
        data-variation-step="<?php echo $step_number_name;?>"
        data-variation-img="<?php echo $variation_img;?>"
        data-next="<?php echo mitch_get_number_name($step_number_value + 1);?>"
        >
          <img src="<?php echo $variation_img;?>" alt="">
          <h4><?php echo mitch_get_product_attribute_name($variation_obj['attributes']["attribute_{$attr_key}"]); //urldecode();?></h4>
        </div>
        <?php
      }
    }
  }
}

function mitch_get_customized_product_variations_steps_old($product_childrens, $attr_key){
  if(!empty($product_childrens)){
    foreach($product_childrens as $variation_id){
      $variation_attr_title = get_post_meta($variation_id, 'attribute_'.$attr_key.'', true);
      if(!empty($variation_attr_title)){
        //$variation_obj = wc_get_product($variation_id);
        // mitch_test_vars(array($variation_obj));
        ?>
        <div class="single_box" data-variation-id="<?php echo $variation_id;?>" data-variation-price="<?php echo $variation_obj->get_price();?>">
          <img src="<?php echo wp_get_attachment_image_src($variation_obj->get_image_id(), 'full')[0];?>" alt="">
          <h4><?php echo mitch_get_product_attribute_name($variation_attr_title); //urldecode();?></h4>
        </div>
        <?php
      }
    }
  }
}

function mitch_get_short_product_data($product_id){
  return array(
    'product_id'           => $product_id,
    'product_title'        => get_the_title($product_id),
    'product_form'         => get_the_terms($product_id,'product_form')[0]->name,
	'product_excerpt'      => get_post_meta($product_id, 'product_excerpt', true),
    'product_price'        => get_post_meta($product_id, '_price', true),
    'product_brand'        => get_post_meta($product_id, 'product_extra_data_product_brand', true),
    'product_image'        => wp_get_attachment_image_src(get_post_thumbnail_id($product_id), 'full')[0],
    'product_url'          => get_permalink($product_id),
  );
}

function mitch_get_products_by_category($category_id,$page_type){
  if($page_type=="filter"){
    $number_of_posts = 1;
  }
  else{
    $number_of_posts = -1;
  }
  if($page_type=="product_form"){
	  $tax = 'product_form';
  }
  $products_ids = get_posts(array(
    'numberposts' => $number_of_posts,
    'fields'      => 'ids',
    'post_type'   => 'product',
    'post_status' => 'publish',
    'tax_query'   => array(
      array(
        'taxonomy' => ($tax)?$tax:'product_cat',
        'field'    => 'term_id',
        'terms'    => $category_id, /*category name*/
        'operator' => 'IN',
      )
    ),
  ));
  shuffle($products_ids);
  return $products_ids;
}

function mitch_get_products_list(){
  return get_posts(array(
    'numberposts' => -1,
    'fields'      => 'ids',
    'post_type'   => 'product',
    'post_status' => 'publish',
  ));
}

add_action('wp_ajax_mitch_make_product_review', 'mitch_make_product_review');
add_action('wp_ajax_nopriv_mitch_make_product_review', 'mitch_make_product_review');
function mitch_make_product_review(){
  $response       = array();
	$post_form_data = $_POST['form_data'];
	parse_str($post_form_data, $form_data);
  $product_id   = intval($form_data['product_id']);
  $fname         = sanitize_text_field($form_data['fname']);
  $lname         = sanitize_text_field($form_data['lname']);
  $email        = sanitize_text_field($form_data['email']);
  $comment      = sanitize_text_field($form_data['comment']);
  $rating       = sanitize_text_field($form_data['rating']);
  $attended = sanitize_text_field($form_data['attended']);
  $hotel_id = intval($form_data['num']);
  if($hotel_id){
	  $product_id = $hotel_id;
  }
  $comment_data = array(
    'comment_post_ID'      => $product_id,
    'comment_author'       => $fname.' '.$lname,
    'comment_author_email' => $email,
    'comment_content'      => $comment,
    'comment_date'         => current_time('Y-m-d H:i:s'),
    'user_id'              => get_current_user_id(),
    'comment_approved'     => 0,
    'comment_type'         => 'review',
  );
  $comment_id = wp_insert_comment($comment_data);
  if($comment_id){
	if($rating){
		update_comment_meta($comment_id, 'rating', $rating);
	}
	if($attended){
		update_comment_meta($comment_id, 'attended', $attended);
	}
    $response = array('status' => 'success', 'comment_data' => $comment_data, 'msg' => 'Thank you for your feedback');
  }else{
    $response = array('status' => 'error');
  }
  echo json_encode($response);
  wp_die();
}
add_action('wp_ajax_mitch_make_phase_review', 'mitch_make_phase_review');
add_action('wp_ajax_nopriv_mitch_make_phase_review', 'mitch_make_phase_review');
function mitch_make_phase_review(){
	global $wpdb;
  $response       = array();
	$post_form_data = $_POST['form_data'];
	parse_str($post_form_data, $form_data);
  $product_id   = intval($_POST['product_id']);
  $variation_id   = intval($_POST['variation_id']);
  $role         = sanitize_text_field($form_data['role']);
  $fname         = sanitize_text_field($form_data['fname']);
  $lname         = sanitize_text_field($form_data['lname']);
  $email        = sanitize_text_field($form_data['email']);
  $number      = $form_data['number'];
  $date_time       = $form_data['date'].' '.get_term_by('slug',$form_data['time-slots-radio'],'pa_time-slots')->name;
  if($product_id){
	if($variation_id){
	$count = $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM wp_product_date_time WHERE product_id = %d", $variation_id));
	if($count==1){
	}else{
		$wpdb->insert('wp_product_date_time', array('product_id' => $variation_id,'product_parent' => $product_id));
	}
	$date_time_meta = $date_time;
	$date_time_arr = array();
	$date_time_arr[] = $date_time_meta;

	$sql = $wpdb->get_var("SELECT date_time from wp_product_date_time WHERE product_id = ".$variation_id."");
	if(!empty($sql)){
	$sql = explode(",",$sql);
	$date_time_arr = array_merge($date_time_arr, $sql);
	}
	$items_arr_unique = array_unique($date_time_arr);
	$items_arr_separated = implode(',',$items_arr_unique);
	$wpdb->query( $wpdb->prepare("UPDATE wp_product_date_time
				SET date_time = %s 
				WHERE product_id = %s",$items_arr_separated, $variation_id)
				);
	}
	$form = Caldera_Forms_Forms::get_form('CF626aa14cb7094');
	//Basic entry information
 	$wpdb->insert('wp_cf_form_entries', array('form_id' => $form['ID'],'datestamp' => current_time('mysql'),'status' => 'pending'));
	$lastid = $wpdb->insert_id;
	// $entryDetials = new Caldera_Forms_Entry_Entry();
	// $entryDetials->form_id = $form['ID'];
	// $entryDetials->datestamp = current_time('mysql');
	// $entryDetials->status = 'pending';
	//Create entry object
	// $entry = new Caldera_Forms_Entry(
	// 	$form,
	// 	false,
	// 	$entryDetials
	// 			);
	// 			// print_r($entry);

$field = Caldera_Forms_Field_Util::get_field('fld_8768091', $form); // first name
$fieldEntryValue = new Caldera_Forms_Entry_Field();
$fieldEntryValue->field_id = $field['ID'];
$fieldEntryValue->slug = $field['slug'];
$fieldEntryValue->value = $fname;
$wpdb->insert('wp_cf_form_entry_values', array('entry_id' => $lastid,'field_id' => $field['ID'],'slug' => $field['slug'],'value' => $fname));
// $entry->add_field($fieldEntryValue);

$field = Caldera_Forms_Field_Util::get_field('fld_9970286', $form); // last name
$fieldEntryValue2 = new Caldera_Forms_Entry_Field();
$fieldEntryValue2->field_id = $field['ID'];
$fieldEntryValue2->slug = $field['slug'];
$fieldEntryValue2->value = $lname;
$wpdb->insert('wp_cf_form_entry_values', array('entry_id' => $lastid,'field_id' => $field['ID'],'slug' => $field['slug'],'value' => $lname));
// $entry->add_field($fieldEntryValue2);

$field = Caldera_Forms_Field_Util::get_field('fld_6009157', $form); // Email
$fieldEntryValue3 = new Caldera_Forms_Entry_Field();
$fieldEntryValue3->field_id = $field['ID'];
$fieldEntryValue3->slug = $field['slug'];
$fieldEntryValue3->value = $email;
$wpdb->insert('wp_cf_form_entry_values', array('entry_id' => $lastid,'field_id' => $field['ID'],'slug' => $field['slug'],'value' => $email));

// $entry->add_field($fieldEntryValue3);

$field = Caldera_Forms_Field_Util::get_field('fld_3716815', $form); // Phone Number
$fieldEntryValue4 = new Caldera_Forms_Entry_Field();
$fieldEntryValue4->field_id = $field['ID'];
$fieldEntryValue4->slug = $field['slug'];
$fieldEntryValue4->value = $number;
$wpdb->insert('wp_cf_form_entry_values', array('entry_id' => $lastid,'field_id' => $field['ID'],'slug' => $field['slug'],'value' => $number));
// $entry->add_field($fieldEntryValue4);

$field = Caldera_Forms_Field_Util::get_field('fld_2758980', $form); // Role
$fieldEntryValue5 = new Caldera_Forms_Entry_Field();
$fieldEntryValue5->field_id = $field['ID'];
$fieldEntryValue5->slug = $field['slug'];
$fieldEntryValue5->value = $role;
$wpdb->insert('wp_cf_form_entry_values', array('entry_id' => $lastid,'field_id' => $field['ID'],'slug' => $field['slug'],'value' => $role));

// $entry->add_field($fieldEntryValue5);
$field = Caldera_Forms_Field_Util::get_field('fld_7683514', $form); // Date Time
$fieldEntryValue6 = new Caldera_Forms_Entry_Field();
$fieldEntryValue6->field_id = $field['ID'];
$fieldEntryValue6->slug = $field['slug'];
$fieldEntryValue6->value = $date_time;
$wpdb->insert('wp_cf_form_entry_values', array('entry_id' => $lastid,'field_id' => $field['ID'],'slug' => $field['slug'],'value' => $date_time));

// $entry->add_field($fieldEntryValue6);

$field = Caldera_Forms_Field_Util::get_field('fld_1294401', $form); // product id
$fieldEntryValue6 = new Caldera_Forms_Entry_Field();
$fieldEntryValue6->field_id = $field['ID'];
$fieldEntryValue6->slug = $field['slug'];
$fieldEntryValue6->value = $product_id;
$wpdb->insert('wp_cf_form_entry_values', array('entry_id' => $lastid,'field_id' => $field['ID'],'slug' => $field['slug'],'value' => $product_id));
$wpdb->query( $wpdb->prepare("UPDATE wp_cf_form_entries
SET status = %s 
WHERE id = %s",'active', $lastid)
);
// $entry->add_field($fieldEntryValue6);
//Save entry in database.
// $entryId = $entry->save();


//Make entry active
// $entry->update_status( 'active' );

    $response = array('status' => 'success', 'msg' => 'Thank you, We will contact you soon');
  }else{
    $response = array('status' => 'error');
  }
  echo json_encode($response);
  wp_die();
}


function mitch_get_bought_together_products($pids, $exclude_pids = 0){
  $sub_exsql2   = '';
	$all_products = array();
	$pids_count   = count($pids);
	$pid          = implode(',',$pids);
	global $wpdb,$table_prefix;
	if ($pids_count >1 ||  ($pids_count == 1 && !$all_products = wp_cache_get( 'bought_together_'.$pid, 'ah_bought_together' )) ) {
		$subsql = "SELECT oim.order_item_id FROM ".$table_prefix."woocommerce_order_itemmeta oim where oim.meta_key='_product_id' and oim.meta_value in ($pid)";
		$sql = "SELECT oi.order_id from  ".$table_prefix."woocommerce_order_items oi where oi.order_item_id in ($subsql) limit 100";
		$all_orders = $wpdb->get_col($sql);
		if($all_orders){
			$all_orders_str = implode(',',$all_orders);
			$subsql2 = "select oi.order_item_id FROM ".$table_prefix."woocommerce_order_items oi where oi.order_id in ($all_orders_str) and oi.order_item_type='line_item'";
			if($exclude_pids){
				$sub_exsql2 = " and oim.meta_value not in ($pid)";
			}
			$sql2 = "select oim.meta_value as product_id,count(oim.meta_value) as total_count from ".$table_prefix."woocommerce_order_itemmeta oim where oim.meta_key='_product_id' $sub_exsql2 and oim.order_item_id in ($subsql2) group by oim.meta_value order by total_count desc limit 15";
			$all_products = $wpdb->get_col($sql2);
			if($pids_count==1){
				wp_cache_add( 'bought_together_'.$pid, $all_products, 'ah_bought_together' );
			}
		}
	}
  if(!empty($all_products)){
    shuffle($all_products);
  }
	return $all_products;
}

function mitch_get_reviews_stars($rating_avg){
  echo '<div class="starssss">';
  if($rating_avg == 0){
    ?>
    <span class="material-icons">star_outline</span>
    <span class="material-icons">star_outline</span>
    <span class="material-icons">star_outline</span>
    <span class="material-icons">star_outline</span>
    <span class="material-icons">star_outline</span>
    <?php
  }elseif($rating_avg >= 1 && $rating_avg < 2){
    ?>
    <span class="material-icons">star_rate</span>
    <span class="material-icons">star_outline</span>
    <span class="material-icons">star_outline</span>
    <span class="material-icons">star_outline</span>
    <span class="material-icons">star_outline</span>
    <?php
  }elseif($rating_avg >= 2 && $rating_avg < 3){
    ?>
    <span class="material-icons">star_rate</span>
    <span class="material-icons">star_rate</span>
    <span class="material-icons">star_outline</span>
    <span class="material-icons">star_outline</span>
    <span class="material-icons">star_outline</span>
    <?php
  }elseif($rating_avg >= 3 && $rating_avg < 4){
    ?>
    <span class="material-icons">star_rate</span>
    <span class="material-icons">star_rate</span>
    <span class="material-icons">star_rate</span>
    <span class="material-icons">star_outline</span>
    <span class="material-icons">star_outline</span>
    <?php
  }elseif($rating_avg >= 4 && $rating_avg < 5){
    ?>
    <span class="material-icons">star_rate</span>
    <span class="material-icons">star_rate</span>
    <span class="material-icons">star_rate</span>
    <span class="material-icons">star_rate</span>
    <span class="material-icons">star_outline</span>
    <?php
  }elseif($rating_avg == 5){
    ?>
    <span class="material-icons">star_rate</span>
    <span class="material-icons">star_rate</span>
    <span class="material-icons">star_rate</span>
    <span class="material-icons">star_rate</span>
    <span class="material-icons">star_rate</span>
    <?php
  }
  echo '</div>';
}

function mitch_remove_decimal_from_rating($rating_avg){
  if(in_array($rating_avg, array('1.00', '2.00', '3.00', '4.00', '5.00'))){
    $rating_avg = intval($rating_avg);
  }
  return $rating_avg;
}

function mitch_get_best_selling_products_ids($limit){
  $args = array(
    'post_type'      => 'product',
    'meta_key'       => 'total_sales',
    'orderby'        => 'meta_value_num',
    'fields'         => 'ids',
    'posts_per_page' => $limit,
  );
  return get_posts($args);
}

function mitch_update_product_total_sales($product_id, $new_quantity){
  $old_total_sales = (int)get_post_meta($product_id, 'total_sales', true);
  $new_total_sales = $old_total_sales + $new_quantity;
  update_post_meta($product_id, 'total_sales', $new_total_sales);
}

function mitch_get_new_arrival_products_ids($limit){
  $start_date = array(
    'year'  => date("Y", strtotime("first day of previous month")),
    'month' => date("n", strtotime("first day of previous month")),
    'day'   => date("j", strtotime("first day of previous month"))
  );
  $end_date = array(
    'year'  => date("Y", strtotime("last day of this month")),
    'month' => date("n", strtotime("last day of this month")),
    'day'   => date("j", strtotime("last day of this month"))
  );
  $args = array(
    'post_type'  => 'product',
    'date_query' => array(
      array(
        'after'     => $start_date,
        'before'    => $end_date,
        'inclusive' => true,
      ),
    ),
    'fields'         => 'ids',
    'posts_per_page' => $limit,
  );
  return get_posts($args);
}

// function mitch_get_product_price_after_rate($product_price){
//   global $wpdb, $theme_settings;
//   $current_curr = $theme_settings['current_currency'];
//   $default_curr = get_field('default_currency', 'options');
//   if($current_curr == $default_curr){
//     return $product_price;
//   }else{
//     $def_rate  = $wpdb->get_row("SELECT rate FROM wp_mitch_currencies_rates WHERE code = '$default_curr'")->rate;
//     $curr_rate = $wpdb->get_row("SELECT rate FROM wp_mitch_currencies_rates WHERE code = '$current_curr'")->rate;
//     $calc_rate = $curr_rate / $def_rate;
//     return round($product_price * $calc_rate, 2);
//   }
// }

// load more products
add_action( 'wp_ajax_nopriv_get_products_ajax', 'get_products_ajax' );
add_action( 'wp_ajax_get_products_ajax', 'get_products_ajax' );
function get_products_ajax(){
	global $language;
	$action = sanitize_text_field($_POST['fn_action']);
	$count    = intval($_POST['count']);
	$page     = intval($_POST['page']);
	$offset   = ($page) * $count;
	$order    = sanitize_text_field($_POST['order']);
	$type = sanitize_text_field($_POST['type']);
	$slug = sanitize_text_field($_POST['slug']);
	$cat = sanitize_text_field($_POST['cat']);
	$search = sanitize_text_field($_POST['search']);
	$ids = $_POST['ids'];
	$lang = $_POST['lang'];
	$ajaxArgs = array();
	// if($lang == "en"){
	// 	$language = "en";
	// }else{
	// 	$language = "";
	// }
	if($type=="sale"){
		// $ajaxQuery = get_on_sale_products_ids($count,$page,$slug,78);
		// if($ajaxQuery):
		// 	foreach ($ajaxQuery as $key => $product) {
		// 		get_template_part( 'template-parts/content', 'on_sale_product_widget',$product ); 
		// 	}
		// endif;
	}
	else{
	$ajaxArgs = array(
		"post_type" => 'product',
		'post_status' => 'publish',
    "fields" => 'ids',
		"suppress_filters" => false,
		'tax_query'=>array(
			'relation' => 'AND',
			array(
				'taxonomy'         => 'product_visibility',
				'terms'            => array( 'exclude-from-catalog', 'exclude-from-search' ),
				'field'            => 'name',
				'operator'         => 'Not IN',
				'include_children' => false,
			),
		),
		// 'meta_query'=>array(
		// 	'relation' => 'AND',
		// 	array(
		// 		'key' => '_stock_status',
		// 		'value' => 'instock',
		// 		'compare' => '=',
		// 	),
		// ),
		);
	if($search !=""){
		$ajaxArgs["s"] = $search;
	}
	if($action == "loadmore"){
		$ajaxArgs["offset"] = $offset;
		$ajaxArgs["posts_per_page"] = $count;
	}else{
		$ajaxArgs["posts_per_page"] = $offset;
	}
	if($type == 'product_cat'){
		$ajaxArgs['product_cat'] = $_POST['slug'];
	}else if($type == 'new'){
		$ajaxArgs['date_query'] = array(
			array(
				'after' => date('Y-m-d', strtotime('-60 days'))
			)
		);
		$ajaxArgs['meta_query'] = array(
			array(
				'key' => '_stock_status',
				'value' => 'instock',
				'compare' => '=',
			));
	}elseif ($type == 'type') {
		$ajaxArgs['tax_query'][]= array(
			'taxonomy'=>'type',
			'field' => 'term_id',
			'terms' => array($_POST['slug'])
			);
	}elseif($type == 'shop'){
	}elseif($type == 'sale'){
		// $ajaxArgs['meta_query'] = WC()->query->get_meta_query();
		// $ajaxArgs['post__in'] = get_on_sale_products_ids_only($count,$page,'',78);
	}else if($type == 'total-sales'){
	}else{
		$ajaxArgs['tax_query'][] = array(
			'taxonomy' => $type,
			'field' => 'term_id',
			'terms' => array($slug),
		);
	}
	if($order == 'price'){
		$orderby = array(
            'meta_value_num' => 'asc',
            'date' => 'desc',
        );
		$ajaxArgs['meta_key'] = '_price';
		$ajaxArgs['orderby'] = $orderby;
		$ajaxArgs['order'] = 'asc';
	}else if($order == 'price-desc'){
		$orderby = array(
            'meta_value_num' => 'desc',
            'date' => 'desc',
        );
		$ajaxArgs['meta_key'] = '_price';
		$ajaxArgs['orderby'] = $orderby;
		$ajaxArgs['order'] = 'desc';
	}else if($order =='featured'){
		$orderby = array(
			'meta_value_num' => 'desc',
			'date' => 'desc',
		);
		$ajaxArgs['meta_key'] = 'featured';
		$ajaxArgs['orderby'] = $orderby;
		$ajaxArgs['order'] = 'desc';
	}
	else if($order =='date'){
		// $orderby = array(
		// 	'meta_value_num' => 'desc',
		// 	'date' => 'desc',
		// );
		// $ajaxArgs['meta_key'] = 'featured';
		$ajaxArgs['orderby'] ='date';
		$ajaxArgs['order'] = 'desc';
	}else if($order == 'popularity'){
	    $orderby = array(
            'meta_value_num' => 'desc',
            'date' => 'desc',
        );
        $ajaxArgs['meta_key'] = 'total_sales'; 
        $ajaxArgs['orderby'] = $orderby; 
        $ajaxArgs['order'] = 'desc';
	}else if($order == 'stock'){
		$orderby = array(
            'meta_value_num' => 'desc',
            'date' => 'desc',
        );
	    $ajaxArgs['meta_key'] = '_stock_status'; 
	    $ajaxArgs['orderby'] = 'meta_value'; 
	    $ajaxArgs['order'] = 'ASC'; 
	}else{
		$orderby = array(
            'meta_value_num' => 'desc',
            'date' => 'desc',
        );
        $ajaxArgs['meta_key'] = 'total_sales'; 
        $ajaxArgs['orderby'] = $orderby; 
        $ajaxArgs['order'] = 'desc';
	}
	if($_POST['min_price']!=''){
		$min_price = intval($_POST['min_price']);
		$max_price = intval($_POST['max_price']);
		if($max_price!=0){
			$ajaxArgs['meta_query'][] = array(
				'relation' => 'AND',
				 array(
					'key' => '_price',
					'value' => array($min_price,$max_price),
					'compare' => 'between',
					'type' => 'numeric'
				),
			);
		}else{
			$ajaxArgs['meta_query'][] = array(
			'relation' => 'AND',
            array(
                'key' => '_price',
                'value' => $min_price,
				'compare' => '>=',
				'type' => 'numeric'
            )
        );
		}
	}
	if($_POST['brand'] && count($_POST['brand'])>0){
		$brand = $_POST['brand'];
		$ajaxArgs['tax_query'][]= array(
			'taxonomy'=>'brand',
			'field' => 'slug',
			'terms' => $brand,
			);
	}
	if($_POST['cats'] && count($_POST['cats'])>0){
		$cats = $_POST['cats'];
		$ajaxArgs['tax_query'][]= array(
			'taxonomy'=>'product_cat',
			'field' => 'slug',
			'terms' => $cats,
			);
	}
	if($_POST['label'] && count($_POST['label'])>0){
		$labels = $_POST['label'];
		$ajaxArgs['tax_query'][]= array(
			'taxonomy'=>'label',
			'field' => 'slug',
			'terms' => $labels,
		);
	}
	if($_POST['collections'] && count($_POST['collections'])>0){
		$collections = $_POST['collections'];
		$ajaxArgs['tax_query'][]= array(
			'taxonomy'=>'collections',
			'field' => 'term_id',
			'terms' => $collections,
		);
	}
	$products_ids =  get_posts($ajaxArgs);
  if(!empty($products_ids)){
    foreach($products_ids as $product_id){
      $product_data = mitch_get_short_product_data($product_id);
      // print_r($product_data);
      wc_get_template( '../theme-parts/product-widget.php', $product_data );
      // include '../theme-parts/product-widget.php';
    }
  }
	}
wp_die();
}

// load more products
add_action( 'wp_ajax_nopriv_get_products_ajax_count', 'get_products_ajax_count' );
add_action( 'wp_ajax_get_products_ajax_count', 'get_products_ajax_count' );

function get_products_ajax_count(){
	global $language;
	$action = sanitize_text_field($_POST['fn_action']);
	$count    = intval($_POST['count']);
	$page     = intval($_POST['page']);
	$offset   = ($page) * $count;
	$order    = sanitize_text_field($_POST['order']);
	$type = sanitize_text_field($_POST['type']);
	$slug = sanitize_text_field($_POST['slug']);
	$cat = sanitize_text_field($_POST['cat']);
	$search = sanitize_text_field($_POST['search']);
	$ids = $_POST['ids'];
	$lang = $_POST['lang'];
	$ajaxArgs = array();
	// if($lang == "en"){
	// 	$language = "en";
	// }else{
	// 	$language = "";
	// }
	if($type=="sale"){
		// $ajaxQuery = get_on_sale_products_ids(-1,0,$slug,78);
	}
	elseif($type=="less_100"){
		// $ajaxQuery = get_products_less_100_ids(-1,0,$slug,'');
	}
	else{
	$ajaxArgs = array(
		"post_type" => 'product',
		"posts_per_page"=> -1 ,
    "fields" => 'ids',
		'post_status' => 'publish',
		"suppress_filters" => false,
		"offset" =>$offset,
		'tax_query'=>array(
			'relation' => 'AND',
			array(
				'taxonomy'         => 'product_visibility',
				'terms'            => array( 'exclude-from-catalog', 'exclude-from-search' ),
				'field'            => 'name',
				'operator'         => 'Not IN',
				'include_children' => false,
			),

		),
		// 'meta_query'=>array(
		// 	'relation' => 'AND',
		// 	array(
		// 		'key' => '_stock_status',
		// 		'value' => 'instock',
		// 		'compare' => '=',
		// 	),
		// ),
	);
	if($search !=""){
		$ajaxArgs["s"] = $search;
	}
	// if($action == "loadmore"){
	// 	$ajaxArgs["offset"] = $offset;
	// 	$ajaxArgs["posts_per_page"] = $count;
	// }else{
	// 	$ajaxArgs["posts_per_page"] = $offset;
	// }
	if($type == 'product_cat'){
		$ajaxArgs['product_cat'] = $_POST['slug'];
	}
	else if($type == 'new'){
		$ajaxArgs['date_query'] = array(
			array(
				'after' => date('Y-m-d', strtotime('-30 days'))
			)
		);
	}
	elseif ($type == 'type') {
		$ajaxArgs['tax_query'][]= array(
			'taxonomy'=>'type',
			'field' => 'term_id',
			'terms' => array($_POST['slug'])
			);
	}
	elseif($type == 'shop'){
	}
	elseif($type == 'sale'){
		// $ajaxArgs['post__in'] = get_on_sale_products_ids_only(0,24,'',78);
	}
	else if($type == 'products-list'){
		$ajaxArgs['post__in'] = $ids;
	}
	else if($type == 'total-sales'){
	}
	elseif($type == 'less_100'){
		// $ajaxArgs['post__in'] = get_products_less_100_ids_only(0,24,'','');

	}else{
		$ajaxArgs['tax_query'][] = array(
				'taxonomy' => $type,
				'field' => 'term_id',
				'terms' => array($slug),
		);
	}
	if($order == 'price'){
		$orderby = array(
            'meta_value_num' => 'asc',
            'date' => 'desc',
        );
		$ajaxArgs['meta_key'] = '_price';
		$ajaxArgs['orderby'] = $orderby;
		$ajaxArgs['order'] = 'asc';
	}else if($order == 'price-desc'){
		$orderby = array(
            'meta_value_num' => 'desc',
            'date' => 'desc',
        );
		$ajaxArgs['meta_key'] = '_price';
		$ajaxArgs['orderby'] = $orderby;
		$ajaxArgs['order'] = 'desc';
	}else if($order =='featured'){
		$orderby = array(
			'meta_value_num' => 'desc',
			'date' => 'desc',
		);
		$ajaxArgs['meta_key'] = 'featured';
		$ajaxArgs['orderby'] = $orderby;
		$ajaxArgs['order'] = 'desc';
	}
	else if($order =='date'){
		// $orderby = array(
		// 	'meta_value_num' => 'desc',
		// 	'date' => 'desc',
		// );
		// $ajaxArgs['meta_key'] = 'featured';
		$ajaxArgs['orderby'] ='date';
		$ajaxArgs['order'] = 'desc';
	}else if($order == 'popularity'){
		
	    $orderby = array(
            'meta_value_num' => 'desc',
            'date' => 'desc',
        );
        $ajaxArgs['meta_key'] = 'total_sales'; 
        $ajaxArgs['orderby'] = $orderby; 
        $ajaxArgs['order'] = 'desc';
	}else if($order == 'stock'){
		$orderby = array(
            'meta_value_num' => 'desc',
            'date' => 'desc',
        );
	    $ajaxArgs['meta_key'] = '_stock_status'; 
	    $ajaxArgs['orderby'] = $orderby; 
	    $ajaxArgs['order'] = 'ASC'; 
	}else{
		$orderby = array(
            'meta_value_num' => 'desc',
            'date' => 'desc',
        );

        $ajaxArgs['meta_key'] = 'total_sales'; 
        $ajaxArgs['orderby'] = $orderby; 
        $ajaxArgs['order'] = 'desc';
	}
	if($_POST['min_price']!=''){
		$min_price = intval($_POST['min_price']);
		$max_price = intval($_POST['max_price']);
		if($max_price!=0){
			$ajaxArgs['meta_query'][] = array(
				'relation' => 'AND',
				 array(
					'key' => '_price',
					'value' => array($min_price,$max_price),
					'compare' => 'between',
					'type' => 'numeric'
				),
			);
		}else{
			$ajaxArgs['meta_query'][] = array(
			'relation' => 'AND',
            array(
                'key' => '_price',
                'value' => $min_price,
				'compare' => '>=',
				'type' => 'numeric'
            )
        );
		}
	}
	if($_POST['brand'] && count($_POST['brand'])>0){
		$brand = $_POST['brand'];
		$ajaxArgs['tax_query'][]= array(
			'taxonomy'=>'brand',
			'field' => 'slug',
			'terms' => $brand,
			);
	}	
	if($_POST['cats'] && count($_POST['cats'])>0){
		$cats = $_POST['cats'];
		$ajaxArgs['tax_query'][]= array(
			'taxonomy'=>'product_cat',
			'field' => 'slug',
			'terms' => $cats,
			);
	}	
	if($_POST['label'] && count($_POST['label'])>0){
		$labels = $_POST['label'];
		$ajaxArgs['tax_query'][]= array(
			'taxonomy'=>'label',
			'field' => 'slug',
			'terms' => $labels,
		);
	}
	if($_POST['collections'] && count($_POST['collections'])>0){
		$collections = $_POST['collections'];
		$ajaxArgs['tax_query'][]= array(
			'taxonomy'=>'collections',
			'field' => 'term_id',
			'terms' => $collections,
		);
	}
	$ajaxQuery =  get_posts($ajaxArgs);
	}
		if($ajaxQuery){
			echo count($ajaxQuery);
		}else{
			echo "0";
		}
	wp_die();
}
add_action( 'wp_ajax_nopriv_get_available_time_slots', 'get_available_time_slots' );
add_action( 'wp_ajax_get_available_time_slots', 'get_available_time_slots' );
function get_available_time_slots(){
	global $wpdb;
	$date = $_POST['date'];
	$product_id = $_POST['product_id'];
	$variations_arr = $_POST['variations_arr'];
	$product_form = $_POST['product_form'];
	if($variations_arr): $count=0;
	foreach($variations_arr as $variation):
	$date_time_var = $_POST['date'].' '.$variation['name'];
	$variation_id = $variation['data_variation_id'];
	$sql = $wpdb->get_var("SELECT date_time from wp_product_date_time WHERE product_id = ".$variation_id." limit 0,100");
	$sql = (strpos($sql, ',') !== false)? explode(",",$sql): $sql;
	if(is_array($sql)){
	  foreach($sql as $single_id){
		$date_time[$single_id] = $single_id;
	  }
	}
	else{
	  $date_time[$sql] = $sql;
	}

	$date_time_unique = array_unique($date_time);
	$today_new_format = date("n-j-Y", strtotime("+2 hour"));
	// echo $date_time_var;
	// print_r($date_time_unique);
	if(!isset($date_time_unique[$date_time_var])):
				?>
			   <div class="single <?php echo($count==0)?'select':'';?>">
			   <div class="box">
				<input type="checkbox" class="variation_option new <?php echo($count==0)?'active':'';?>" data-pid="<?php echo $product_id;?>" data-variation_id="<?php echo $variation_id;?>" data-name="<?php echo $variation['name'];?>" id="<?php echo $variation['data_id'];?>" name="time-slots-radio" value="<?php echo $variation['data_value'];?>" data-value="<?php echo $variation['data_value'];?>" data-key="<?php echo $variation['data_key'];?>" >
				<label for="<?php echo $variation['data_id'];?>"><?php echo $variation['name']; //echo $vari;?></label>
				</div>
			  </div>
				<?php
				$count++;
	else:
	?>
	<div class="single not_available">
	<div class="box">
	 <input type="radio" class="variation_option new" data-variation_id="<?php echo $variation_id;?>" data-name="<?php echo $variation['name'];?>" id="<?php echo $variation['data_id'];?>" name="time-slots-radio" value="<?php echo $variation['data_value'];?>" data-value="<?php echo $variation['data_value'];?>" data-key="<?php echo $variation['data_key'];?>" disabled >
	 <label for="<?php echo $variation['data_id'];?>"><?php echo $variation['name']; //echo $vari;?></label>
	</div>
   </div>
	 <?php
	endif;
		  ?>
	<?php
	$count++;
	endforeach;
	endif;
	wp_die();
}
add_action( 'wp_ajax_nopriv_get_available_date', 'get_available_date' );
add_action( 'wp_ajax_get_available_date', 'get_available_date' );
function get_available_date(){
	global $wpdb;
	$date = $_POST['date'];
	$product_id = $_POST['product_id'];
	// $variations_arr = $_POST['variations_arr'];
	if($product_id): $count=0;
	$sql = $wpdb->get_var("SELECT date_time from wp_product_date_time WHERE product_id = ".$product_id." limit 0,100");
	$sql = (strpos($sql, ',') !== false)? explode(",",$sql): $sql;
	if(is_array($sql)){
	  foreach($sql as $single_id){
		$date_time[] = $single_id;
	  }
	}
	else{
	  $date_time[] = $sql;
	}

	$date_time_unique = array_unique($date_time);
	// $today_new_format = date("n-j-Y", strtotime("+2 hour"));
	// print_r($date_time_unique);
	if(!isset($date_time_unique[$date])):
			return 1;
	else:
		return 0;
	endif;
		  ?>
	<?php
 endif;
	wp_die();
}