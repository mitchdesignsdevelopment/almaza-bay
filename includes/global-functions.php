<?php
$theme_settings = mitch_theme_settings();

require_once $theme_settings['theme_abs_url'].'languages/'.$theme_settings['current_lang'].'.php';
function mitch_get_number_name($number){
  $numbers_names_list = array('zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten');
  return $numbers_names_list[$number];
}

function mitch_theme_settings(){
  $currency_symbol = get_woocommerce_currency(); //_symbol
  // if($currency_symbol == 'ر.ق'){
  //   $currency_symbol = 'QAR';
  // }
  if($_GET['lang'] && $_GET['lang']=='ar'){
	$lang = 'ar';
}
else{
	$lang = 'en';
}
  return array(
    'site_url'                => site_url(),
    'theme_url'               => get_template_directory_uri(),
    'theme_abs_url'           => preg_replace('/wp-content.*$/','',__DIR__).'wp-content/themes/almaza/',
    'theme_logo'              => get_field('logo', 'options'),
    'theme_favicon'           => get_field('fav_icon', 'options'),
    'theme_favicon_black'     => get_field('fav_icon_black', 'options'),
    'current_lang'            => $lang,
    'current_currency'        => $currency_symbol,
    'default_country_code'    => 'EG',
    'default_country_name'    => 'Egypt',
    'default_shipping_method' => 'filters_by_cities_shipping_method',
  );
}
function mitch_test_vars($vars){
  echo '<h2 style="direction:ltr;background: #222;color: #fff;border: 2px solid #fff;padding: 5px;margin: 5px;">Data Debug:</h2>';
  if(is_array($vars)){
    foreach($vars as $var){
      echo '<pre style="direction:ltr;background: #222;color: #fff;border: 2px solid #fff;padding: 5px;margin: 5px;">';
      var_dump($var);
      echo '</pre>';
    }
  }else{
    echo '<pre style="direction:ltr;background: #222;color: #fff;border: 2px solid #fff;padding: 5px;margin: 5px;">';
    var_dump($vars);
    echo '</pre>';
  }
}

function mitch_get_active_page_class($page_name){
  if($page_name == basename(get_permalink())){
    return 'active';
  }
}

function mitch_validate_logged_in(){
  if(!is_user_logged_in()){
    wp_redirect(home_url());
    exit;
  }
}
add_action( 'wp_ajax_nopriv_custom_search', 'custom_search' );
add_action( 'wp_ajax_custom_search', 'custom_search' );

function custom_search(){
	$lang = $_POST['lang'];
	$s = $_POST['s'];
	global $language;
	if($lang == '_en') {
		$language = 'en';
	}
	$args = array(
		'posts_per_page'  => 5,
		'post_type'       => 'product',
		's'    => $s,
		'post_status'     => 'publish',
		'meta_key' => '_stock_status',
		'orderby' => 'meta_value',
		'order' => 'ASC',
	);
	$new_prods1 =  new WP_Query($args);
	$restOfProd = 5 - (int)$new_prods1->post_count;
	if($restOfProd>0){
		$args = array(
			'posts_per_page'  => $restOfProd,
			'post_type'       => 'product',
			'post_status'     => 'publish',
			'meta_query' => array(
				'relation'=>'OR',
				array(
					'key' => '_sku',
					'value' => $s,
					'compare' => 'LIKE'
				),
			),
			'meta_key' => '_stock_status',
			'orderby' => 'meta_value',
			'order' => 'ASC',
		);
		$new_prods2 = new WP_Query($args);
		$new_prods1->post_count = $new_prods1->post_count + $new_prods2->post_count;
		$restOfProd = 5 - (int)$new_prods1->post_count;
		$new_prods1->posts = array_unique(array_merge($new_prods1->posts,$new_prods2->posts),SORT_REGULAR);
		if($restOfProd>0){
			$args = array(
				'posts_per_page'  => $restOfProd,
				'post_type'       => 'product',
				'post_status'     => 'publish',
				'meta_query' => array(
					'relation'=>'OR',
						array(
						'key' => 'post_content',
						'value' => $s, 
						'compare' => 'LIKE'
					),
				),
				'meta_key' => '_stock_status',
				'orderby' => 'meta_value',
				'order' => 'ASC',
			);
			$new_prods3 = new WP_Query($args);
			$new_prods1->post_count = $new_prods1->post_count + $new_prods2->post_count + $new_prods3->post_count;
			$restOfProd = 5 - (int)$new_prods1->post_count;
			$new_prods1->posts = array_unique(array_merge($new_prods1->posts,$new_prods2->posts,$new_prods3->posts),SORT_REGULAR);
			if($restOfProd>0){
				$args = array(
					'posts_per_page'  => $restOfProd,
					'post_type'       => 'product',
					'post_status'     => 'publish',
					'tax_query' => array(
						'relation'=>'OR',
							array(
							'taxonomy' => 'brand',
							'field' => 'name',
							'terms' => array($s),
							'compare' => 'LIKE',
							),
						),
					'meta_key' => '_stock_status',
					'orderby' => 'meta_value',
					'order' => 'ASC',
				);
				$new_prods4 = new WP_Query($args);
				$new_prods1->post_count = $new_prods1->post_count + $new_prods2->post_count + $new_prods3->post_count + $new_prods4->post_count;
				$restOfProd = 5 - (int)$new_prods1->post_count;
				$new_prods1->posts = array_unique(array_merge($new_prods1->posts,$new_prods2->posts,$new_prods3->posts,$new_prods4->posts),SORT_REGULAR);
				if($restOfProd>0){
					$args = array(
						'posts_per_page'  => $restOfProd,
						'post_type'       => 'product',
						'post_status'     => 'publish',
						'tax_query' => array(
								array(
								'taxonomy' => 'product_tag',
								'field' => 'name',
								'terms' => array($s),
								'compare' => 'LIKE',
								),
							),
						'meta_key' => '_stock_status',
						'orderby' => 'meta_value',
						'order' => 'ASC',
					);
					$new_prods5 = new WP_Query($args);
					$new_prods1->post_count = $new_prods1->post_count + $new_prods2->post_count + $new_prods3->post_count + $new_prods4->post_count + $new_prods5->post_count;
					$restOfProd = 5 - (int)$new_prods1->post_count;
					$new_prods1->posts = array_unique(array_merge($new_prods1->posts,$new_prods2->posts,$new_prods3->posts,$new_prods4->posts,$new_prods5->posts),SORT_REGULAR);
						if($restOfProd>0){
							$term_args = array('taxonomy' => 'brand');
								$terms = get_terms( $term_args );
								$term_ids = array();
								if ( ! empty( $terms ) && ! is_wp_error( $terms ) )
								{
									foreach( $terms as $term ) 
									{
										$val = get_term_meta( $term->term_id, 'en_title', true);
										if( strcasecmp($val, $s) == 0 ) {
											$term_ids[] = $term->term_id;
										}
									}
								}
							$args = array(
								'posts_per_page'  => $restOfProd,
								'post_type'       => 'product',
								'post_status'     => 'publish',
								'tax_query' => array(
									array(
									'taxonomy' => 'brand',
									'field'    => 'ID',
									'terms'    => $term_ids,
									'operator' => 'IN',
									),
								),
								'meta_key' => '_stock_status',
								'orderby' => 'meta_value',
								'order' => 'ASC',
							);
							$new_prods7 = new WP_Query($args);
							$new_prods1->post_count = $new_prods1->post_count + $new_prods2->post_count + $new_prods3->post_count + $new_prods4->post_count + $new_prods5->post_count + $new_prods7->post_count;
							$restOfProd = 10 - (int)$new_prods1->post_count;
							$new_prods1->posts = array_unique(array_merge($new_prods1->posts,$new_prods2->posts,$new_prods3->posts,$new_prods4->posts,$new_prods5->posts,$new_prods7->posts),SORT_REGULAR);
						}
				}
			}
		}
	}
	if($new_prods1->have_posts()):
		while ($new_prods1->have_posts()) :
			$new_prods1->the_post(); 
			if(!empty(get_the_ID())){
          $product_data = mitch_get_short_product_data(get_the_ID());
          wc_get_template('../almaza/theme-parts/product-widget.php',$product_data);
          // include '../almaza/theme-parts/product-widget.php';
			}
		endwhile; wp_reset_postdata(); 
	else: global $language;?>
    <sec class="emty_filter">
		 <p > <?php echo ($language=="en")? 'No results found' : 'لا توجد نتائج' ; ?></p>
    </sec>
	<?php endif;
wp_die();
}

function enable_page_excerpt() {
	add_post_type_support('product', array('excerpt'));
  }
  add_action('init', 'enable_page_excerpt');

  add_filter( 'cf_entry_limiter_entry_limit', function( $limit, $entry, $field, $form  ){
    //IMPORTANT: Change fld_9970286 to your field's ID
    if( 'fld_4827280' === $field[ 'ID' ] ){

        //IMPORTANT: Use the field values (not labels) in these conditionals

        //change max entries for frisbee to 10
        if( '9:30 AM' === $entry ){
            $limit = 1;
        }
		elseif ( '10:30-AM' === $entry ){
            $limit = 1;
        }


    }

    return $limit;
}, 10, 4 );
