<?php require_once 'header.php';?>
<?php
$single_product_data = mitch_get_product_data(get_the_id());
$page_content = get_field('single_product',$single_product_data['main_data']->get_id());
mitch_validate_single_product($single_product_data['main_data']);
// mitch_test_vars(array());
$class_name = '';
if($single_product_data['product_form']->term_id==81){
  $class_name = "single_item_camp";
}
elseif($single_product_data['product_form']->term_id==77){
  $class_name = "single_retreat";
}
elseif($single_product_data['product_form']->term_id==78){
  $class_name = "single_sport";
}
elseif($single_product_data['product_form']->term_id==80){
  $class_name = "single_book_beach";
  if(have_rows('dates_excluded',$single_product_data['main_data']->get_id())):
    global $excluded_dates;
    $excluded_dates = array();
    while(have_rows('dates_excluded',$single_product_data['main_data']->get_id())):
      the_row();
      $excluded_dates[]= get_sub_field('date');
    endwhile;
  endif;wp_reset_postdata();
  $sql = $wpdb->get_var("SELECT date_time from wp_product_date_time WHERE product_id = ".$single_product_data['main_data']->get_id()." limit 0,100");
  $sql = (strpos($sql, ',') !== false)? explode(",",$sql): $sql;
  if(is_array($sql)){
    foreach($sql as $single_id){
      $date_time[] = $single_id;
    }
  }
  else{
    $date_time[] = $sql;
  }
  $date_time = array_merge($date_time,$excluded_dates);
  $date_time_unique = array_unique($date_time);
}
elseif($single_product_data['product_form']->term_id==79){
  $class_name = "single_phase";
}
?>
<div id="page" class="site">
  <?php require_once 'theme-parts/main-menu.php';?>
  <!--start page-->
  <div id="product_<?php echo $single_product_data['main_data']->get_id();?>_block" data-form="<?php echo $single_product_data['product_form']->term_id;?>" data-exDates='<?php if(!empty($date_time_unique)){echo json_encode($date_time_unique);}?>' class="single_page <?php echo $class_name;?>" data-sku="<?php echo $single_product_data['main_data']->get_sku();?>" data-id="<?php echo $single_product_data['main_data']->get_id();?>">
    <div class="section_item grid">
      <div id="single_product_alerts" class="ajax_alerts"></div>
      <div class="content_gallary content">
        <?php include_once 'theme-parts/single-product/gallary-section.php';?>
        <?php include_once 'theme-parts/single-product/info-section.php';?>
      </div>
    </div>
   
    <?php include_once 'theme-parts/related-single.php';?>
    <?php include_once 'theme-parts/reviews-products.php';?>
    <?php include_once 'theme-parts/related-products.php';?>
    <?php //include_once 'theme-parts/accessories.php';?>
  </div>
  <!--end page-->
</div>
<?php require_once 'footer.php';?>
<script>
function select_star(start_value){
  $("#rating").val(start_value);
}
</script>
