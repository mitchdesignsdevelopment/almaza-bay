<?php require_once 'header.php'; global $post; //var_dump($post);?>
<div id="page" class="site">
  <?php require_once 'theme-parts/main-menu.php';?>
  <?php
  if(is_product_category()){
    $cate = get_queried_object();
    if($cate){
      $id = $cate->term_id;
      $cate_slug = $cate->slug;
      $parent = $cate->parent;
      $cat_slug = ($cate->taxonomy == 'product_cat')? $cate->slug : $cate->term_id;
      $page_type = $cate->taxonomy;
      // $allproducts=$cate->count;
    }
    $allproducts=21;
    $posts_per_page=20
    ?>
    <!--start category-->
    <div class="site-content page_list">
        <div class="grid">
            <div class="list_content">
                <?php include_once 'theme-parts/filters-form.php';?>
                <div class="product list">
                    <div class="grid">
                        <div class="product_container">
                            <ul class="products_list products" data-slug="<?php echo (!$cate)? '':$cat_slug; ?>" data-type="<?php echo($cate)? $cate->taxonomy:''; ?>" data-count="20" data-page="1" data-posts="<?php echo $allproducts; ?>" data-search="<?php echo $_GET['s']?>" data-sort="<?php echo (isset($_GET['orderby']))? $_GET['orderby'] :'popularity'; ?>" data-lang="<?php echo $language; ?>">
                            <?php
                            $category_products_ids = mitch_get_products_by_category($cate->term_id,'');
                            if(!empty($category_products_ids)){
                              foreach($category_products_ids as $product_id){
                                $product_data = mitch_get_short_product_data($product_id);
                                include 'theme-parts/product-widget.php';
                              }
                            }
                            ?>
                            </ul>
                            <?php 
                       // if($allproducts > $posts_per_page):
                    ?>

                    <div class="spinner" data-slug="<?php echo (!$cat_slug)? '':$cat_slug; ?>" 
                        data-type="<?php echo (!$cat_slug)?'shop':$term->taxonomy; ?>" 
                        data-count="20" data-page="1" data-posts="<?php echo $allproducts; ?>" 
                        data-sort="<?php echo (isset($_GET['orderby']))? $_GET['orderby'] :'popularity'; ?>">

                            <div class="widget">
                                <div class="image"></div>
                                <div class="content"></div>
                                <div class="content"></div>
                            </div>
                            <div class="widget">
                                <div class="image"></div>
                                <div class="content"></div>
                                <div class="content"></div>
                            </div>
                            <div class="widget">
                                <div class="image"></div>
                                <div class="content"></div>
                                <div class="content"></div>
                            </div>
                            <div class="widget">
                                <div class="image"></div>
                                <div class="content"></div>
                                <div class="content"></div>
                            </div>
                            <div class="widget">
                                <div class="image"></div>
                                <div class="content"></div>
                                <div class="content"></div>
                            </div>
                    </div>
                    <?php //endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end category-->
    <?php
  }else{
    ?>
    <!--start page-->
    <div class="site-content style_page_form">
      <div class="grid">
        <div class="section_title">
          <img class="almaza_icon_page" src="<?php echo $theme_settings['theme_favicon'];?>" alt="" width='50'>
          <h1><?php echo $post->post_title;?></h1>
        </div>
        <div class="section privacy_and_terms">
        <?php echo $post->post_content;?>
        </div>
      </div>
    </div>
    <!--end page-->
    <?php
  }
  ?>
</div>
<?php require_once 'footer.php';?>
