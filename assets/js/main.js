$(document).ready(function () {

    $(window)
    .on("resize", function () {
      var HeaderFooter =
        $("header").innerHeight() + $("footer").innerHeight();
      $(".site-content").css({
        "min-height": $(window).outerHeight() - HeaderFooter,
      });
      // console.log('HeaderFooter',HeaderFooter);
    })
    .resize();

  // Start sticky Header //
     $(window).scroll(function () {
      var sticky = $(".sticky"),
        scroll = $(window).scrollTop();

      if (scroll >= 150)
        sticky.addClass("fixed"), $("body").addClass("sticky-header");
      else sticky.removeClass("fixed"), $("body").removeClass("sticky-header");
    });


    $(window).scroll(function () {
      var sticky = $(".sticky_logo"),
        scroll = $(window).scrollTop();

      if (scroll >= 150)
        sticky.addClass("start");
      else sticky.removeClass("start");
    });

    // $(window).scroll(function () {
    //   var sticky = $(".sticky_text"),
    //     scroll = $(window).scrollTop();

    //   if (scroll >= 4700 && scroll <= 6000)
    //     sticky.addClass("fixed"), $("body").addClass("sticky-header");
       
    //   else sticky.removeClass("fixed"), $("body").removeClass("sticky-header");
    // });
  // End sticky Header //

     // start Mobile Nav ///
          $(document).on("click", ".menu_mobile_icon.open", function () {
              $(this).addClass('hide');
              $('.menu_mobile_icon.close').addClass('show');
              $(".mobile-nav").addClass("active");
              $('html, body').css('overflow', 'hidden');
              return false;
          });
          $(document).on("click", ".menu_mobile_icon.close", function () {
              $(this).removeClass('show');
              $('.menu_mobile_icon.open').removeClass('hide');
              $(".mobile-nav").removeClass("active");
              $('html, body').css('overflow', 'visible');
              return false;
          });


          $(document).on("click", ".mobile-menu .menu-item-has-children:not(.active) .menu-trigger",function () {
              $(this).parent().addClass('active').siblings().removeClass('active');
              $(this).parent().find('.list_subcategory_mobile').slideDown();
              $(this).parent().find('.list_subcategory_mobile').slideDown();
              $(this).parent().closest('.menu-item-has-children').siblings().find('.list_subcategory_mobile').slideUp();
              return false;
          });

          $(document).on("click", ".mobile-menu .menu-item-has-children.active .menu-trigger",function () {
              $(this).parent().removeClass('active');
              $(this).parent().find('.list_subcategory_mobile').slideUp();
              return false;
          });
    // End Mobile Nav ///

    // $(".slider_hotels_img").slick({
    //     rtl: true,
    //     autoplay: false,
    //     slidesToShow: 1,
    //     slidesToScroll: 1,
    //     arrows: false,
    //     dots: true,
    //     infinite: false,
    //     speed: 800,
    //     fade: true,
    // });

    $(".page_home .sec_img_hotel_slick").slick({
      autoplay: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      dots: true,
      infinite: false,
      speed: 800,
      fade: true,
    });
    $(".page_home .section_banner_slick").slick({
      autoplay: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      dots: true,
      infinite: false,
      speed: 800,
      fade: true,
      nextArrow: '<div class="next arrow"><span></span></div>',
      prevArrow: '<div class="prev arrow"><span></span></div>',
    });


    //start open coupon //
      $(document).on('click', '.page_cart .open-coupon', function() {
        $('.page_cart .discount-form').addClass('active');
        $(this).addClass('active');
        return false;
      });

      $(document).on('click', '.page_cart .close-coupon', function() {
        $('.page_cart .discount-form').removeClass('active');
        $('.page_cart .open-coupon').removeClass('active');
          return false;
      });

      $(document).on('click', '.min_cart .open-coupon', function() {
        $('.min_cart .discount-form').addClass('active');
        $(this).addClass('active');
        $('.all_item').addClass('height');
        return false;
      });

      $(document).on('click', '.min_cart .close-coupon', function() {
        $('.min_cart .discount-form').removeClass('active');
        $('.min_cart .open-coupon').removeClass('active');
        $('.all_item').removeClass('height');
          return false;
      });

      $(document).on('click', '.checkout-content .open-coupon', function() {
          $('.checkout_coupon').addClass('active');
          $(this).addClass('active');
          return false;
      });

      $(document).on('click', '.checkout-content .close-coupon', function() {
        $('.checkout_coupon').removeClass('active');
        $('.open-coupon').removeClass('active');
        $(this).removeClass('active');
        return false;
      });
    
    //End open coupon //

    // start menu hover //
        var hoverTimer;
        $(document).on('mouseenter', '.single_menu.has-mega', function() {
        var hotspot = $(this);
            hoverTimer = setTimeout(function() {
            $('.mega-menu').removeClass('active');
            hotspot.find(".mega-menu").addClass('active');        
            }, 300);
        });
        $(document).on('mouseleave', '.single_menu.has-mega', function() {
                clearTimeout(hoverTimer);
                $('.mega-menu').removeClass('active');       
        });
    // End menu hover //  


        $(document).on('click', '.nav_single_title', function() {
            var ths = $(this);
            var trgt = $($(this).attr('href'));
            $('.single_faq').removeClass('active');
            $('.nav_single_title').removeClass('active');
            $(this).addClass('active');
            trgt.addClass('active');
            
            return false;
        });
        $(document).on('click', '.nav_single_title.all_faq', function() {
          $(this).addClass('active');
          $('.single_faq').addClass('active');
          
          return false;
        });
      //start dropdown faq //
        $(document).on('click', '.single_faq .content.faq:not(.active) .question', function(){
            $('.single_faq .content.faq').removeClass('active');
            $('.single_faq .content.faq .answer').slideUp(500);
            $(this).next().slideDown(500);
            $(this).parent().addClass('active');
        });
        $(document).on('click', '.single_faq .content.faq.active .question', function(){
            $(this).next().slideUp(500);
            $(this).parent().removeClass('active');
        });
      //End dropdown faq //



//start Single Hotel And Rooms Types //
    $(document).on('click', '.single_phases.active .nav_single_title', function() {
      var ths = $(this);
      var trgt = $($(this).attr('href'));
      $('.single_phases.active .single_room').removeClass('active');
      $('.single_phases.active .nav_single_title').removeClass('active');
      $(this).addClass('active');
      trgt.addClass('active');
      return false;
      
    });



    if($(".hero_single_img_slick").length){
      $('.hero_single_img_slick').each(function (){
      var slick = $(this).attr('data-page');
        $('.single_phases.active .hero_single_img_slick'+slick).slick({
          autoplay: false,
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false,
          dots: true,
          infinite: false,
          speed: 800,
          fade: true,
      });
      });
    }








    $(document).on('click', '.nav_single_phases', function() {
      var ths = $(this);
      var trgt = $($(this).attr('href'));
      var slick = $(this).attr('data-page');
      $('.single_phases').removeClass('active');
      $('.nav_single_phases').removeClass('active');
      $(this).addClass('active');
      trgt.addClass('active');
      if(!$('.single_phases.active .hero_single_img_slick'+slick).hasClass('slick-initialized')){
        $('.single_phases.active .hero_single_img_slick'+slick).slick({
          autoplay: false,
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false,
          dots: true,
          infinite: false,
          speed: 800,
          fade: true,
      });
      }
     
      return false;
  });
    $(".sec_room_gallery").slick({
      autoplay: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      dots: false,
      infinite: false,
      speed: 800,
      fade: true,
      nextArrow: '<div class="next arrow"><span></span></div>',
      prevArrow: '<div class="prev arrow"><span></span></div>',
    });
    $(".slider_img").slick({
      autoplay: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      dots: false,
      infinite: false,
      speed: 800,
      fade: true,
      nextArrow: '<div class="next arrow"><span></span></div>',
      prevArrow: '<div class="prev arrow"><span></span></div>',
    });
    
    $(document).on('click', '.nav_single_phases', function() {
      var ths = $(this);
      var trgt = $($(this).attr('href'));
      $('.single_phases').removeClass('active');
      $('.nav_single_phases').removeClass('active');
      $(this).addClass('active');
      trgt.addClass('active');
      var trgt_room = $('.single_phases.active').attr('data-main-count');
      $('#single_room_ctr'+trgt_room).click();
      return false;
  });
//End Rooms Types //



//start play video //
    jQuery(document).on('click','.js-videoPlayer',function(e) {
      //отменяем стандартное действие button
      e.preventDefault();
      var poster = $(this);
      // ищем родителя ближайшего по классу
      var wrapper = poster.closest('.js-videoWrapper');
      videoPlay(wrapper);
    });
    function videoPlay(wrapper) {
        var iframe = wrapper.find('.js-videoIframe');
        // Берем ссылку видео из data
        var src = iframe.data('src');
        // скрываем постер
        wrapper.addClass('active');
        // подставляем в src параметр из data
        iframe.attr('src',src);
    }
//End play video //


//start slick single_item //
  $(".product-slider").slick({
    autoplay: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: true,
    // asNavFor: ".slider-nav",
    accessibility: false,
    nextArrow: '<div class="next-arrow"><span></span></div>',
    prevArrow: '<div class="prev-arrow"><span></span></div>',
    responsive: [
    {
        breakpoint: 999,
        settings: {
        rtl:true,
        autoplay: false,
        // dots: true,
        dotsClass: 'custom_paging',
        customPaging: function (slider, i) {
            return  (i + 1) + '/' + slider.slideCount;
        }
        }
    },
    ],
  });

  $(".slider-nav").slick({
    slidesToShow: 6.8,
    slidesToScroll: 1,
    asNavFor: ".product-slider",
    dots: false,
    arrows: true,
    centerMode: false,
    vertical: true,
    verticalSwiping: true,
    focusOnSelect: false,
    infinite: false,
    autoplay: false,
    responsive: [
      {
          breakpoint: 999,
          settings: {
            vertical: false,
            arrows: false,
          }
      },
      ],
  });

  $('.slider-nav .slick-slide').on('click', function (event) {
    $('.slider-nav .slick-slide').removeClass('active');
    $('.product-slider').slick('slickGoTo', $(this).data('slickIndex'));
    $(this).addClass('active');
  });

  $(window).on("load", function () {
    $(".product-slider").addClass("active");
    $(".slider-nav img:nth-child(1)").addClass("active");
    $(".products-slider").addClass("active");
    $(".product-widget .image").addClass("active");
  });



//End slick single_item //



//start popup //
$(document).on('click', '.js-popup-opener', function() {
  var ths = $(this);
  var trgt = $($(this).attr('href'));
  // $(".mobile-nav").removeClass("active");
  // $('.menu_mobile_icon.close').removeClass('show');
  // $('.menu_mobile_icon.open').removeClass('hide');
  $('.popup').removeClass('popup_visible');
  $('.menu_mobile_icon.close').click();
  $('html, body').css('overflow', 'hidden');
  $('#overlay').addClass('overlay_visible');
  trgt.addClass('popup_visible');
  
  return false;
});
$(document).on('click', '.js-popup-closer', function() {
  $('.popup').removeClass('popup_visible');
  $('#overlay').removeClass('overlay_visible');
  $('html, body').css('overflow', 'visible');
  return false;
});

$(".overlay").on("click", function () {
  $('.popup').removeClass('popup_visible');
  $('#overlay').removeClass('overlay_visible');
  $('html, body').css('overflow', 'visible');
});

//End popup //





  //start slick by title //
    $(".new_image_slider_1,.new_image_slider_2,.new_image_slider_3").slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      infinite: true,
      speed: 400,
      fade: true,
  });

  $(".title_slider_1,.title_slider_2,.title_slider_3").slick({
      slidesToShow: 5,
      // slidesToScroll: 1,
      asNavFor: ".new_image_slider_1,.new_image_slider_2,.new_image_slider_3",
      dots: false,
      arrows: false,
      centerMode: false,
      vertical: true,
      verticalSwiping: false,
      focusOnSelect: false,
      infinite: false,
      autoplay: false,
  });



  $('.title_slider_1 .slick-slide').on('click', function (event) {
      $('.title_slider_1 .slick-slide').removeClass('active');
      $('.new_image_slider_1').slick('slickGoTo', $(this).data('slickIndex'));
      $(this).addClass('active');
  });
//   $('.new_image_slider_1').on('beforeChange', function(event, slick, currentSlide, nextSlide){
//       $('.slick-current img').addClass('active');
//       $('.slick-current img').removeClass('zoomm');
//   });
//   $(window).on("load", function () {
//       $(".new_image_slider_1").addClass("active");
//       $(".title_slider_1 img:nth-child(1)").addClass("active");
//       $(".new_image_slider_1").addClass("active");
//       $(".product_widget .image").addClass("active");
//   });

  jQuery('.title_slider_1').addClass('active');
  $('.new_image_slider_1').on('beforeChange', function(event, slick, currentSlide, nextSlide){
    $('.title_slider_1 li').removeClass('active');
    $('.title_slider_1 li'+nextSlide).addClass('active');
  });





  $('.title_slider_2 .slick-slide').on('click', function (event) {
    $('.title_slider_2 .slick-slide').removeClass('active');
    $('.new_image_slider_2').slick('slickGoTo', $(this).data('slickIndex'));
    $(this).addClass('active');
});
// $('.new_image_slider_2').on('beforeChange', function(event, slick, currentSlide, nextSlide){
//     $('.slick-current img').addClass('active');
//     $('.slick-current img').removeClass('zoomm');
// });
// $(window).on("load", function () {
//     $(".new_image_slider_2").addClass("active");
//     $(".title_slider_2 img:nth-child(1)").addClass("active");
//     $(".new_image_slider_2").addClass("active");
//     $(".product_widget .image").addClass("active");
// });

jQuery('.title_slider_2').addClass('active');
$('.new_image_slider_2').on('beforeChange', function(event, slick, currentSlide, nextSlide){
  $('.title_slider_2 li').removeClass('active');
  $('.title_slider_2 li'+nextSlide).addClass('active');
});
        

$('.title_slider_3 .slick-slide').on('click', function (event) {
  $('.title_slider_3 .slick-slide').removeClass('active');
  $('.new_image_slider_3').slick('slickGoTo', $(this).data('slickIndex'));
  $(this).addClass('active');
});
// $('.new_image_slider_3').on('beforeChange', function(event, slick, currentSlide, nextSlide){
//     $('.slick-current img').addClass('active');
//     $('.slick-current img').removeClass('zoomm');
// });
// $(window).on("load", function () {
//     $(".new_image_slider_3").addClass("active");
//     $(".title_slider_3 img:nth-child(1)").addClass("active");
//     $(".new_image_slider_3").addClass("active");
//     $(".product_widget .image").addClass("active");
// });

jQuery('.title_slider_3').addClass('active');
$('.new_image_slider_3').on('beforeChange', function(event, slick, currentSlide, nextSlide){
$('.title_slider_3 li').removeClass('active');
$('.title_slider_3 li'+nextSlide).addClass('active');
});
      
   //End slick by title //

  

   $('.hero_single_img_slick_hotel').slick({
    autoplay: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: true,
    infinite: false,
    speed: 800,
    fade: true,
});

});

