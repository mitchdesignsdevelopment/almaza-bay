<?php
require_once 'header.php';
mitch_validate_logged_in();
$current_user_id = get_current_user_id();
$main_address    = mitch_get_user_main_address($current_user_id);
$other_addresses = mitch_get_user_others_addresses_list($current_user_id);
$countries       = mitch_get_countries()['ar'];
global $states;
$shipping_data   = mitch_get_shipping_data()['shipping_methods'][$theme_settings['default_shipping_method']];
?>
<div id="page" class="site">
  <?php require_once 'theme-parts/main-menu.php';?>
  <!--start page-->
  <div class="site-content page_myaccount">
    <div class="grid">
      <div class="section_title">
          <h1><?php echo $fixed_string['myaccount_page_title'];?></h1>
      </div>
      <div class="page_content">
        <?php include_once 'theme-parts/myaccount-sidebar.php';?>
        <div class="dashbord">
            <div class="section_addresses">
                <div class="address basic">
                  <h5><?php echo $fixed_string['myaccount_page_addresses_title1'];?></h5>
                  <?php
                  if(!empty($main_address)){
                    ?>
                    <p class="single_address">
                      <?php
                      echo $fixed_string['checkout_form_building'].':'.$main_address->building.' - ';
                      echo $fixed_string['checkout_form_street'].':'.$main_address->street.' - ';
                      echo $fixed_string['checkout_form_area'].':'.$main_address->area.' - ';
                      echo $fixed_string['checkout_form_city'].':'.$fixed_string[$main_address->city].' - ';
                      echo $fixed_string['checkout_form_country'].':'.$main_address->country;
                      // echo $main_address->address;
                      ?>
                      <a href="#popup-edit-address"  class="edit load_form_data js-popup-opener"
                      data-id="<?php echo $main_address->ID;?>"
                      data-country="<?php echo $main_address->country;?>"
                      data-city="<?php echo $main_address->city;?>"
                      data-building="<?php echo $main_address->building;?>"
                      data-street="<?php echo $main_address->street;?>"
                      data-area="<?php echo $main_address->area;?>"
                      >
                        <?php echo $fixed_string['myaccount_page_addresses_edit'];?>
                      </a>
                    </p>
                    <?php
                  }
                  ?>
                </div>
                <div class="address all">
                  <h5>
                    <?php echo $fixed_string['myaccount_page_addresses_title2'];?>
                    <a href="#popup-edit-address" class="btn btn-primary pull-left add_new_address js-popup-opener" type="button"><?php echo $fixed_string['myaccount_page_addresses_add_btn'];?></a>
                  </h5>
                  <?php
                  if(!empty($other_addresses)){
                    foreach($other_addresses as $other_address){
                      ?>
                      <p class="single_address">
                        <?php
                        echo $fixed_string['checkout_form_building'].':'.$other_address->building.' - ';
                        echo $fixed_string['checkout_form_street'].':'.$other_address->street.' - ';
                        echo $fixed_string['checkout_form_area'].':'.$other_address->area.' - ';
                        echo $fixed_string['checkout_form_city'].':'.$fixed_string[$main_address->city].' - ';
                        echo $fixed_string['checkout_form_country'].':'.$other_address->country;
                        // echo $other_address->address;
                        ?>
                        <a href="#popup-edit-address"  class="edit load_form_data js-popup-opener"
                        data-id="<?php echo $other_address->ID;?>"
                        data-country="<?php echo $other_address->country;?>"
                        data-city="<?php echo $other_address->city;?>"
                        data-building="<?php echo $other_address->building;?>"
                        data-street="<?php echo $other_address->street;?>"
                        data-area="<?php echo $other_address->area;?>"
                        >
                          <?php echo $fixed_string['myaccount_page_addresses_edit'];?>
                        </a>
                      </p>
                      <?php
                    }
                  }
                  ?>
                </div>
            </div>
            <p class="note_address"><?php echo $fixed_string['myaccount_page_addresses_note'];?></p>
        </div>
      </div>
    </div>
  </div>
  <!--end page-->
  <div id="overlay" class="overlay"></div>
  <div id="popup-edit-address" class="popup edit-address"> 
      <div class="popup__window edit">
        <button type="button" class="popup__close material-icons js-popup-closer">close</button>
        <div class="form-content">
          <h3><img class="almaza_icon_page" src="<?php echo $theme_settings['theme_favicon'];?>" alt="" width='50'><?php echo $fixed_string['myaccount_page_addresses_edit_add'];?></h3>
          <form id="edit_address" action="#" method="post">
            <div class="field select_arrow">
              <label for=""><?php echo 'المحافظة';//$fixed_string['checkout_form_country'];?><span>*</span></label>
              <select id="country" name="country">
              <?php
                  if(!empty($countries)){
                    // print_r($countries);
                    foreach($countries as $code => $name){
                      if($code == $theme_settings['default_country_code']){
                        $selected = 'selected';
                      }else{
                        $selected = '';
                      }
                      echo '<option value="'.$code.'" '.$selected.'>'.$states['ar'][$code].'</option>';
                    }
                  }
              ?>
              </select>
            </div>
            <div class="field select_arrow">
              <label for=""><?php echo 'المنطقة';//$fixed_string['checkout_form_city'];?><span>*</span></label>
              <select id="city" name="city" required>
                <option><?php echo $fixed_string['checkout_form_choose_city'];?></option>
                <?php
                if(!empty($shipping_data['cities'])){
                  foreach($shipping_data['cities'] as $city){
                    echo '<option value="'.$city.'">'.$city.'</option>';
                  }
                }
                ?>
              </select>
            </div>
            <div class="field half_full">
              <label for=""><?php echo $fixed_string['checkout_form_building'];?><span>*</span></label>
              <input id="building" type="number" name="building" required>
            </div>
            <div class="field half_full">
              <label for=""><?php echo 'رقم الشقة';//$fixed_string['checkout_form_area'];?><span>*</span></label>
              <input id="area" type="number" name="area" required>
            </div>
            <div class="field half_full">
              <label for=""><?php echo 'اسم الشارع';//$fixed_string['checkout_form_street'];?><span>*</span></label>
              <input id="street" type="text" name="street" required>
            </div>
            <input id="address_id" type="hidden" name="address_id" value="">
            <input id="operation" type="hidden" name="operation" value="">
            <button type="submit" value=""><?php echo $fixed_string['myaccount_page_addresses_save_add'];?></button>
          </form>
        </div>
      </div>
  </div>
</div>
<?php require_once 'footer.php';?>
