<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package almaza
 * 
 * Template Name: Beach
 *
 */
require_once 'header.php'; global $post;
$page_content = get_field('single_beach');
//var_dump($post);?>
<div id="page" class="site">
  <?php require_once 'theme-parts/main-menu.php';?>
    <!--start page-->
    <div class="site-content the_beach_landing">
            <div class="section_hero">
                <div class="hero_image">
                    <img src="<?php echo $page_content['hero_section']['image'];?>" alt="">
                    <div class="hero_text">
                        <p><?php echo $page_content['hero_section']['title'];?></p>
                        <h1><?php echo $page_content['hero_section']['subtitle'];?></h1>
                        <a class="link" href="<?php echo $page_content['button']['url'];?>"><?php echo $page_content['button']['title'];?></a>
                    </div>
                </div>
            </div>
            <!-- <div class="grid">
                <div class="section_sub_hero">
                    <div class="grid">
                        <h3><?php //echo $page_content['escape_section']['title'];?></h3>
                        <p><?php //echo $page_content['escape_section']['description'];?></p>
                    </div>
                </div>
                <div class="video-box js-videoWrapper">
                    <div class="bg" style="background-image: url(<?php //echo $page_content['escape_section']['video_thumbnail'];?>);"><button class="player js-videoPlayer"></button></div>
                    <div class="youtube-video">
                      <?php //echo $page_content['escape_section']['video'];?>
                    </div>
                </div>
                <div class="section_info">
                    <div class="text">
                        <h3><?php //echo $page_content['confidence_section']['title'];?></h3>
                        <?php //echo $page_content['confidence_section']['description'];?>
                        <a class="link" href="<?php //echo $page_content['button']['url'];?>"><?php //echo $page_content['button']['title'];?></a>
                      </div>
                    <div class="img">
                        <div class="half_img">
                            <img src="./assets/img/half_03.png" alt="">
                            <img src="./assets/img/half_04.png" alt="">
                        </div>
                        <div class="full_img">
                            <img src="./assets/img/full_02.png" alt="">
                        </div>
                        <div class="all_img">
                        <?php //if($page_content['confidence_section']['gallery']): foreach($page_content['confidence_section']['gallery'] as $image):?>
                              <img src="<?php //echo $image;?>" alt="">
                        <?php// endforeach; endif;?>
                        </div>
                      
                    </div>
                </div>
                <div class="hero_single_img">
                <?php //if($page_content['sea_gift_section']['gallery']): foreach($page_content['sea_gift_section']['gallery'] as $image):?>
                    <img src="<?php //echo $image;?>" alt="">
                <?php //endforeach; endif;?>
                </div>
                <div class="section_info">
                    <div class="text">
                        <h3><?php //echo $page_content['sea_gift_section']['title'];?></h3>
                        <?php //echo $page_content['sea_gift_section']['description'];?>
                        <a class="link" href="<?php //echo $page_content['button']['url'];?>"><?php //echo $page_content['button']['title'];?></a>
                      </div>
                    <div class="img ">
                    <img src="<?php //echo $page_content['sea_gift_section']['image'];?>" alt="">
                    </div>
                </div>
            </div> -->
        </div>
        <!--end page-->
</div>
<?php require_once 'footer.php';?>