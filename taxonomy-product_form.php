<?php require_once 'header.php'; global $post; //var_dump($post);?>
<div id="page" class="site">
  <?php require_once 'theme-parts/main-menu.php';?>
  <?php
    $cate = get_queried_object();
    $page_content = get_field('page_content',$cate);
    if($cate){
      $id = $cate->term_id;
      $cate_slug = $cate->slug;
      $parent = $cate->parent;
      $cat_slug = ($cate->taxonomy == 'product_cat')? $cate->slug : $cate->term_id;
      $page_type = $cate->taxonomy;
      // $allproducts=$cate->count;
    }
    $allproducts=21;
    $posts_per_page=20
    ?>
        <div class="site-content page_form_list">
            <div class="section_hero">
                <div class="hero_image">
                    <img src="<?php echo $page_content['hero_section']['image'];?>" alt="">
                    <div class="hero_text">
                        <p><?php echo $page_content['hero_section']['title'];?></p>
                        <h1><?php echo $page_content['hero_section']['subtitle'];?></h1>
                    </div>
                </div>
            </div>
            <div class="grid">
                <div class="section_sub_hero">
                        <h3><?php echo $page_content['escape_section']['title'];?></h3>
                        <p><?php echo $page_content['escape_section']['description'];?></p>
                </div>
                <!-- <div class="sec_list">
                    <?php
                        // $category_products_ids = mitch_get_products_by_category($cate->term_id,'product_form');
                        // if(!empty($category_products_ids)){ $count=1;
                        //     foreach($category_products_ids as $product_id){
                        //if($page_content['widgets']){ foreach($page_content['widgets'] as $widget){
                            //$product_data = mitch_get_short_product_data($widget['product']);
                        ?>
                    <div class="single_list <?php //echo($count%2==0)?'':'row_reverse';?>">
                        <div class="text">
                            <?php //if($product_data['product_form']=='Camps'):?>
                                <span><?php //echo get_field('ages_text',$product_id);?></span>
                            <?php //endif;?>
                            <h3><?php //echo $product_data['product_title'];?></h3>
                            <p><?php //echo $product_data['product_excerpt'];?></p>

                            <div class="items">
                                <ul class="title_slider">
                                    <?php //if($block['links'] && $block['has_links']): $var_count=1; foreach($block['links'] as $link):?>
                                        <li class="slider-nav-title-<?php //echo $count;?> <?php //echo($var_count==1)?'active':'';?>" data-target="<?php //echo 'variant'.$var_count;?>">
                                            <h5 class="link_item"> <?php //echo $link['button_text'];?></h5>   
                                        </li>
                                    <?php //$var_count++; endforeach; endif;?>
                                </ul>
                            </div>

                            <a href="<?php //echo $product_data['product_url'];?>" class="link"><?php //echo $widget['button_text'];?></a>
                            <?php //if($widget['has_another_product']):?>
                            <a href="<?php //echo get_the_permalink($widget['another_product']);?>" class="link"><?php //echo $widget['another_button_text'];?></a>
                            <?php //endif;?>
                        </div>
                        <div class="img ">
                          <img src="<?php //echo $product_data['product_image'];?>" alt="">
                        </div>
                    </div>
                    <?php //$count++; } }?>


                    

                </div> -->
                <div class="sec_list">
                    <?php if($page_content['blocks_section']['blocks']): $count=1; foreach($page_content['blocks_section']['blocks'] as $block):?>
                                <div class="single_list_new  <?php echo($count%2==0)?'':'row_reverse';?>">
                                   
                                    <div class="text">
                                        <h3><?php echo $block['title'];?></h3>
                                        <span><?php echo $block['description'];?></span>

                                        <?php if ( $block['data_not_links_button'] == false ) { ?>
                                            <div class="items">
                                                <ul class="title">
                                                    <?php if($block['links_data'] && !$block['has_links']): $var_count=1; foreach($block['links_data'] as $link):?>
                                                        <li class="slider-nav-title">
                                                            <a class="link_item" target="_blank" href="<?php echo $link['button_text']['url'];?>"><?php echo $link['button_text']['title'];?></a>
                                                        </li>
                                                    <?php $var_count++; endforeach; endif;?>
                                                </ul>
                                            </div>
                                        <?php } ?>
                                        <?php if ( $block['data_not_links_button'] == true ) { ?>
                                            <div class="items not_link">
                                                <ul class="title">
                                                    <?php if($block['data_not_links']): $var_count=1; foreach($block['data_not_links'] as $no_link):?>
                                                        <li class="slider-nav-title">
                                                            <p class="link_item"><?php echo $no_link['title_no_link'];?></p>
                                                        </li>
                                                    <?php $var_count++; endforeach; endif;?>
                                                </ul>
                                            </div>
                                        <?php } ?>

                                        <?php if($block['call_button']['title']):?>
                                        <div class="links">
                                            <a href="<?php echo $block['call_button']['url'];?>" class="link call"><?php echo $block['call_button']['title'];?></a>
                                        </div>
                                        <?php endif;?>
                                    </div>
                                    <div class="img img-con-variant">
                                        <img src="<?php echo $block['image'];?>" alt="">
                                        <?php if($block['box_data'] && $block['has_links']): foreach($block['box_data'] as $link):?>
                                        <div class="box">
                                            <?php if($link['logo']):?>
                                            <img class="sm_icon" src="<?php echo $link['logo'];?>" alt="">
                                            <?php endif;?>
                                            <?php if($link['phone']):?>
                                            <div class="row">
                                                <h4 class="phone">Phone</h4>
                                                <span><a href="tel:<?php echo $link['phone'];?>"><?php echo $link['phone'];?></a></span>
                                            </div>
                                            <?php endif;?>
                                            <?php if($link['working_hours']):?>
                                            <div class="row last">
                                                    <h4 class="working-hours">Working Hours</h4>
                                                    <span><?php echo $link['working_hours'];?></span>
                                            </div>
                                            <?php endif;?>
                                        </div>     
                                        <?php endforeach; endif;?>
                                    </div>
                                </div>
                    <?php $count++; endforeach; endif;?>
                </div>
            </div>
        </div>
</div>
<?php require_once 'footer.php';?>