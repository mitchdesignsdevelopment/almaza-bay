<?php
require_once 'header.php';
$page_content = get_field('page_content');
?>
<div id="page" class="site">
  <?php require_once 'theme-parts/main-menu.php';?>
 <!--start page-->
 <div class="site-content page_experience_almaza">
     <div class="grid">
            <div class="section_title_hero grid_phases">
                <h1>Experience Almaza Bay</h1>
                <span class="subtitle">live your dream</span>
            </div>
            <div class="hero_single_img grid_phases">
                <?php if($page_content['gallery']): foreach($page_content['gallery'] as $image):?>
                    <img src="<?php echo $image;?>" alt="">
                <?php endforeach;endif;?>
            </div>
            <div class="grid_list sec_list">
                <?php if($page_content['sections']): $count=1; foreach($page_content['sections'] as $section):?>
                    <div class="single_list <?php echo($count%2==0)?'row_reverse':'';?>">
                        <div class="text">
                            <h3><?php echo $section['title'];?></h3>
                        <p><?php echo $section['subtitle'];?></p>
                        <?php if($section['items']):?>
                        <div class="items">
                            <ul>
                                <?php foreach($section['items'] as $item):?>
                                    <li>
                                        <a href="<?php echo $item['link']['url'];?>" class="link_item"><?php echo $item['link']['title'];?></a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                                
                            </div>
                            <?php endif;?>
                        <?php if($section['button']['title']):?>
                        <div class="links">
                                <a href="<?php echo $section['button']['url'];?>" class="link "><?php echo $section['button']['title'];?></a>
                        </div>
                            <?php endif;?>
                        </div>
                        <div class="img ">
                        <img src="<?php echo $section['image'];?>" alt="">
                        </div>
                    </div>
                <?php $count++; endforeach;endif;?>
            </div>
     </div>
</div>
        <!--end page-->
</div>
<?php require_once 'footer.php';?>
