<?php //do_action( 'woocommerce_before_checkout_shipping_form', $checkout ); ?>
<?php
// $zones   = WC_Shipping_Zones::get_zones();
// $methods = array_map(function($zone) {
//     return $zone['shipping_methods'];
// }, $zones);

add_action('woocommerce_before_checkout_shipping_form', 'before_shipping');
function before_shipping($checkout){
	WC()->cart->set_shipping_total(50);
	// WC()->cart->calculate_totals();
  // WC()->cart->calculate_shipping();
}

// add_filter( 'woocommerce_package_rates', 'woocommerce_package_rates', 10, 2 );
// function woocommerce_package_rates( $rates, $package ) {
//
// 	if ( ! WC()->cart->is_empty() ) {
// 		$required_products = array( 34 );
//
// 		$found = false;
//
// 		foreach ( WC()->cart->get_cart() as $cart_item ) {
// 			if ( in_array( $cart_item['product_id'], $required_products ) ) {
// 				$found = true;
// 			}
// 		}
// 	}
//
// 	if ( !$found ) {
// 		foreach($rates as $key => $rate ) {
// 			if ( $rates[$key]->label == 'Free shipping' ) {
// 				unset($rates[$key]);
// 			}
// 		}
// 	}
// 	return $rates;
// }

// function oley_reset_default_shipping_method( $method, $available_methods ) {
//
//     $method = key($available_methods);
//     return $method;
//
// }
// add_filter('woocommerce_shipping_chosen_method', 'oley_reset_default_shipping_method', 10, 2);

?>
<!--
<div class="step two">
  <div class="hover_page">
    <div class="grid">
      <div class="content">
        <div class="title">
          <img src="<?php echo $theme_settings['theme_url'];?>/assets/img/Combined_Shape.png" alt="">
          <h2><p class="customize_subtitle"><?php echo $fixed_string['checkout_steps_page_title'];?></p>أختار نوع الياقة</h2>
        </div>
        <div class="slide_customize">
          <?php mitch_get_customized_product_variations_steps($product_data['main_data']->get_children(), $attributes_keys[0]);?>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="step three">
  <div class="hover_page">
    <div class="grid">
      <div class="content">
        <div class="title">
          <img src="<?php echo $theme_settings['theme_url'];?>/assets/img/Combined_Shape.png" alt="">
          <h2><p class="customize_subtitle"><?php echo $fixed_string['checkout_steps_page_title'];?></p>أختار نوع الكم</h2>
        </div>
        <div class="slide_customize">
          <?php mitch_get_customized_product_variations_steps($product_data['main_data']->get_children(), $attributes_keys[1]);?>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="step four">
  <div class="hover_page">
    <div class="grid">
      <div class="content">
        <div class="title">
        <img src="<?php echo $theme_settings['theme_url'];?>/assets/img/Combined_Shape.png" alt="">
          <h2><p class="customize_subtitle"><?php echo $fixed_string['checkout_steps_page_title'];?></p>أختار نوع الأزرار</h2>
        </div>
        <div class="slide_customize">
          <?php mitch_get_customized_product_variations_steps($product_data['main_data']->get_children(), $attributes_keys[2]);?>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="step five">
  <div class="hover_page">
    <div class="grid">
      <div class="content">
        <div class="title">
          <img src="<?php echo $theme_settings['theme_url'];?>/assets/img/Combined_Shape.png" alt="">
          <h2><p class="customize_subtitle"><?php echo $fixed_string['checkout_steps_page_title'];?></p>أختار نوع الجيب</h2>
        </div>
        <div class="slide_customize">
          <?php mitch_get_customized_product_variations_steps($product_data['main_data']->get_children(), $attributes_keys[3]);?>
        </div>
        <button type="button">أضف لعربة التسوق</button>
      </div>
    </div>
  </div>
</div>
-->
<?php
$cart_customized_products = array();
if(!get_post_meta($values['product_id'], 'product_extra_data_product_customized', true)){

}else{
  if(!empty($values['variation'])){
    foreach($values['variation'] as $cart_var_key => $cart_var_value){
      if($cart_var_value != 'none'){
        $cart_customized_products[$values['product_id']]['product_attributes'][] = $cart_var_value;
        $cart_customized_products[$values['product_id']]['product_total'][]      = $values['line_subtotal'];
      }
    }
  }
}
/*if(!empty($cart_customized_products)){
  foreach($cart_customized_products as $cart_customized_product_id => $cart_customized_product_data){
    $products_ids[]    = $cart_customized_product_id;
    $cart_product_data = mitch_get_short_product_data($cart_customized_product_id);
    ?>
    <div id="<?php echo $cart_customized_product_id;?>" class="single_item">
        <div class="sec_item">
            <div class="img">
                <img height="100px" src="<?php echo $cart_product_data['product_image'];?>" alt="<?php echo $cart_product_data['product_title'];?>">
            </div>
            <div class="info">
                <div class="text">
                    <h4><?php echo $cart_product_data['product_title'];?></h4>
                    <ul>
                    <?php
                    if(!empty($cart_customized_product_data['product_attributes'])){
                      foreach($cart_customized_product_data['product_attributes'] as $single_attr){
                        ?>
                        <li><?php echo mitch_get_product_attribute_name($single_attr);?></li>
                        <?php
                      }
                    }
                    ?>
                    </ul>
                    <a href="javascript:void(0);" onclick="cart_remove_item('', '<?php echo $cart_customized_product_id;?>');"><?php echo $fixed_string['cart_delete_title'];?></a>
                </div>
                <p><?php echo array_sum($cart_customized_product_data['product_total']);?> <?php echo $theme_settings['current_currency'];?></p>
            </div>
        </div>
        <div class="section_count">
            <button class="increase" id="increase" onclick="increaseValue()" value="Increase Value"></button>
            <input class="number_count" type="number" id="number" value="1" />
            <button class="decrease" id="decrease" onclick="decreaseValue()" value="Decrease Value"></button>
        </div>
        <p class="total_price"><?php echo array_sum($cart_customized_product_data['product_total']);?> <?php echo $theme_settings['current_currency'];?></p>
    </div>
    <?php
  }
}*/





if(!empty($product_data['main_data']->get_available_variations())){
	foreach($product_data['main_data']->get_available_variations() as $variation_obj){
		foreach($variation_obj['attributes'] as $var_attr_key => $var_attr_value){
			$variations_data[$variation_obj['variation_id']][$var_attr_key] = mitch_get_product_attribute_name($var_attr_value);
			$attributes_data[] = mitch_get_product_attribute_name($var_attr_value);
		}
	}
}
$attributes_data = array_unique($attributes_data);
// if(!empty($variations_data)){
//   foreach($variations_data as $variation_id => $variation_data){
//     mitch_test_vars(array($variation_data));
//   }
// }
// mitch_test_vars(array($variations_data, $attributes_data));
// global $wpdb;
if(!empty($product_data['main_data']->get_attributes())){
	foreach($product_data['main_data']->get_attributes() as $attribute_key => $attribute_obj){
		$attribute_name     = str_replace('pa_', '', $attribute_obj['name']);
		// $terms      = '('.implode(',', $attribute_obj['options']).')';
		// $terms_data = $wpdb->get_results("SELECT term_id, name FROM `wp_terms` WHERE term_id IN $terms");
		//mitch_test_vars(array($terms_data));
		// wc_dropdown_variation_attribute_options(
		// 	array(
		// 		'options'   => $attribute_obj['options'],
		// 		'attribute' => $attribute_name,
		// 		'product'   => $product_data['main_data'],
		// 	)
		// );
		if($attribute_obj['id'] == 6){
			?>
			<div class="section_color">
					<label><?php echo $attribute_name;?></label>
					<div class="colores" id="color_option">
							<div class="single_color active">
									<span style="background-color: white;"></span>
									<p>أبيض</p>
							</div>
							<div class="single_color">
									<span style="background-color: #e2c5b0;"></span>
									<p>كريمي</p>
							</div>
							<div class="single_color">
									<span style="background-color: #5f68a0;"></span>
									<p>أزرق</p>
							</div>
							<div class="single_color">
									<span style="background-color: #000104;"></span>
									<p>أسود</p>
							</div>
							<div class="single_color">
									<span style="background-color: #bebebe;"></span>
									<p>رمادي</p>
							</div>
					</div>
			</div>
			<?php
		}else{
			?>
			<div class="select_height">
					<label><?php echo $attribute_name;?></label>
					<div class="select_arrow">
							<select name="height_option" id="height_option">
							<?php
							if(!empty($variations_data)){
								foreach($variations_data as $variation_id => $variation_data){
									$new_key = 'attribute_'.$attribute_key;
									if(!$variation_data[$new_key]){
										continue;
									}
									?>
									<option value="<?php echo $variation_id;?>"><?php echo $variation_data[$new_key];?></option>
									<?php
								}
							}
							/*if(!empty($product_data['main_data']->get_available_variations())){
								foreach($product_data['main_data']->get_available_variations() as $variation_obj){
									// mitch_test_vars(array($attribute_key, $variation_obj));
									$new_key = 'attribute_'.$attribute_key;
									if(in_array($new_key, array_keys($variation_obj['attributes']))){
										?>
										<option value="<?php echo $variation_obj['variation_id'];?>"><?php echo mitch_get_product_attribute_name($variation_obj['attributes'][$new_key]);?></option>
										<?php
									}
								}
							}*/
							?>
							</select>
					</div>
			</div>
			<?php
		}
	}
}




?>
<li id="product_<?php echo $r_product_id;?>_block" class="product_widget">
		<?php
		if(mitch_check_wishlist_product(get_current_user_id(), $r_product_id)){
			?>
			<span class="fav_btn favourite" onclick="remove_product_from_wishlist(<?php echo $r_product_id;?>);"></span>
			<?php
		}else{
			?>
			<span class="fav_btn not-favourite" onclick="add_product_to_wishlist(<?php echo $r_product_id;?>);"></span>
			<?php
		}
		?>
		<a href="<?php echo $r_product_data['product_url'];?>" class="product_widget_box">
				<div class="img">
						<img src="<?php echo $r_product_data['product_image'];?>" alt="<?php echo $r_product_data['product_title'];?>">
				</div>
				<div class="text">
						<h3 class="title">
							<?php echo $r_product_data['product_title'];?>
							<span>
							<?php if(!empty($r_product_data['product_brand'])){ ?>
							<img src="<?php echo $theme_settings['theme_url'];?>/assets/img/icons/japan.png" alt=""></span>
							<?php }?>
						</h3>
						<?php if(!empty($r_product_data['product_country'])){ ?>
						<p  class="brand">
							<?php echo $fixed_string['product_single_page_country'];?> :
							<span><?php echo $r_product_data['product_country'];?></span>
						</p>
						<?php }?>
						<p class="price"><?php echo $r_product_data['product_price'];?> <?php echo $theme_settings['current_currency'];?></p>
				</div>
		</a>
</li>
<?php
?>


<!--<li class="menu-item menu-item-has-children">
			<a href=""  class="menu-trigger">ملابس المنزل <span class="back_menu">عودة للقائمة الرئيسية</span</a>
			<div class="list_subcategory_mobile">
					<div class="col">
							<h3>أثواب نسيج أبيض</h3>
							<ul class="subcategory">
									<li><a href="">نسيج المجلس</a></li>
									<li> <a href="">نسيج السُمُو</a></li>
									<li> <a href="">نسيج لوسيل</a></li>
									<li> <a href="">نسيج الجزيرة</a></li>
									<li> <a href="">نسيج العز</a></li>
									<li> <a href="">نسيج البدع</a></li>
									<li><a href="">نسيج المعالى</a></li>
									<li><a href="">نسيج الشاهين</a></li>
									<li><a href="">نسيج الصافى</a></li>
							</ul>
							<ul class="subcategory more">
									<li><a href="">نسيج المجلس</a></li>
									<li> <a href="">نسيج السُمُو</a></li>
									<li> <a href="">نسيج لوسيل</a></li>
									<li> <a href="">نسيج الجزيرة</a></li>
									<li> <a href="">نسيج العز</a></li>
									<li> <a href="">نسيج البدع</a></li>
									<li><a href="">نسيج المعالى</a></li>
									<li><a href="">نسيج الشاهين</a></li>
									<li><a href="">نسيج الصافى</a></li>
							</ul>
					</div>
			</div>
	</li>
	<li class="menu-item menu-item-has-children">
			<a href=""  class="menu-trigger">مستلزمات رجالية <span class="back_menu">عودة للقائمة الرئيسية</span</a>
			<div class="list_subcategory_mobile">
					<div class="col">
							<h3>أثواب نسيج أبيض</h3>
							<ul class="subcategory">
									<li><a href="">نسيج المجلس</a></li>
									<li> <a href="">نسيج السُمُو</a></li>
									<li> <a href="">نسيج لوسيل</a></li>
									<li> <a href="">نسيج الجزيرة</a></li>
									<li> <a href="">نسيج العز</a></li>
									<li> <a href="">نسيج البدع</a></li>
									<li><a href="">نسيج المعالى</a></li>
									<li><a href="">نسيج الشاهين</a></li>
									<li><a href="">نسيج الصافى</a></li>
							</ul>
							<ul class="subcategory more">
									<li><a href="">نسيج المجلس</a></li>
									<li> <a href="">نسيج السُمُو</a></li>
									<li> <a href="">نسيج لوسيل</a></li>
									<li> <a href="">نسيج الجزيرة</a></li>
									<li> <a href="">نسيج العز</a></li>
									<li> <a href="">نسيج البدع</a></li>
									<li><a href="">نسيج المعالى</a></li>
									<li><a href="">نسيج الشاهين</a></li>
									<li><a href="">نسيج الصافى</a></li>
							</ul>
					</div>
			</div>
	</li>
	<li class="menu-item menu-item-has-children">
													<a href=""  class="menu-trigger"> العنايه بالذقن <span class="back_menu">عودة للقائمة الرئيسية</span</a>
													<div class="list_subcategory_mobile">
															<div class="col">
																	<h3>أثواب نسيج أبيض</h3>
																	<ul class="subcategory">
																			<li><a href="">نسيج المجلس</a></li>
																			<li> <a href="">نسيج السُمُو</a></li>
																			<li> <a href="">نسيج لوسيل</a></li>
																			<li> <a href="">نسيج الجزيرة</a></li>
																			<li> <a href="">نسيج العز</a></li>
																			<li> <a href="">نسيج البدع</a></li>
																			<li><a href="">نسيج المعالى</a></li>
																			<li><a href="">نسيج الشاهين</a></li>
																			<li><a href="">نسيج الصافى</a></li>
																	</ul>
																	<ul class="subcategory more">
																			<li><a href="">نسيج المجلس</a></li>
																			<li> <a href="">نسيج السُمُو</a></li>
																			<li> <a href="">نسيج لوسيل</a></li>
																			<li> <a href="">نسيج الجزيرة</a></li>
																			<li> <a href="">نسيج العز</a></li>
																			<li> <a href="">نسيج البدع</a></li>
																			<li><a href="">نسيج المعالى</a></li>
																			<li><a href="">نسيج الشاهين</a></li>
																			<li><a href="">نسيج الصافى</a></li>
																	</ul>
															</div>
													</div>
											</li>
	-->
