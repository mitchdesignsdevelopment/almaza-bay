<?php
require_once 'header.php';
$page_content = get_field('page_content');
?>
<div id="page" class="site">
  <?php require_once 'theme-parts/main-menu.php';?>
        <!--start page-->
        <div class="site-content the_village">
            <div class="page_the_village">
                <div class="section_hero">
                    <div class="hero_image">
                        <img src="<?php echo $page_content['hero_section']['image'];?>" alt="">
                        <div class="hero_text">
                            <p><?php echo $page_content['hero_section']['title'];?></p>
                            <h1><?php echo $page_content['hero_section']['subtitle'];?></h1>
                        </div>
                    </div>
                </div>
                <div class="grid">
                    <div class="section_sub_hero">
                        <h3><?php echo $page_content['escape_section']['title'];?></h3>
                        <p><?php echo $page_content['escape_section']['description'];?></p>
                    </div>
                    <div class="sec_list">
                        <?php if($page_content['blocks_section']['blocks']): $count=1; foreach($page_content['blocks_section']['blocks'] as $block):?>
                                <div class="single_list_gallary  <?php echo($count%2==0)?'':'row_reverse';?>">
                                   
                                    <div class="text">
                                        <p><?php echo $block['title'];?></p>
                                        <h3><?php echo $block['subtitle'];?></h3>
                                        <?php if($block['description'] && !$block['has_links']):?>
                                            <span><?php echo $block['description'];?></span>
                                        <?php endif;?>
                                        <div class="items">
                                            <ul class="title_slider title_slider_<?php echo $count; ?>">
                                                <?php if($block['links'] && $block['has_links']): $var_count=1; foreach($block['links'] as $link):?>
                                                    <li class="slider-nav-title-<?php echo $count;?> <?php echo($var_count==1)?'active':'';?>" data-target="<?php echo 'variant'.$var_count;?>">
                                                        <h5 class="link_item"> <?php echo $link['button_text'];?></h5>   
                                                    </li>
                                                <?php $var_count++; endforeach; endif;?>
                                            </ul>
                                        </div>
                                      
                                    </div>
                                    <div class="img img-con new_image_slider new_image_slider_<?php echo $count; ?>">
                                        <?php if($block['image'] && !$block['has_links']):?>
                                            <img src="<?php echo $block['image'];?>" alt="">
                                        <?php endif;?>
                                        <?php if($block['links'] && $block['has_links']): $var_count=1; foreach($block['links'] as $link):?>
                                            <div class="img-con-variant <?php echo($var_count==1)?'active':'';?>" id="<?php echo 'variant'.$var_count;?>">
                                                <img src="<?php echo $link['image'];?>" alt="">
                                                <div class="box">
                                                
                                                    <img class="sm_icon" src="<?php echo $link['logo'];?>" alt="">
                                                    <div class="row">
                                                        <h4 class="phone">Phone</h4>
                                                        <span><a href="tel:<?php echo $link['phone'];?>"><?php echo $link['phone'];?></a></span>
                                                    </div>
                                                    <div class="row last">
                                                            <h4 class="working-hours">Working Hours</h4>
                                                            <span><?php echo $link['working_hours'];?></span>
                                                    </div>
                                                    
                                                </div>     
                                            </div>
                                        <?php $var_count++; endforeach; endif;?>
                                    </div>
                                </div>
                        <?php $count++; endforeach; endif;?>
                    </div>
            </div>
        </div>
        <!--end page-->
</div>
<?php require_once 'footer.php';?>
