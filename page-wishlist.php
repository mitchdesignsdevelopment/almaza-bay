<?php
require_once 'header.php';
mitch_validate_logged_in();
?>
<div id="page" class="site">
  <?php require_once 'theme-parts/main-menu.php';?>
  <!--start page-->
  <div class="site-content page_wishlist">
      <div class="grid">
          <div class="list_content">
              <div class="product wishlist">
                  <div class="grid">
                      <div class="section_title">
                          <img class="naseej_icon_page" src="<?php echo $theme_settings['theme_favicon'];?>" alt="" width='50'>
                          <h2><?php echo $fixed_string['wishlist_page_title'];?></h2>
                      </div>
                      <div class="product_container">
                      <?php
                      $products_query = mitch_get_wishlist_products();
                      if(!empty($products_query)){
                        ?>
                        <ul class="products_list">
                        <?php
                        foreach($products_query as $product_query_obj){
                          $product_data = mitch_get_short_product_data($product_query_obj->product_id);
                          ?>
                          <li id="product_<?php echo $product_query_obj->product_id;?>_block" class="product_widget">


                            <a style="pointer-events: all;" href="#" class="wishlist_remove" onclick="remove_product_from_wishlist(<?php echo $product_query_obj->product_id;?>, 'yes');">
                                <span class="fav_btn favourite" onclick="remove_product_from_wishlist(<?php echo $product_data['product_id'];?>);"></span>
                             </a>
                              <!-- <a style="pointer-events: all;" href="#" class="wishlist_remove" onclick="remove_product_from_wishlist(<?php echo $product_query_obj->product_id;?>, 'yes');">
                                  <span class="material-icons">delete_forever</span>
                                  <?php// echo $fixed_string['cart_delete_title'];?>
                              </a> -->
                              <a href="<?php echo $product_data['product_url'];?>" class="product_widget_box">
                                  <div class="img">
                                      <img src="<?php echo $product_data['product_image'];?>" alt="<?php echo $product_data['product_title'];?>">
                                  </div>
                                  <div class="text">
                                      <h3 class="title">
                                        <?php echo $product_data['product_title'];?>
                                        <span><img src="<?php echo $theme_settings['theme_url'];?>/assets/img/icons/japan.png" alt=""></span>
                                      </h3>
                                      <p  class="brand">
                                        <?php echo $fixed_string['product_single_page_brand'];?> :
                                        <span><?php echo $product_data['product_brand'];?></span>
                                      </p>
                                      <p class="price"><?php echo $product_data['product_price'];?> <?php echo $theme_settings['current_currency'];?></p>
                                  </div>
                              </a>
                          </li>
                          <?php
                        }
                        ?>
                        </ul>
                        <?php
                      }else{
                        ?>
                        <div class="emty_wishlist">
                          <p> <?php echo $fixed_string['alert_wishlist_empty'];?></p>
                        </div>
                        <?php
                      }
                      ?>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!--end page-->
<?php require_once 'footer.php';?>
