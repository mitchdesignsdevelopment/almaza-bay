<?php
require_once 'header.php';
mitch_validate_logged_in();
global $current_user;
?>
<div id="page" class="site">
  <?php require_once 'theme-parts/main-menu.php';?>
  <!--start page-->
  <div class="site-content page_myaccount">
    <div class="grid">
      <div class="section_title">
        <h1><?php echo $fixed_string['myaccount_page_title'];?></h1>
      </div>
      <div class="page_content">
        <?php include_once 'theme-parts/myaccount-sidebar.php';?>
        <div class="dashbord">
          <?php echo get_avatar( $current_user->ID, 80 );?>
          <!-- <img class="profile_img" src="<?php echo $theme_settings['theme_url'];?>/assets/img/profile_img.png" alt=""> -->
          <h3 class="name"><?php echo get_user_meta($current_user->ID, 'first_name', true).' '.get_user_meta($current_user->ID, 'last_name', true);?></h3>
          <!-- <p class="user_email"><?php //echo $current_user->user_login;?></p> -->
          <p class="user_email"><?php echo $current_user->user_email;?></p>
          <p class="user_email"><?php echo get_user_meta($current_user->ID, 'phone_number', true);?></p>
        </div>
      </div>
    </div>
  </div>
  <!--end page-->
</div>
<?php require_once 'footer.php';?>
