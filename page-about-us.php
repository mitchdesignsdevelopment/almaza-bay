<?php
require_once 'header.php';
$page_content = get_field('about_page');
// var_dump($page_content);
?>
<style>
/* .page_about .section_slide .single_slide .text .content {
  color: #000000;
  font-size: 20px;
  font-weight: normal;
  line-height: 36px;
} */
</style>
<div id="page" class="site">
  <?php require_once 'theme-parts/main-menu.php';?>
  <!--start page-->
<div class="site-content about">
      <div class="page_about">
        <div class="grid">
            <!-- <div class="video-box js-videoWrapper">
                <div class="bg" style="background-image: url(<?php //echo $theme_settings['theme_url'];?>/assets/img/about_6.png);"><button class="player js-videoPlayer"></button></div>
                <div class="youtube-video">
                  <iframe class="videoIframe js-videoIframe" src="https://www.youtube.com/embed/mIBjvVL6Bog" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div> -->
            <div class="section_sub_hero">
                <div class="grid">
                    <span class="num"><?php  echo $page_content['experience_section']['number']; ?></span>
                    <h3><?php  echo $page_content['experience_section']['text']; ?></h3>
                    <p><?php  echo $page_content['experience_section']['description']; ?></p>
                </div>
            </div>
            <div class="section_info row_reverse">
                <div class="text">
                    <h3><?php  echo $page_content['travco_section']['title']; ?></h3>
                    <p><?php  echo $page_content['travco_section']['description']; ?></p>
                    <a class="link" target="_blank" href="<?php  echo $page_content['travco_section']['button']['url']; ?>"><?php  echo $page_content['travco_section']['button']['title']; ?></a>
                </div>
                <div class="img">
                    <div class="all_img">
                      <img src="<?php  echo $page_content['travco_section']['image']; ?>" alt="">
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="section_hero_about">
            <div class="hero_image">
                <img src="<?php  //echo $page_content['first_banner_section']['image']; ?>" alt="">
                <div class="hero_text">
                    <h1><?php  //echo $page_content['first_banner_section']['text']; ?></h1>
                    <a class="link" href="<?php  //echo $page_content['first_banner_section']['button']['url']; ?>"><?php  //echo $page_content['first_banner_section']['button']['title']; ?></a>
                </div>
            </div>
        </div> -->
        <div class="grid">
          <div class="section_title">
              <p><?php echo $page_content['discover_more_section']['subtitle']; ?></p>
              <h4><?php  echo $page_content['discover_more_section']['title']; ?></h4>
          </div>
          <?php  if($page_content['discover_more_section']['sections']): $count=1; foreach($page_content['discover_more_section']['sections'] as $section): ?>
              <div class="section_info <?php echo($count%2==0)?'row_reverse':'';?>">
                  <div class="text">
                      <span><?php   echo $section['image_subtitle']; ?></span>
                      <h3><?php   echo $section['image_title']; ?></h3>
                      <p><?php   echo $section['image_description']; ?></p>
                  </div>
                  <div class="img ">
                    <img src="<?php   echo $section['image']; ?>" alt="">
                  </div>
              </div>
          <?php $count++; endforeach; endif;?>
        </div>
        <div class="section_hero_about bottom">
            <div class="hero_image">
                <img src="<?php   echo $page_content['second_banner_section']['image']; ?>" alt="">
                <div class="hero_text">
                    <p><?php echo $page_content['second_banner_section']['text']; ?></p>
                    <h1><?php echo $page_content['second_banner_section']['subtext']; ?></h1>
                    <a class="link" target="_blank" href="<?php  echo $page_content['second_banner_section']['button']['url']; ?>"><?php   echo $page_content['second_banner_section']['button']['title']; ?></a>
                </div>
            </div>
        </div>
      </div>
    </div>
</div>
  <!--end page-->

<?php
 echo $page_content['hero_section']['video'];
 
?>
</div>
<?php require_once 'footer.php';?>
