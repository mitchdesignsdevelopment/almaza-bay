var mitch_home_url     = $('body').attr("data-mitch-home-url");
var mitch_ajax_url     = $('body').attr("data-mitch-ajax-url");
var mitch_logged_in    = $('body').attr("data-mitch-logged-in");
var mitch_current_lang = $('body').attr("data-mitch-current-lang");
function setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}
function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
function eraseCookie(name) {
  document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}
//solve issue of slick slider when reload page it's not work
jQuery('.slider-nav').addClass('active');
// jQuery('.product-slider').addClass('active');
$('.product-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
  $('.slider-nav img').removeClass('active');
  $('.slider-nav-img-'+nextSlide).addClass('active');
  //console.log(nextSlide);
});
function mitch_show_error(element_id, error_msg){
  alert('HIFromError');
  $('#'+element_id).html(error_msg);
  $('#'+element_id).show('slow');
}
function mitch_show_error(element_id){
  $('#'+element_id).html('');
  $('#'+element_id).hide('slow');
}
function mitch_ajax_request(ajax_url, ajax_action, form_data, error_element_id, success_alert_type = 'none'){
  $('#ajax_loader').show();
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: ajax_url,
    data: {
      action: ajax_action,
      form_data: form_data,
    },
    success: function (data) {
      //alert('form was submitted');
      $('#ajax_loader').hide();
      if(data.status == 'success'){
        if(data.redirect_to){
          window.location.replace(data.redirect_to);
        }
        if(data.cart_count){
          $('#cart_total_count').html(data.cart_count);
          $('.sec_end .cart').removeClass('hide');
        }
        else{
          if(data.cart_count==0){
            $('.sec_end .cart').addClass('hide');
          }
        }
        if(data.cart_total){
          $('.cart_total').each(function (){
            $(this).html(data.cart_total);
          });
        }
        if(data.cart_content){
          $('#side_mini_cart_content').html(data.cart_content);
          $('.js-popup-opener[href="#popup-min-cart"]').click();
        }
        if(success_alert_type == 'popup'){
          Swal.fire({
            title: 'Success',
            html: data.msg,
            icon: 'success',
            showConfirmButton: false,
            timer: 1500
          });
        }else{
          if(data.msg){
            $('#'+error_element_id).html('<div class="alert alert-success">تم '+data.msg+'</div>');
            $('#'+error_element_id).show('slow');
            $("html, body").animate({ scrollTop: 0 }, "slow");
          }
        }
      } else if(data.status == 'error'){
        $('#'+error_element_id).html('<div class="alert alert-danger">Something went wrong '+data.msg+'</div>');
        $('#'+error_element_id).show('slow');
        // if(error_element_id == 'checkout_form_alerts'){
        //   $("html, body").animate({ scrollTop: 0 }, "slow");
        // }
        /*if(data.code == 401){
          Swal.fire({
            title: 'Something went wrong',
            html: data.msg,
            icon: 'error',
            showConfirmButton: true,
            // timer: 1500
          });
        }else{
          Swal.fire({
            title: 'Something went wrong',
            text: data.msg,
            icon: 'error',
            showConfirmButton: false,
            timer: 1500
          });
        }*/
      }
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      // alert("Error:" + errorThrown); //"Status: " + textStatus +
      $('#ajax_loader').hide();
      // Swal.fire({
      //   title: 'Something went wrong',
      //   text: errorThrown,
      //   icon: 'error',
      //   showConfirmButton: false,
      //   timer: 1500
      // });
      $('#'+error_element_id).html('<div class="alert alert-danger">Something went wrong</div>');
      $('#'+error_element_id).show('slow');
    }
  });
}
if(mitch_ajax_url){

  $('#register_form').on('submit', function (e) {
    e.preventDefault();
    $('#ajax_loader').show();
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: mitch_ajax_url,
      data: {
        action: "mitch_register_users",
        form_data: $(this).serialize(),
      },
      success: function (data) {
        //alert('form was submitted');
        $('#ajax_loader').hide();
        if(data.status == 'success'){
          if(data.redirect_to){
            window.location.replace(data.redirect_to);
          }
        } else if(data.status == 'error'){
          if(data.code == 401){
            Swal.fire({
              title: 'Something went wrong',
              text: data.msg,
              icon: 'error',
              showConfirmButton: true,
              // timer: 1500
            });
          }else{
            Swal.fire({
              title: 'Something went wrong',
              text: data.msg,
              icon: 'error',
              showConfirmButton: false,
              timer: 1500
            });
          }
        }
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        // alert("Error:" + errorThrown); //"Status: " + textStatus +
        $('#ajax_loader').hide();
        Swal.fire({
          title: 'Something went wrong',
          text: errorThrown,
          icon: 'error',
          showConfirmButton: false,
          timer: 1500
        });
      }
    });
  });

  $('#login_form').on('submit', function (e) {
    e.preventDefault();
    var element_id = 'login_form_alerts';
    var email      = $('#login_email').val();
    var pass       = $('#login_password').val();
    $('#'+element_id).html('');
    if(email != '' && pass != ''){
      mitch_ajax_request(mitch_ajax_url, 'mitch_login_users', $(this).serialize(), element_id);
    }else{
      if(email == ''){
        var error_msg  = 'Something went wrong يجب ان تقوم بكتابة الايميل!';
        $('#'+element_id).append('<div id="login_email_alert" class="alert alert-danger">'+error_msg+'</div>').show('slow');
      }
      if(pass == ''){
        var error_msg  = 'Something went wrong يجب ان تقوم بكتابة كلمة السر!';
        $('#'+element_id).append('<div class="alert alert-danger">'+error_msg+'</div>').show('slow');
      }
      $('#'+element_id).show('slow');
    }
  });

  function cart_remove_item(cart_item_key, product_id, type = 'cart_page'){
    $('#ajax_loader').show();
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: mitch_ajax_url,
      data: {
        action: "cart_remove_item",
        cart_item_key: cart_item_key,
        product_id: product_id
      },
      success: function (data) {
        if(product_id){
          $('#'+product_id).remove();
        }
        if(cart_item_key){
          $('#cart_page_'+cart_item_key).remove();
          $('#mini_cart_'+cart_item_key).remove();
        }
        if(data.cart_count==0){
          $('.sec_end .cart').addClass('hide');
        }
        else{
          $('.sec_end .cart').removeClass('hide');
          $('#cart_total_count').html(data.cart_count);
        }
        $('.cart_total').each(function (){
          $(this).html(data.cart_total);
        });
        // $('#cart_total').html(data.cart_total);
        $('#side_mini_cart_content').html(data.cart_content);
        // alert(data.result);
        if(data.cart_count == 0 && type == 'cart_page'){
          // Simulate an HTTP redirect:
          window.location.replace(mitch_home_url);
        }
        // alert('تم حذف المنتج من سلة المنتجات بنجاح.');
        $('#ajax_loader').hide();
        // Swal.fire({
        //   title: 'تم بنجاح',
        //   text: 'حذف المنتج من سلة المنتجات',
        //   icon: 'success',
        //   showConfirmButton: false,
        //   timer: 1500
        // });
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        // alert("Error:" + errorThrown); //"Status: " + textStatus +
        $('#ajax_loader').hide();
        Swal.fire({
          title: 'Something went wrong',
          text: errorThrown,
          icon: 'error',
          showConfirmButton: false,
          timer: 1500
        });
      }
    });
  }

  function update_cart_items(cart_item_key, location){
    $('#ajax_loader').show();
    if(location == 'cart_page'){
      var quantity_number = $('#cart_page_'+cart_item_key+' .number_count').val();
    }else if(location == 'mini_cart'){
      var quantity_number = $('#mini_cart_'+cart_item_key+' .number_count').val();
    }
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: mitch_ajax_url,
      data: {
        action: "update_cart_items",
        cart_item_key: cart_item_key,
        quantity_number: quantity_number,
        //product_id: product_id
      },
      success: function (data) {

        $('#cart_total_count').html(data.cart_count);
        $('.cart_total').each(function (){
          $(this).html(data.cart_total);
        });
        // $('#cart_total').html(data.cart_total);
        $('#line_subtotal_'+cart_item_key).html(data.item_total);
        $('#side_mini_cart_content').html(data.cart_content);
        // alert('تم تعديل سلة المنتجات بنجاح!');
        $('#ajax_loader').hide();
        // Swal.fire({
        //   title: 'تم بنجاح',
        //   text: 'تعديل سلة المنتجات',
        //   icon: 'success',
        //   showConfirmButton: false,
        //   timer: 1500
        // });
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        // alert("Error:" + errorThrown); //"Status: " + textStatus +
        $('#ajax_loader').hide();
        Swal.fire({
          title: 'Something went wrong',
          text: errorThrown,
          icon: 'error',
          showConfirmButton: false,
          timer: 1500
        });
      }
    });
  }

  // var links = document.getElementsByTagName('a');
  // // alert(links.length);
  // for(var i = 0; i< links.length; i++){
  //   alert(links[i].href);
  // }

  $(document).on('click','#apply_coupon', function () {
    $('#ajax_loader').show();
    var coupon_code = $('#coupon_code').val();
    if(coupon_code){
      $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: mitch_ajax_url,
        data: {
          action: "mitch_apply_coupon",
          coupon_code: coupon_code,
          coupon_from: 'cart'
        },
        success: function (data) {
          //alert('form was submitted');
          $('#ajax_loader').hide();
          if(data.status == 'success'){
            $('#apply_coupon').hide();
            $('#remove_coupon').show();
          $("body").trigger("updated_cart_totals");
          $("body").trigger("update_checkout");
            Swal.fire({
              title: 'Success',
              text: 'Coupon code applied successfully!',
              icon: 'success',
              showConfirmButton: false,
              timer: 1500
            });
            if(data.cart_total){
              // $('#cart_total').html(data.cart_total);
              $('.cart_total').each(function (){
                $(this).html(data.cart_total);
              });
            }
            if(data.redirect_to){
              window.location.replace(data.redirect_to);
            }
          } else if(data.status == 'error'){
            if(data.code == 401){
              Swal.fire({
                title: 'Something went wrong',
                text: data.msg,
                icon: 'error',
                showConfirmButton: true,
                // timer: 1500
              });
            }else{
              Swal.fire({
                title: 'Something went wrong',
                text: data.msg,
                icon: 'error',
                showConfirmButton: false,
                timer: 1500
              });
            }
          }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
          // alert("Error:" + errorThrown); //"Status: " + textStatus +
          $('#ajax_loader').hide();
          Swal.fire({
            title: 'Something went wrong',
            text: errorThrown,
            icon: 'error',
            showConfirmButton: false,
            timer: 1500
          });
        }
      });
    }else{
      $('#ajax_loader').hide();
      Swal.fire({
        title: 'Something went wrong',
        text: 'Please enter coupon code!',
        icon: 'error',
        showConfirmButton: false,
        timer: 1500
      });
    }
  });

  $(document).on('click','#remove_coupon', function () {
    $('#ajax_loader').show();
    var coupon_code = $('#coupon_code').val();
    if(coupon_code){
      $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: mitch_ajax_url,
        data: {
          action: "mitch_remove_coupon",
          coupon_code: coupon_code,
          coupon_from: 'cart'
        },
        success: function (data) {
          //alert('form was submitted');
          $('#ajax_loader').hide();
          if(data.status == 'success'){
            $("body").trigger("updated_cart_totals");
            $("body").trigger("update_checkout");
            $('#apply_coupon').show();
            $('#remove_coupon').hide();
            $('.list_pay.discount').hide();
            document.getElementById('coupon_code').value = '';
            // Swal.fire({
            //   title: 'تم بنجاح',
            //   text: 'ازالة كوبون الخصم!',
            //   icon: 'success',
            //   showConfirmButton: false,
            //   timer: 1500
            // });
            if(data.cart_total){
              // $('#cart_total').html(data.cart_total);
              $('.cart_total').each(function (){
                $(this).html(data.cart_total);
              });
            }
            if(data.redirect_to){
              window.location.replace(data.redirect_to);
            }
          } else if(data.status == 'error'){
            if(data.code == 401){
              Swal.fire({
                title: 'Something went wrong',
                text: data.msg,
                icon: 'error',
                showConfirmButton: true,
                // timer: 1500
              });
            }else{
              Swal.fire({
                title: 'Something went wrong',
                text: data.msg,
                icon: 'error',
                showConfirmButton: false,
                timer: 1500
              });
            }
          }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
          // alert("Error:" + errorThrown); //"Status: " + textStatus +
          $('#ajax_loader').hide();
          Swal.fire({
            title: 'Something went wrong',
            text: errorThrown,
            icon: 'error',
            showConfirmButton: false,
            timer: 1500
          });
        }
      });
    }else{
      $('#ajax_loader').hide();
      Swal.fire({
        title: 'Something went wrong',
        text: 'لا يوجد كود خصم!',
        icon: 'error',
        showConfirmButton: false,
        timer: 1500
      });
    }
  });

  function add_product_to_wishlist(product_id){
    if(mitch_logged_in == 'yes'){
      $('#ajax_loader').show();
      $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: mitch_ajax_url,
        data: {
          action: 'add_product_to_wishlist',
          product_id: product_id,
        },
        success: function (data) {
          $('#ajax_loader').hide();
          if(data.status == 'success'){
            $('#product_'+product_id+'_block .fav_btn').attr("onclick","remove_product_from_wishlist("+product_id+")");
            $('#product_'+product_id+'_block .fav_btn').removeClass('not-favourite');
            $('#product_'+product_id+'_block .fav_btn').addClass('favourite');
            // Swal.fire({
            //   title: 'تم بنجاح',
            //   text: 'اضافة المنتج لقائمة المفضلة',
            //   icon: 'success',
            //   showConfirmButton: false,
            //   timer: 1500
            // });
          }else{
            Swal.fire({
              title: 'Something went wrong',
              text: '',
              icon: 'error',
              showConfirmButton: false,
              timer: 1500
            });
          }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
          $('#ajax_loader').hide();
          Swal.fire({
            title: 'Something went wrong',
            text: errorThrown,
            icon: 'error',
            showConfirmButton: false,
            timer: 1500
          });
        }
      });
    }else{
      $('.login.js-popup-opener').click();
    }
  }

  function remove_product_from_wishlist(product_id, remove_block = 'NULL'){
    $('#ajax_loader').show();
    // alert(product_id);
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: mitch_ajax_url,
      data: {
        action: 'remove_product_from_wishlist',
        product_id: product_id,
      },
      success: function (data) {
        $('#ajax_loader').hide();
        if(data.status == 'success'){
          $('#product_'+product_id+'_block .fav_btn').attr("onclick","add_product_to_wishlist("+product_id+")");
          $('#product_'+product_id+'_block .fav_btn').removeClass('favourite');
          $('#product_'+product_id+'_block .fav_btn').addClass('not-favourite');
          if(remove_block == 'yes'){
            $('#product_'+product_id+'_block').remove();
          }
          // Swal.fire({
          //   title: 'تم بنجاح',
          //   text: 'ازالة المنتج من قائمة المفضلة',
          //   icon: 'success',
          //   showConfirmButton: false,
          //   timer: 1500
          // });
        }else{
          Swal.fire({
            title: 'Something went wrong',
            text: '',
            icon: 'error',
            showConfirmButton: false,
            timer: 1500
          });
        }
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        $('#ajax_loader').hide();
        Swal.fire({
          title: 'Something went wrong',
          text: errorThrown,
          icon: 'error',
          showConfirmButton: false,
          timer: 1500
        });
      }
    });
  }
  // alert(mitch_logged_in);

  $('.load_form_data').on('click', function () {
    $("#address_id").val($(this).data('id'));
    $("#country").val($(this).data('country'));
    $("#city").val($(this).data('city'));
    $("#building").val($(this).data('building'));
    $("#street").val($(this).data('street'));
    $("#area").val($(this).data('area'));
    $("#operation").val('edit_address');
  });
  $('.add_new_address').on('click', function () {
    $('#edit_address')[0].reset();
    $("#operation").val('add_address');
    $("#address_id").val('');
  });
  $('#edit_address').on('submit', function (e) {
    e.preventDefault();
    $('#ajax_loader').show();
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: mitch_ajax_url,
      data: {
        action: "mitch_edit_address",
        form_data: $(this).serialize(),
      },
      success: function (data) {
        //alert('form was submitted');
        $('#ajax_loader').hide();
        if(data.status == 'success'){
          // Swal.fire({
          //   title: 'تم بنجاح',
          //   text: 'حفظ بيانات العنوان',
          //   icon: 'success',
          //   showConfirmButton: false,
          //   timer: 1500
          // });
          location.reload();
        }
        if(data.status == 'error'){
          Swal.fire({
            title: 'Something went wrong',
            text: errorThrown,
            icon: 'error',
            showConfirmButton: false,
            timer: 1500
          });
        }
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        // alert("Error:" + errorThrown); //"Status: " + textStatus +
        $('#ajax_loader').hide();
        Swal.fire({
          title: 'Something went wrong',
          text: errorThrown,
          icon: 'error',
          showConfirmButton: false,
          timer: 1500
        });
      }
    });
  });

  $('#cancel_order_form').on('submit', function (e) {
    e.preventDefault();
    $('#ajax_loader').show();
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: mitch_ajax_url,
      data: {
        action: "mitch_cancel_order",
        form_data: $(this).serialize(),
      },
      success: function (data) {
        //alert('form was submitted');
        $('#ajax_loader').hide();
        if(data.status == 'success'){
          if(data.redirect_to){
            window.location.replace(data.redirect_to);
          }
        } else if(data.status == 'error'){
          if(data.code == 401){
            Swal.fire({
              title: 'Something went wrong',
              html: data.msg,
              icon: 'error',
              showConfirmButton: true,
              // timer: 1500
            });
          }else{
            Swal.fire({
              title: 'Something went wrong',
              text: data.msg,
              icon: 'error',
              showConfirmButton: false,
              timer: 1500
            });
          }
        }
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        // alert("Error:" + errorThrown); //"Status: " + textStatus +
        $('#ajax_loader').hide();
        Swal.fire({
          title: 'Something went wrong',
          text: errorThrown,
          icon: 'error',
          showConfirmButton: false,
          timer: 1500
        });
      }
    });
  });

  // $('#checkout_form').on('submit', function (e) {
  //   //e.preventDefault();
  //   // alert($('input[name="terms-accept"]:checked').length);
  //   var element_id = 'checkout_form_alerts';
  //   var error_msg  = '';
  //   if($('input[name="terms-accept"]:checked').length == 0){
  //     var error_msg = 'برجاء تحقق من الشروط والاحكام!';
  //   }
  //   if($("input[name='building']").val() == '' || $("input[name='area']").val() == '' || $("input[name='street']").val() == ''){
  //     var error_msg = 'برجاء كتابة بيانات العنوان كاملة!';
  //   }
  //   if($("select[name='city']").val() == ''){
  //     var error_msg = 'برجاء اختيار المدينة!';
  //   }
  //   if($("input[name='phone']").val() == ''){
  //     var error_msg = 'برجاء كتابة رقم الجوال!';
  //   }
  //   if($("input[name='email']").val() == ''){
  //     var error_msg = 'برجاء كتابة الايميل!';
  //   }
  //   if($("input[name='lastname']").val() == ''){
  //     var error_msg = 'برجاء كتابة اسم العائلة!';
  //   }
  //   if($("input[name='firstname']").val() == ''){
  //     var error_msg = 'برجاء كتابة الأسم الاول!';
  //   }
  //   if(error_msg == ''){
  //     mitch_ajax_request(mitch_ajax_url, 'mitch_create_order', $(this).serialize(), element_id);
  //   }else{
  //     $('#'+element_id).html('');
  //     $('#'+element_id).append('<div class="alert alert-danger">'+error_msg+'</div>').show('slow');
  //     $('#'+element_id).show('slow');
  //     // window.scrollTo(0, 0);
  //     $("html, body").animate({ scrollTop: 0 }, "slow");
  //   }
  // });
  function show_password_fields(){
    if($('#new_account_button input').is(":checked")){
      $("#password_fields").show('slow');
      $("#new_password").prop('required',true);
      $("#confirm_password").prop('required',true);
    }else{
      $("#password_fields").hide('slow');
      $("#new_password").prop('required',false);
      $("#confirm_password").prop('required',false);
    }
  }
  show_password_fields();
  // $('#apply_coupon').on('click', function () {
  //   $('#ajax_loader').show();
  //   var coupon_code = $('#coupon_code').val();
  //   if(coupon_code){
  //     $.ajax({
  //       type: 'POST',
  //       dataType: 'JSON',
  //       url: mitch_ajax_url,
  //       data: {
  //         action: "mitch_apply_coupon",
  //         coupon_code: coupon_code,
  //         coupon_from: 'checkout'
  //       },
  //       success: function (data) {
  //         //alert('form was submitted');
  //         $('#ajax_loader').hide();
  //         if(data.status == 'success'){
  //           eraseCookie('custom_product_home_visit_time');
  //           eraseCookie('custom_product_branch_visit');
  //           eraseCookie('custom_product_visit_type');
  //           $('#apply_coupon').hide();
  //           $('#remove_coupon').show();
  //           if(data.cart_total){
  //             // $('#cart_total').html(data.cart_total);
  //             $('.cart_total').each(function (){
  //               $(this).html(data.cart_total);
  //             });
  //           }
  //           if(data.cart_discount_div){
  //             $(data.cart_discount_div).insertAfter("#car_subtotal_div");
  //           }
  //           // Swal.fire({
  //           //   title: 'تم بنجاح',
  //           //   text: 'تطبيق كوبون الخصمم!',
  //           //   icon: 'success',
  //           //   showConfirmButton: false,
  //           //   timer: 1500
  //           // });
  //           if(data.redirect_to){
  //             window.location.replace(data.redirect_to);
  //           }
  //         } else if(data.status == 'error'){
  //           if(data.code == 401){
  //             Swal.fire({
  //               title: 'Something went wrong',
  //               text: data.msg,
  //               icon: 'error',
  //               showConfirmButton: true,
  //               // timer: 1500
  //             });
  //           }else{
  //             Swal.fire({
  //               title: 'Something went wrong',
  //               text: data.msg,
  //               icon: 'error',
  //               showConfirmButton: false,
  //               timer: 1500
  //             });
  //           }
  //         }
  //       },
  //       error: function(XMLHttpRequest, textStatus, errorThrown) {
  //         // alert("Error:" + errorThrown); //"Status: " + textStatus +
  //         $('#ajax_loader').hide();
  //         Swal.fire({
  //           title: 'Something went wrong',
  //           text: errorThrown,
  //           icon: 'error',
  //           showConfirmButton: false,
  //           timer: 1500
  //         });
  //       }
  //     });
  //   }else{
  //     $('#ajax_loader').hide();
  //     Swal.fire({
  //       title: 'Something went wrong',
  //       text: 'Please enter coupon code!',
  //       icon: 'error',
  //       showConfirmButton: false,
  //       timer: 1500
  //     });
  //   }
  // });

  // $('#remove_coupon').on('click', function () {
  //   $('#ajax_loader').show();
  //   var coupon_code = $('#coupon_code').val();
  //   if(coupon_code){
  //     $.ajax({
  //       type: 'POST',
  //       dataType: 'JSON',
  //       url: mitch_ajax_url,
  //       data: {
  //         action: "mitch_remove_coupon",
  //         coupon_code: coupon_code,
  //         coupon_from: 'checkout'
  //       },
  //       success: function (data) {
  //         //alert('form was submitted');
  //         $('#ajax_loader').hide();
  //         if(data.status == 'success'){
  //           $('#apply_coupon').show();
  //           $('#remove_coupon').hide();
  //           $('.list_pay.discount').hide();
  //           document.getElementById('coupon_code').value = '';
  //           // Swal.fire({
  //           //   title: 'تم بنجاح',
  //           //   text: 'ازالة كوبون الخصم!',
  //           //   icon: 'success',
  //           //   showConfirmButton: false,
  //           //   timer: 1500
  //           // });
  //           if(data.cart_total){
  //             // $('#cart_total').html(data.cart_total);
  //             $('.cart_total').each(function (){
  //               $(this).html(data.cart_total);
  //             });
  //           }
  //           if(data.redirect_to){
  //             window.location.replace(data.redirect_to);
  //           }
  //         } else if(data.status == 'error'){
  //           if(data.code == 401){
  //             Swal.fire({
  //               title: 'Something went wrong',
  //               text: data.msg,
  //               icon: 'error',
  //               showConfirmButton: true,
  //               // timer: 1500
  //             });
  //           }else{
  //             Swal.fire({
  //               title: 'Something went wrong',
  //               text: data.msg,
  //               icon: 'error',
  //               showConfirmButton: false,
  //               timer: 1500
  //             });
  //           }
  //         }
  //       },
  //       error: function(XMLHttpRequest, textStatus, errorThrown) {
  //         // alert("Error:" + errorThrown); //"Status: " + textStatus +
  //         $('#ajax_loader').hide();
  //         Swal.fire({
  //           title: 'Something went wrong',
  //           text: errorThrown,
  //           icon: 'error',
  //           showConfirmButton: false,
  //           timer: 1500
  //         });
  //       }
  //     });
  //   }else{
  //     $('#ajax_loader').hide();
  //     Swal.fire({
  //       title: 'Something went wrong',
  //       text: 'لا يوجد كود خصم!',
  //       icon: 'error',
  //       showConfirmButton: false,
  //       timer: 1500
  //     });
  //   }
  // });
  function next_button_proceed(){
    if(getCookie('custom_product_home_visit_time') || getCookie('custom_product_branch_visit')){
      $('#next_button').removeClass('disabled');
      $('.next_step').removeClass('disabled');
      $('.breadcramb').removeClass('disabled');
      $('.step-nav-two').removeClass('disabled');
    }else{
      $('#next_button').addClass('disabled');
      $('.next_step').addClass('disabled');
      $('.breadcramb').addClass('disabled');
    }
  }

  $(document).on('click', '.single_box', function(){
    var img  = $(this).data('variation-img'); //one
    var step = $(this).data('variation-step');
    $('.step-nav-'+step+' .min_box').removeClass('emty'); //
    $('.step-nav-'+step+' .min_box').addClass('done');
    $('.step-nav-'+step+' .min_box img').attr('src', img);
  });

  // function checkout_location(){
  //   if($('#home_checkbox').is(":checked")){
  //     $(".home_checkbox_content").show();
  //     $(".branch_checkbox_content").hide();
  //   }else if ($('#branch_checkbox').is(":checked")){
  //     $(".home_checkbox_content").hide();
  //     $(".branch_checkbox_content").show();
  //   }
  // }

  $('#profile_settings').on('submit', function (e) {
    e.preventDefault();
    $('#ajax_loader').show();
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: mitch_ajax_url,
      data: {
        action: "mitch_profile_settings",
        form_data: $(this).serialize(),
      },
      success: function (data) {
        //alert('form was submitted');
        $('#ajax_loader').hide();
        if(data.status == 'success'){
          // Swal.fire({
          //   title: 'تم بنجاح',
          //   text: 'تعديل بيانات الحساب',
          //   icon: 'success',
          //   showConfirmButton: false,
          //   timer: 1500
          // });
        } else if(data.status == 'error'){
          if(data.code == 401){
            Swal.fire({
              title: 'Something went wrong',
              text: data.msg,
              icon: 'error',
              showConfirmButton: true,
              // timer: 1500
            });
          }else{
            Swal.fire({
              title: 'Something went wrong',
              text: data.msg,
              icon: 'error',
              showConfirmButton: false,
              timer: 1500
            });
          }
        }
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        // alert("Error:" + errorThrown); //"Status: " + textStatus +
        $('#ajax_loader').hide();
        Swal.fire({
          title: 'Something went wrong',
          text: errorThrown,
          icon: 'error',
          showConfirmButton: false,
          timer: 1500
        });
      }
    });
  });

  // function simple_product_add_to_cart(product_id){
  //   // var quantity = parseInt($('#number').val());
  //   var quantity = 1;
  //   $('#ajax_loader').show();
  //   var time_slot = '';
  //   var  date_time_var = '';
  //   var  unit_number = '';
  //   var  error_msg = '';
  //   var  target_err = '';
  //   if($('.time_slots .variation_option.active').attr("data-name")){
  //     $('.time_slots .variation_option.active').each(function (){
  //       time_slot += $('#billing_date_time').val()+' '+$(this).attr("data-name")+',';

  //     });
  //   }
  //   if($('#billing_date_time').val()){
  //     date_time_var = $('#billing_date_time').val();
  //   }
  //   if(time_slot){
  //     date_time_var = time_slot;
  //   }
  //   if($('#billing_unit_number').length){
  //     unit_number = $('#billing_unit_number').val();
  //     if(!unit_number){
  //     error_msg = 'Please unter your unit number';
  //     target_err = $('#billing_unit_number').attr("id");
  //     }
  //     else{
  //       $('#billing_unit_number').removeClass('disable');
  //     }
  //   }
  //   if(!error_msg){
  //   $.ajax({
  //     type: 'POST',
  //     dataType: 'JSON',
  //     url: mitch_ajax_url,
  //     data: {
  //       action: "simple_product_add_to_cart",
  //       product_id: product_id,
  //       date_time: date_time_var,
  //       unit_number: unit_number,
  //       quantity_number: quantity,
  //     },
  //     success: function (data) {
  //       //alert('تم اضافة المنتج لسلة المشتريات بنجاح.');
  //       $('#ajax_loader').hide();
  //       if(data.status == 'success'){
  //         $('.sec_end .cart').removeClass('hide');
  //         $('#cart_total_count').html(data.cart_count);
  //         $('#side_mini_cart_content').html(data.cart_content);
  //         // Swal.fire({
  //         //   title: 'تم بنجاح',
  //         //   text: 'اضافة المنتج الي سلة المشتريات',
  //         //   icon: 'success',
  //         //   showConfirmButton: false,
  //         //   timer: 1500
  //         // });
  //         $('.js-popup-opener[href="#popup-min-cart"]').click();
  //         if(data.redirect_to){
  //           window.location.replace(data.redirect_to);
  //         }
  //       } else if(data.status == 'error'){
  //         if(quantity == 0){
  //           var msg = 'Enter Quantity';
  //         }else if(data.msg){
  //           var msg = data.msg;
  //         }else{
  //           var msg = 'Something went wrong!';
  //         }
  //         if(data.code == 401){
  //           Swal.fire({
  //             title: 'Something went wrong',
  //             html: msg,
  //             icon: 'error',
  //             showConfirmButton: true,
  //             // timer: 1500
  //           });
  //         }else{
  //           Swal.fire({
  //             title: 'Something went wrong',
  //             html: msg,
  //             icon: 'error',
  //             showConfirmButton: false,
  //             timer: 1500
  //           });
  //         }
  //       }
  //     },
  //     error: function(XMLHttpRequest, textStatus, errorThrown) {
  //       //alert("Error:" + errorThrown); //"Status: " + textStatus +
  //       $('#ajax_loader').hide();
  //       Swal.fire({
  //         title: 'Something went wrong',
  //         text: errorThrown,
  //         icon: 'error',
  //         showConfirmButton: false,
  //         timer: 1500
  //       });
  //     }
  //   });
  //   }
  //   else{
  //     if(target_err){
  //       $('#'+target_err).addClass('disable');
  //     }
  //   }
  // }


  // function variable_product_add_to_cart(product_id){
  //   $('#ajax_loader').show();
  //   var var_items = jQuery('.variation_option.active').map(function() {
  //     var key       = $(this).data('key');
  //     var item_arr  = new Object();
  //     item_arr[key] = $(this).attr('data-value');
  //     return item_arr;
  //   }).get();
  //   // alert(var_items);
  //   var time_slot = '';
  //   var  date_time_var = '';
  //   var  unit_number = '';
  //   var  error_msg = '';
  //   var  target_err = '';
  //   if($('.time_slots .variation_option.active').attr("data-name")){
  //     $('.time_slots .variation_option.active').each(function (){
  //       time_slot += $('#billing_date_time').val()+' '+$(this).attr("data-name")+',';

  //     });
  //   }
  //   if($('#billing_date_time').val()){
  //     date_time_var = $('#billing_date_time').val();
  //   }
  //   if(time_slot){
  //     date_time_var = time_slot;
  //   }
  //   if($('#billing_unit_number').length){
  //     unit_number = $('#billing_unit_number').val();
  //     if(!unit_number){
  //     error_msg = 'Please unter your unit number';
  //     target_err = $('#billing_unit_number').attr("id");
  //     }
  //     else{
  //       $('#billing_unit_number').removeClass('disable');
  //     }
  //   }

  //   if(!error_msg){
  //     $.ajax({
  //       type: 'POST',
  //       dataType: 'JSON',
  //       url: mitch_ajax_url,
  //       data: {
  //         action: "variable_product_add_to_cart",
  //         product_id: product_id,
  //         selected_items: var_items,
  //         date_time: date_time_var,
  //         unit_number: unit_number,
  //         // variation_id: variation_id,
  //         quantity_number: $('#number').val(),
  //       },
  //       success: function (data) {
  //         //alert('تم اضافة المنتج لسلة المشتريات بنجاح.');
  //         $('.sec_end .cart').removeClass('hide');
  //         $('#cart_total_count').html(data.cart_count);
  //         $('#side_mini_cart_content').html(data.cart_content);
  //         $('#ajax_loader').hide();
  //         $('.js-popup-opener[href="#popup-min-cart"]').click();
  //         // Swal.fire({
  //         //   title: 'تم بنجاح',
  //         //   text: 'اضافة المنتج الي سلة المشتريات',
  //         //   icon: 'success',
  //         //   showConfirmButton: false,
  //         //   timer: 1500
  //         // });
  //       },
  //       error: function(XMLHttpRequest, textStatus, errorThrown) {
  //         //alert("Error:" + errorThrown); //"Status: " + textStatus +
  //         $('#ajax_loader').hide();
  //         Swal.fire({
  //           title: 'Something went wrong',
  //           text: errorThrown,
  //           icon: 'error',
  //           showConfirmButton: false,
  //           timer: 1500
  //         });
  //       }
  //     });
  //   }
  //   else{
  //       if(target_err){
  //         $('#'+target_err).addClass('disable');
  //       }
  //   }
  // }


  $(window).bind("load", function () {
    if($('.variable_middle').length){
      get_availablility_variable_product($('.single_page').attr('data-id'),$('.single_page').attr('data-form'));
    }

    $("body").addClass("fully-loaded");
  });
  $(document).on("change",'#height_option_les-elfes-camp-sessions',function(){
    $('#height_option_les-elfes-camp-sessions .variation_option').each(function (){
      $(this).removeClass('active');
    });
    $('#height_option_les-elfes-camp-sessions option:selected').toggleClass('active');
   });
  $(document).on("change",'#height_option_health-insurance',function(){
    $('#height_option_health-insurance .variation_option').each(function (){
      $(this).removeClass('active');
    });
    $('#height_option_health-insurance option:selected').toggleClass('active');
   });
   $(document).on("change",'#height_option_camper-type',function(){
    $('#height_option_camper-type .variation_option').each(function (){
      $(this).removeClass('active');
    });
    $('#height_option_camper-type option:selected').toggleClass('active');
   });
 $(document).on("change",'#height_option_weeks',function(){
  $('#height_option_weeks .variation_option').each(function (){
    $(this).removeClass('active');
  });
  $('#height_option_weeks option:selected').toggleClass('active');
 });
 $(document).on("change",'#height_option_no-of-people',function(){
  $('#height_option_no-of-people .variation_option').each(function (){
    $(this).removeClass('active');
  });
  $('#height_option_no-of-people option:selected').toggleClass('active');
 });
 $(document).on("change",'#height_option_fields',function(){
  $('#height_option_fields .variation_option').each(function (){
    $(this).removeClass('active');
  });
  $('#height_option_fields option:selected').toggleClass('active');
 });
 $(document).on("change",'#height_option_court',function(){
  $('#height_option_court .variation_option').each(function (){
    $(this).removeClass('active');
  });
  $('#height_option_court option:selected').toggleClass('active');
 });
 $(document).on("change",'#height_option_group-sessions',function(){
  $('#height_option_group-sessions .variation_option').each(function (){
    $(this).removeClass('active');
  });
  $('#height_option_group-sessions option:selected').toggleClass('active');
 });
 if($('#height_option_age-group').length){
  if($('#height_option_age-group').val().includes('Kids')){
    // $("#height_option_group-sessions option:contains('20 Sessions')").attr("disabled","disabled");
    $("input[type=radio][value=intro-to-climbing-one-time-pass]").prop("disabled",true);
    $("input[type=radio][value=intro-to-climbing-one-time-pass]").parent().addClass("disabled");
  }
  else{
    $("input[type=radio][value=intro-to-climbing-one-time-pass]").prop("disabled",false);
    $("input[type=radio][value=intro-to-climbing-one-time-pass]").parent().removeClass("disabled");
  }
 $(document).on("change",'#height_option_age-group',function(){
  var val  = $(this).val();
  if(val.includes('Kids')){
    // $("#height_option_group-sessions option:contains('20 Sessions')").attr("disabled","disabled");
    $("input[type=radio][value=intro-to-climbing-one-time-pass]").prop("disabled",true);
    $("input[type=radio][value=intro-to-climbing-one-time-pass]").parent().addClass("disabled");
    if($("input[name=pass-type-radio]:checked").val()=="intro-to-climbing-one-time-pass"){
      $("input[type=radio][value=4-sessions-valid-for-2-weeks]").click();
      $("input[type=radio][value=4-sessions-valid-for-2-weeks]").prop('checked', true);
      $("input[type=radio][value=intro-to-climbing-one-time-pass]").removeClass("active");
      $("input[type=radio][value=4-sessions-valid-for-2-weeks]").addClass('active');
    }
  }
  else{
    $("input[type=radio][value=intro-to-climbing-one-time-pass]").prop("disabled",false);
    $("input[type=radio][value=intro-to-climbing-one-time-pass]").parent().removeClass("disabled");
  }
  $('#height_option_age-group .variation_option').each(function (){
    $(this).removeClass('active');
  });
  $('#height_option_age-group option:selected').toggleClass('active');
 });
}
 $(document).on("change",'#height_option_program',function(){
  var val  = $(this).val();
  if(val.includes('Private')){
    $("#height_option_group-sessions option:contains('20 Sessions')").attr("disabled","disabled");
    $("#height_option_group-sessions option:contains('20 Sessions')").addClass("disabled");
  }
  else{
    $("#height_option_group-sessions option:contains('20 Sessions')").removeAttr('disabled');
    $("#height_option_group-sessions option:contains('20 Sessions')").removeClass("disabled");
  }
  $('#height_option_program .variation_option').each(function (){
    $(this).removeClass('active');
  });
  $('#height_option_program option:selected').toggleClass('active');
 });
 $(document).on("change",'#height_option_private-sessions-adults',function(){
  $('#height_option_private-sessions-adults .variation_option').each(function (){
    $(this).removeClass('active');
  });
  $('#height_option_private-sessions-adults option:selected').toggleClass('active');
 });
 if($("#height_option_private-sessions-adults").length && !$('#height_option_tennis-academy-groups').val().includes('Private')){
  $('#height_option_private-sessions-adults .variation_option').each(function (){
    // $(this).removeClass('active');
      $(this).attr('data-value','');
    });
  $("#height_option_private-sessions-adults").parent().parent().hide();
 }
 $(document).on("change",'#height_option_tennis-academy-groups',function(){
  var val  = $(this).val();
  if(val.includes('Private')){
    $("#height_option_number-of-sessions option:contains('Private')").attr("disabled","disabled");
    $("#height_option_number-of-sessions option:contains('Private')").addClass("disabled");
    $("#height_option_private-sessions-adults").parent().parent().show();
    $('#height_option_private-sessions-adults .variation_option').each(function (){
      $(this).attr('data-value',$(this).attr('data-tmp'));
    });
  }
  else{
    $("#height_option_number-of-sessions option:contains('Private')").removeAttr('disabled');
    $("#height_option_number-of-sessions option:contains('Private')").removeClass("disabled");
    $("#height_option_private-sessions-adults").parent().parent().hide();
    $('#height_option_private-sessions-adults .variation_option').each(function (){
      $(this).attr('data-value','');
    });
  }
  $('#height_option_tennis-academy-groups .variation_option').each(function (){
    $(this).removeClass('active');
  });
  $('#height_option_tennis-academy-groups option:selected').toggleClass('active');
 });
 $(document).on("change",'#height_option_tennis-academy-groups',function(){
  $('#height_option_tennis-academy-groups .variation_option').each(function (){
    $(this).removeClass('active');
  });
  $('#height_option_tennis-academy-groups option:selected').toggleClass('active');
 });
 $(document).on("change",'#height_option_child-age-group',function(){
  $('#height_option_child-age-group .variation_option').each(function (){
    $(this).removeClass('active');
  });
  $('#height_option_child-age-group option:selected').toggleClass('active');
 });
 $(document).on("change",'#height_option_week-reservation',function(){
  $('#height_option_week-reservation .variation_option').each(function (){
    $(this).removeClass('active');
  });
  $('#height_option_week-reservation option:selected').toggleClass('active');
 });
 if($("#height_option_week-reservation").length && !$('input[name=pass-type-radio]').val().includes('weekly')){
  $('#height_option_week-reservation .variation_option').each(function (){
    // $(this).removeClass('active');
      $(this).attr('data-value','');
    });
  $("#height_option_week-reservation").parent().parent().hide();
 }
 $(document).on("change",'input[name=pass-type-radio]',function(){
  var val  = $(this).val();
  if(val.includes('weekly')){
    $("#height_option_week-reservation").parent().parent().show();
    $('#height_option_week-reservation .variation_option').each(function (){
      $(this).attr('data-value',$(this).attr('data-tmp'));
    });
  }
  else{
    $("#height_option_week-reservation").parent().parent().hide();
    $('#height_option_week-reservation .variation_option').each(function (){
      $(this).attr('data-value','');
    });
  }
 });
 $(document).on("change",'#height_option_swimming-sessions',function(){
  $('#height_option_swimming-sessions .variation_option').each(function (){
    $(this).removeClass('active');
  });
  $('#height_option_swimming-sessions option:selected').toggleClass('active');
 });
 $(document).on("change",'#height_option_session-type',function(){
  $('#height_option_session-type .variation_option').each(function (){
    $(this).removeClass('active');
  });
  $('#height_option_session-type option:selected').toggleClass('active');
 });
 $(document).on("change",'#height_option_number-of-sessions',function(){
  $('#height_option_number-of-sessions .variation_option').each(function (){
    $(this).removeClass('active');
  });
  $('#height_option_number-of-sessions option:selected').toggleClass('active');
 });
 $(document).on("change",'input[name=pass-type-radio]',function(){
   var class_name = '';
  if($('.section_info.pass-type').length){
      class_name = 'pass-type';
  }else if($('.section_info.session').length){
    class_name = 'session';
  }
  $('.'+class_name+' .variation_option').each(function (){
    $(this).removeClass('active');
  });
  if(class_name=='session'){
    if($('input[name=pass-type-radio]:checked').val()!='1-session'){
      $('.section_info.time_slots').hide();
      $('.middle .date').hide();
      $('#billing_date_time').val("");
      // $('.terms-rules').show();
    }
    else{
      $('.section_info.time_slots').show();
      $('.middle .date').show();
      if($('#billing_date_time').length){
      var today = new Date().toISOString().split('T')[0];
      document.getElementById("billing_date_time").setAttribute("min", today);
      $('#billing_date_time').val(today);
      }
      $('.terms-rules').hide();
    }
  }
  $('.'+class_name+' .variation_option:checked').toggleClass('active');
    get_availablility_variable_product($('.single_page').attr('data-id'),$('.single_page').attr('data-form'),'');
});
$(document).on("change",'input[name=time-slots-radio]',function(){
  $('.time-slots .variation_option').each(function (){
    $(this).removeClass('active');
    $(this).parent().parent().removeClass('select');
  });
  $('.time-slots .variation_option:checked').toggleClass('active');
  $('.time-slots .variation_option:checked').parent().parent().toggleClass('select')
  if($('#billing_date_time').length){
    // get_availablility_variable_product($('.single_page').attr('data-id'),$('.single_page').attr('data-form'),$('#billing_date_time').val());
  }
});
$(document).on("change",'#interested_in',function(){
  if($('#billing_date_time').length){
    get_availablility_variable_product($(this).val(),$('.single_page').attr('data-form'),$('#billing_date_time').val());
  }
});
 
//  $(document).on("change",'#height_option_weeks',function(){
//   $('#height_option_weeks option:selected').removeClass('active');
//   $('#height_option_weeks option:selected', this).addClass('active');
//  });

  function get_availablility_variable_product(product_id,product_form,billing_date_time){
    $('#ajax_loader').show();
    if($('.section_info.session').length){
      if($('input[name=pass-type-radio]:checked').val()!='1-session'){
        $('.time-slots .variation_option').each(function (){
          $(this).removeClass('active');
          $(this).parent().parent().removeClass('select');
        });
        $('.time-slots-content').append('<div class="single" style="display: none;"><div class="box"><input type="radio"  class="variation_option" id="8sessionvar" name="time-slots-radio" value="academy" data-value="academy" data-key="attribute_pa_time-slots"><label for="8sessionvar"></label></div></div>');
        $('#8sessionvar').click();
      }
      else{
        $('#8sessionvar').parent().parent().remove();
        $("input[name='time-slots-radio']:first").click();
      }
    }
    setTimeout(() => {
      var var_items = jQuery('.variation_option.active').map(function() {
        var key       = $(this).attr('data-key');
        var item_arr  = new Object();
        item_arr[key] = $(this).attr('data-value');
        return item_arr;
      }).get();
    console.log(var_items);
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: mitch_ajax_url,
      data: {
        action: "get_availablility_variable_product",
        product_id: product_id,
        selected_items: var_items,
        product_form: product_form,
        date_time:billing_date_time,
      },
      success: function (data) {
        $('#ajax_loader').hide();
        // $('#number').val('1');
        // $('#increase').removeClass('disabled');
        // $('#number').attr('data-max','');
        if(data.time_slots_html){
          $('.time-slots-content').html(data.time_slots_html);
          $('#billing_date_time').change();
        }
        if(data.price){
        $('.variable_middle .price').html(data.price);
        }
        // if(data.quantity && data.quantity!==-1){
        //   $('#number').attr('data-max',data.quantity);
        // }
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        //alert("Error:" + errorThrown); //"Status: " + textStatus +
        $('#ajax_loader').hide();
        Swal.fire({
          title: 'Something went wrong',
          text: errorThrown,
          icon: 'error',
          showConfirmButton: false,
          timer: 1500
        });
      }
    });
  });
  }
  // function customized_product_add_to_cart(attrs_count, parent_id){
  //   $(document).on('click', '#customized_product_add_to_cart', function(){
  //     $('#ajax_loader').show();
  //     // var attrs_count    = attrs_count;
  //     // var parent_id      = parent_id;
  //     var selected_items = jQuery('.single_box.active').map(function() {
  //       return 1;
  //     }).get();
  //     if(getCookie('custom_product_visit_type')){
  //       if(selected_items.length == attrs_count){
  //         var variations_ids = jQuery('.single_box.active').map(function() {
  //           return jQuery(this).data('variation-id');
  //         }).get();
  //         var attributes_keys = jQuery('.single_box.active').map(function() {
  //           return jQuery(this).data('variation-attribute-key');
  //         }).get();

  //         var attributes_vals = jQuery('.single_box.active').map(function() {
  //           return jQuery(this).data('variation-attribute-val');
  //         }).get();
  //         $.ajax({
  //           type: 'POST',
  //           url: mitch_ajax_url,
  //           dataType: 'JSON',
  //           data: {
  //             action: "customized_product_add_to_cart",
  //             parent_id: parent_id,
  //             variations_ids: variations_ids,
  //             attributes_keys: attributes_keys,
  //             attributes_vals: attributes_vals,
  //             visit_type: getCookie('custom_product_visit_type'),
  //             visit_branch: getCookie('custom_product_branch_visit'),
  //             visit_home: getCookie('custom_product_home_visit_time')
  //           },
  //           success: function (data) {
  //             $('#ajax_loader').hide();
  //             $('#cart_total_count').html(data.cart_count);
  //             $('#side_mini_cart_content').html(data.cart_content);
  //             // alert('تم اضافة المنتج لسلة المشتريات بنجاح.');
  //             // Swal.fire({
  //             //   title: 'تم بنجاح',
  //             //   text: 'اضافة المنتج الي سلة المشتريات',
  //             //   icon: 'success',
  //             //   showConfirmButton: false,
  //             //   timer: 1500
  //             // });
  //             if(data.redirect_to){
  //               window.location.replace(data.redirect_to);
  //             }
  //           },
  //           error: function(XMLHttpRequest, textStatus, errorThrown) {
  //             $('#ajax_loader').hide();
  //             // alert("Error:" + errorThrown); //"Status: " + textStatus +
  //             Swal.fire({
  //               title: 'Something went wrong',
  //               text: errorThrown,
  //               icon: 'error',
  //               showConfirmButton: false,
  //               timer: 1500
  //             });
  //           }
  //         });
  //       }else{
  //         $('#ajax_loader').hide();
  //         Swal.fire({
  //           title: 'Something went wrong',
  //           text: 'يجب ان تقوم بأختيار اختيار واحد من كل خطوة!',
  //           icon: 'error',
  //           showConfirmButton: true,
  //           // timer: 1500
  //         });
  //       }
  //     }else{
  //       $('#ajax_loader').hide();
  //       Swal.fire({
  //         title: 'Something went wrong',
  //         text: 'يجب ان تختار نوع الزيارة في اول خطوة لمعرفة مقاس الثوب!',
  //         icon: 'error',
  //         showConfirmButton: true,
  //         // timer: 1500
  //       });
  //     }
  //     //alert(variations_ids);
  //   });
  // }
  $(document).on("click",'.nav_single_phases',function(){
      var target = $(this).attr('data-target');
      var pid = $(this).attr('data-pid');
      // $('.single_phases.active .link').attr('data-id',target);
      var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?uid='+target;
      // get_availablility_variable_product(pid); //hidden for now
      window.history.pushState({path:newurl},'',newurl);
  });
  $('#reviews_form').on('submit', function (e) {
    e.preventDefault();
    var element_id = 'reviews_form_alerts';
    var error_msg  = '';
    // if($("select[name='rating']").val() == ''){
    //   var error_msg = 'برجاء اختيار تقييم!';
    // }
    if($("input[name='email']").val() == ''){
      error_msg += '<span>Please enter your email address!</span>';
      $("input[name='email']").addClass('required');
    }
    else{
      $("input[name='email']").removeClass('required');
    }
    if($("input[name='fname']").val() == ''){
      error_msg += '<span>Please enter your first name!</span>';
    }
    else{
      $("input[name='fname']").removeClass('required');
    }
    if($("input[name='lname']").val() == ''){
      error_msg += '<span>Please enter your last name!</span>';
    }
    else{
      $("input[name='lname']").removeClass('required');
    }
    if($("input[name='number']").val() == ''){
      error_msg += '<span>Please enter your phone number!</span>';
    }
    else{
      $("input[name='number']").removeClass('required');
    }
    if(error_msg == ''){
      mitch_ajax_request(mitch_ajax_url, 'mitch_make_product_review', $(this).serialize(), element_id, 'popup');
      this.reset();
    }else{
      $('#'+element_id).html('');
      $('#'+element_id).append('<div class="alert alert-danger">'+error_msg+'</div>').show('slow');
      $('#'+element_id).show('slow');
      // window.scrollTo(0, 0);
      // $("html, body").animate({ scrollTop: 0 }, "slow");
    }
  });
  $('#phases_form').on('submit', function (e) {
    e.preventDefault();
    var element_id = 'phases_form_alerts';
    var error_msg  = '';
    if($("#phases_form input[name='email']").val() == ''){
      error_msg += '<span>Please enter email</span>';
    }
    if($("#phases_form input[name='fname']").val() == ''){
      error_msg += '<span>Please enter first name</span>';
    }
    if($("#phases_form input[name='lname']").val() == ''){
      error_msg += '<span>Please enter last name</span>';
    }
    if($("#phases_form input[name='number']").val() == ''){
      error_msg += '<span>Please enter phone number</span>';
    }
    if($("#phases_form input[name='time-slots-radio']").val() == ''){
      error_msg += '<span>Please choose time slot</span>';
    }
    if($("#phases_form input[name='date']").val() == ''){
      error_msg += '<span>Please choose date</span>';
    }
    if($(".variation_option.active").val() == ''){
      error_msg += '<span>No time slots avilable in this date</span>';
    }
    if(error_msg == ''){
      // var product_id = $("input[name='time-slots-radio']:checked").attr('data-pid');
      var product_id = $("#interested_in").val();
      var variation_id = $("input[name='time-slots-radio']:checked").attr('data-variation_id');
      var formData = $(this).serialize();
      $.ajax({
        type: "POST",
        url: mitch_ajax_url,
        data: {
          action: "mitch_make_phase_review",
          form_data: formData,
          variation_id: variation_id,
          product_id: product_id,
        },
        success: function (data) {
          $result = JSON.parse(data);
          $('#ajax_loader').hide();
          $('#phases_form .fade-content').hide();
          if($result['status'] == 'success'){
              if($result['msg']){
                $('#'+element_id).html('<div class="alert alert-success">'+$result['msg']+'</div>');
                $('#'+element_id).show('slow');
                $("html, body").animate({ scrollTop: 0 }, "slow");
              }
          }
        },
        });
      // mitch_ajax_request(mitch_ajax_url, 'mitch_make_phase_review', formData, element_id, 'popup');
      this.reset();
    }else{
      $('#'+element_id).html('');
      $('#'+element_id).append('<div class="alert alert-danger">'+error_msg+'</div>').show('slow');
      $('#'+element_id).show('slow');
      // window.scrollTo(0, 0);
      // $("html, body").animate({ scrollTop: 0 }, "slow");
    }
  });

  function bought_together_products_add_to_cart(){
    var element_id   = 'single_product_alerts';
    var products_ids = jQuery('.active-item.single_item').map(function() {
      return $(this).data('id');
    }).get();
    mitch_ajax_request(mitch_ajax_url, 'mitch_bought_together_products', {products_ids: products_ids}, element_id, 'none');
  }
  function bought_item_change(product_id, main_price, variation_price){
    // alert(product_id);
    //$('#' + id).is(":checked")
    var total_bought = parseFloat($('#total_bought').html());
    if($('#btcheck_'+product_id).is(":checked")){
      $('.bought_product_item_'+product_id).show('slow');
      $('.bought_product_item_'+product_id).addClass('active-item');
      var total_bought_after = total_bought + variation_price;
    }else{
      $('.bought_product_item_'+product_id).hide('slow');
      $('.bought_product_item_'+product_id).removeClass('active-item');
      var total_bought_after = total_bought - variation_price;
    }
    $('#total_bought').html(total_bought_after);
  }
}else{
  alert('Sorry Please reload page!');
}

jQuery(".sort").on("change", function () {
  $posts_per_page = 20;
  $(".sortby.active").removeClass("active");
  $(".products").data("page", 1);
  $(this).addClass("active");
  let option = $(this).val();
  $(".products").attr("data-sort", option);
  get_products_ajax("sort", "desktop");
  return false;
});

jQuery(window).scroll(function () {
  if ($(".spinner").is(":visible")) {
    if ($(".product_widget").length) {
      Footeroffset = jQuery(".product_widget").last().offset().top;
    }
    winPosBtn = jQuery(window).scrollTop();
    winH = jQuery(window).outerHeight();
    if (winPosBtn + winH > Footeroffset + 5) {
      get_products_ajax("loadmore");
      //console.log("read");
    }
  }
});
jQuery(document).on("change", ".filter_input", function () {
  $(".spinner").show();
  $(".products").data("page", 1);
  get_products_ajax("filter", "desktop");
});

// load more on scroll and click, filter, and sort
$posts_per_page = 20;
$loading_more = false;
var jqxhr_add_get_products_ajax = {abort: function () {}};
function get_products_ajax(action, view = "") {
//console.log("called get_products_ajax");
// jqxhr_add_get_products_ajax.abort();
var ajax_url = mitch_ajax_url;
$count = $(".products").attr("data-count");
$page = $(".products").attr("data-page");
$posts = $(".products").attr("data-posts");
$order = $(".products").attr("data-sort");
$type = $(".products").attr("data-type");
$search = $(".products").attr("data-search");
$lang = $(".products-list").attr("data-lang");
$slug = "";
$cat = "";
$ids = new Array();
if ($type == "shop") {
} else if ($type == "products-list") {
  $ids = $(".products").data("ids");
} else {
  $slug = $(".products").data("slug");
  $cat = $(".products").data("cat");
}

let min_price = "";
let max_price = "";
let max_prices = new Array();
let min_prices = new Array();
let brand = new Array();
let labels = new Array();
let cats = new Array();
let collections = new Array();

$(".filter_input:checked").each(function () {
  if ($(this).hasClass("filter-price")) {
    min_prices.push(parseInt($(this).data("min")));
    max_prices.push(parseInt($(this).data("max")));
    max_price =
      parseInt($(this).data("max")) == 0
        ? parseInt($(this).data("max"))
        : Math.max(...max_prices);
    min_price = Math.min(...min_prices);
    $order = "price";
  } else if ($(this).hasClass("filter-brand")) {
    brand.push($(this).val());
  } else if ($(this).hasClass("filter-cat")) {
    cats.push($(this).val());
  }else if ($(this).hasClass("filter-label")) {
    labels.push($(this).val());
  } else if ($(this).hasClass("filter-collections")) {
    collections.push($(this).val());
  }
});


if (($loading_more || $posts_per_page >= $posts) && action == "loadmore") {
  // console.log("khalstt " + $posts);
  return;
}
$loading_more = true;
jqxhr_add_get_products_ajax = $.ajax({
  type: "POST",
  url: ajax_url,
  data: {
    action: "get_products_ajax",
    count: $count,
    page: $page,
    order: $order,
    type: $type,
    slug: $slug,
    min_price: min_price,
    max_price: max_price,
    brand: brand,
    label: labels,
    collections: collections,
    cat: $cat,
    cats: cats,
    search: $search,
    fn_action: action,
    ids: $ids,
  },
  success: function (posts) {
    get_products_ajax_count(action);
    $loading_more = false;
    if (action == "loadmore") {
      $(".products").append(posts);
      $(".products").attr("data-page", parseInt($page) + 1);
      $(".spinner").attr("data-page", parseInt($page) + 1);
      //console.log($(".products").attr("data-page"));
      $posts_per_page += parseInt($count);
      $posts = $(".products").attr("data-posts");
      //console.log("$posts_per_page", $posts_per_page);
      //console.log("$posts", $posts);
      if ($posts_per_page >= $posts) {
        /// Begin of get out of stock products function
        $(".spinner").hide();
      } else {
        if ($posts_per_page < $posts) {
          $(".spinner").show();
        }
      }
    } else {
      $(".products").html(posts);
      if (parseInt($page) % 2 == 0 && $posts_per_page < $posts) {
        $(".spinner").show();
      } else if (parseInt($page) % 2 == 1 && $posts_per_page < $posts) {
        $(".spinner").show();
      } else if ($posts_per_page >= $posts) {
        /// Begin of get out of stock products function
        $(".spinner").hide();
      }
    }
  },
});
}
var jqxhr_add_get_products_ajax_count = {abort: function () {}};
function get_products_ajax_count(view) {
jqxhr_add_get_products_ajax_count.abort();
// console.log('get_products_ajax_count');
var ajax_url = mitch_ajax_url;
$count = $(".products").attr("data-count");
$page = $(".products").attr("data-page");
$posts = $(".products").attr("data-posts");
$order = $(".products").attr("data-sort");
$type = $(".products").attr("data-type");
$search = $(".products").attr("data-search");

$slug = "";
$cat = "";
$ids = new Array();
if ($type == "shop") {
} else if ($type == "products-list") {
  $ids = $(".products").data("ids");
} else {
  $slug = $(".products").data("slug");
  $cat = $(".products").data("cat");
}

let min_price = "";
let max_price = "";
let max_prices = new Array();
let min_prices = new Array();
let brand = new Array();
let labels = new Array();
let collections = new Array();
let cats = new Array();

$(".filter_input:checked").each(function () {
  if ($(this).hasClass("filter-price")) {
    min_prices.push(parseInt($(this).data("min")));
    max_prices.push(parseInt($(this).data("max")));
    max_price =
      parseInt($(this).data("max")) == 0
        ? parseInt($(this).data("max"))
        : Math.max(...max_prices);
    min_price = Math.min(...min_prices);
    $order = "price";
  } else if ($(this).hasClass("filter-brand")) {
    brand.push($(this).val());
  } else if ($(this).hasClass("filter-cat")) {
    cats.push($(this).val());
  } else if ($(this).hasClass("filter-label")) {
    labels.push($(this).val());
  } else if ($(this).hasClass("filter-collections")) {
    collections.push($(this).val());
  }
});
setTimeout(function () {
  jqxhr_add_get_products_ajax_count = $.ajax({
    type: "POST",
    url: ajax_url,
    data: {
      action: "get_products_ajax_count",
      count: $count,
      page: $page,
      order: $order,
      type: $type,
      slug: $slug,
      min_price: min_price,
      max_price: max_price,
      brand: brand,
      label: labels,
      collections: collections,
      cat: $cat,
      search: $search,
      cats: cats,
      ids: $ids,
    },
    success: function (posts) {
      // console.log('posts', posts);
      if (20 >= parseInt(posts)) {
        $(".spinner").addClass("hide");
      } else if (parseInt(posts) == 0) {
        $(".spinner").removeClass("hide");
      } else {
        $(".spinner").removeClass("hide");
      }
      $(".products").attr("data-posts", posts);
      $(".spinner").attr("data-posts", posts);
    },
  });
});
}

$("#billing_state,#country").on("change", function () {
  var urlParams = new URLSearchParams(window.location.search);
  let state = $(this).val();
  let city = $("#billing_city").val();
  $("#billing_city_field").addClass("blocked");
  // $("#billing_area_field").addClass("blocked");
  let lang = "";
  if (urlParams.has("lang")) {
    lang = urlParams.get("lang");
  }

  $.ajax({
    type: "POST",
    url: mitch_ajax_url,
    data: {
      action: "get_city",
      state: state,
      lang: lang,
    },
    success: function (posts) {
      if(window.location.href.indexOf('addresses')>-1){
      $("#city").html(posts);
      }else{
      $("#billing_city_field").html(posts);
      }
      $("#billing_city_field").removeClass("blocked");
    },
  });
 
});
if(window.location.href.indexOf('checkout')>-1){
  $("#billing_state").change();
}
if(window.location.href.indexOf('my-account/addresses')>-1){
  $("#country").change();
}

if ($(".new_search").length) {
  var jqxhr_add = {abort: function () {}};
  var lang = "";
  if (
    /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
      navigator.userAgent
    )
  ) {
    // some code..
  } else {
    window.addEventListener("click", function (e) {
      if (document.getElementById("newSearch").contains(e.target)) {
        if ($(".new-search").val()) {
          $(".search-result").addClass("show");
          $(".sec_search").addClass("show");
          $('html').addClass('no-scroll');
        }
      } else {
        $(".search-result").removeClass("show");
        $(".sec_search").removeClass("show");
        $('html').removeClass('no-scroll');
      }
    });
    $("#newSearch").on("focus", function () {
      if (!$(".search-result").hasClass("show")) {
        if ($(".new-search").val().length >= 1) {
          $(".search-result").addClass("show");
          $(".sec_search").addClass("show");
        }
      }
    });
  }
  jQuery($(".new-search")).keyup(function () {
    jqxhr_add.abort();
    if ($(".search-result").length) {
      $(".search-result").html("");
      if ($(".search-result").hasClass("show")) {
        $(".search-result").removeClass("show");
        $(".sec_search").removeClass("show");
      }
      if ($(".new-search").val().length >= 1) {
        $(".loader_search").show();
        jqxhr_add = $.ajax({
          type: "POST",
          url: mitch_ajax_url,
          data: {
            action: "custom_search",
            s: $(".new-search").val(),
            // lang: lang,
          },
          success: function (data) {
            if (data) {
              $(".search-result").addClass("show");
              $(".sec_search").addClass("show");
              $(".loader_search").hide();
              $(".search-result").html(data);
            }
          },
        });
      }
    }
  });
}
function navigateMyForm() {
  var lang = "";
  var urlParams = new URLSearchParams(window.location.search);
  if (urlParams.has("lang")) {
    lang = "_" + urlParams.get("lang");
  }
  var s = $(".search-formm .new-search").val();
  if (lang == "_en") {
    window.location.href =
      "http://139.162.130.202:50/search/?search=" + s + "&lang=en";
  } else {
    window.location.href =
      "http://139.162.130.202:50/search/?search=" + s;
  } //other stuff you want to do instead...
  return true;
}

function increaseValue() {
  var value = parseInt(document.getElementById('number').value, 10);
  value = isNaN(value) ? 0 : value;
  if(document.getElementById('number').value!==$('#number').attr('data-max')){
  value++;
  }
  else{
      $('#increase').addClass('disabled');
  }
  document.getElementById('number').value = value;
}

function decreaseValue() {
  var value = parseInt(document.getElementById('number').value, 10);
  value = isNaN(value) ? 1 : value;
  value < 2 ? value = 2 : '';
  value--;
  document.getElementById('number').value = value;
}
function increaseValueByID(element_id) {
  var value = parseInt(document.getElementById(element_id).value, 10);
  value = isNaN(value) ? 0 : value;
  value++;
  document.getElementById(element_id).value = value;
}

function decreaseValueByID(element_id) {
  var value = parseInt(document.getElementById(element_id).value, 10);
  value = isNaN(value) ? 1 : value;
  value < 2 ? value = 2 : '';
  value--;
  document.getElementById(element_id).value = value;
}




var xmlHttp;
function srvTime() {
  try {
    //FF, Opera, Safari, Chrome
    xmlHttp = new XMLHttpRequest();
  } catch (err1) {
    //IE
    try {
      xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (err2) {
      try {
        xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
      } catch (eerr3) {
        //AJAX not supported, use CPU time.
        alert("AJAX not supported");
      }
    }
  }
  xmlHttp.open("HEAD", window.location.href.toString(), false);
  xmlHttp.setRequestHeader("Content-Type", "text/html");
  //xmlHttp.send("");
  return xmlHttp.getResponseHeader("Date");
}

var st = srvTime();
const date = new Date(st);
var new_date_day = date.getDate();
const monthNames = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];
var day = date.getDay();
var month = date.getMonth();
var year = date.getFullYear();
var actual_month = date.getMonth() + 1;
var maxDate = year + '-' + month + '-' + day;
// alert(maxDate);
$('#date').attr('min', maxDate);
var new_date = actual_month+ "-" + date.getDate() + "-" + date.getFullYear();
var excluded_dates = $("#single_page").attr("data-exDates");
if($("#billing_date_time_select_picker").length){
$("#billing_date_time_select_picker").datepicker({
  //format: 'dd/mm/yyyy',
  dateFormat: "mm/dd/yy",
  yearRange: "-100:+20",
  minDate: "2",
  beforeShowDay: disableAllTheseDays,
  onSelect: function () {
    $("#billing_date_time").val($(this).val());
    console.log("selectedDateDelivery: "+$(this).val());
  },
});

function disableAllTheseDays(date) {
  if (excluded_dates) {
    var jsonData_excluded_days = JSON.parse(excluded_dates);
    if ((strTime == cut_off_12 && ampm == "pm") || today >= cut_off) {
      //console.log('heree');
      jsonData_excluded_days.unshift(new_date_new_format);
    }
    //console.log(jsonData_excluded_days);
    for (var i = 0; i < jsonData_excluded_days.length; i++) {
      var counter = jsonData_excluded_days[i];
      //console.log(counter);
    }
    var disabledDays = jsonData_excluded_days;
  } else {
    var disabledDays = ["05-13-2021"];
  }
  var disabledWeekdays = [5];
  var m = date.getMonth(),
    d = date.getDate(),
    y = date.getFullYear();
  for (i = 0; i < disabledDays.length; i++) {
    if ($.inArray(m + 1 + "-" + d + "-" + y, disabledDays) != -1) {
      return [false];
    }
  }
  var currentdate =
    date.getMonth() + 1 + "-" + date.getDate() + "-" + date.getFullYear();
  if ($.inArray(currentdate, disabledDays) > -1) {
    return [false];
  }
  if ($.inArray(date.getDay(), disabledWeekdays) > -1) {
    return [false];
  }
  return [true];
}
function getMinDate() {
  var date = new Date(st);
  while (!disableAllTheseDays(date)[0]) {
    date.setDate(date.getDate() + 1);
  }
  return date;
}
var new_result_2 = getMinDate();
var time_slots = '';
if (excluded_dates) {
  var x_month = new_result_2.getMonth() + 1;
  if (new_date_day == new_result_2.getDate()) {
    var twodays = new_result_2.getDate() + 2;
    new_result = x_month + "-" + twodays + "-" + new_result_2.getFullYear();
  } else {
    new_result =
      x_month +
      "-" +
      new_result_2.getDate() +
      "-" +
      new_result_2.getFullYear();
  }
  if($('.time_slots').length){
    $('.time_slots .variation_option.active').each(function (){
      time_slots += new_result+' '+$(this).attr("data-name")+',';
    });
    $("#billing_date_time").val(time_slots);
  }
  else{
    $("#billing_date_time").val(new_result);
  }
} else {
  if($('.time_slots').length){
    $('.time_slots .variation_option.active').each(function (){
      time_slots += new_date+' '+$(this).attr("data-name")+',';
    });
    $("#billing_date_time").val(time_slots);
  }
  else{
    $("#billing_date_time").val(new_date);
  }
}
}

$(window).bind("load",function(){
  if($('#billing_date_time').length){
  setTimeout(() => {
    $(document).on("change",'#billing_date_time',function() {
      var date = $(this).val();
      var product_form = $('.single_page').attr('data-form');
      var product_id = $('.single_page').attr('data-id');
      let variations_arr = new Array;
      var action_fn = '';
      if($('.time_slots .variation_option').length){
        action_fn = 'get_available_time_slots';
      $('.time_slots .variation_option').each(function (){
      var valueToPush = { }; 
        valueToPush["name"] = $(this).attr('data-name');
        valueToPush["data_key"] = $(this).attr('data-key');
        valueToPush["data_variation_id"] = $(this).attr('data-variation_id');

    valueToPush["data_id"] = $(this).attr('id');
    valueToPush["data_value"] = $(this).val();
        variations_arr.push(valueToPush);
      }); console.log(variations_arr);
    }
    else{
      action_fn = 'get_available_date';
    }
      $.ajax({
        type: 'POST',
        // dataType: 'JSON',
        url: mitch_ajax_url,
        data: {
          action: action_fn,
          date: date,
          variations_arr: variations_arr,
          product_form: product_form,
          product_id: product_id,
        },
        success: function (data) {
          //alert('form was submitted');
          console.log(data);
          $('#ajax_loader').hide();
          $('.time-slots-content').html(data);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
          // alert("Error:" + errorThrown); //"Status: " + textStatus +
          $('#ajax_loader').hide();
          Swal.fire({
            title: 'Something went wrong',
            text: errorThrown,
            icon: 'error',
            showConfirmButton: false,
            timer: 1500
          });
        }
      });
    });
    const picker = document.getElementById('billing_date_time');
    var today = new Date().toISOString().split('T')[0];
    // document.getElementsByName("date")[0].setAttribute('min',today);
    document.getElementById("billing_date_time").setAttribute("min", today);
    $('#billing_date_time').val(today);
    picker.addEventListener('input', function(e){
      var day = new Date(this.value).getUTCDay();
      var month = new Date(this.value).getUTCMonth();
      var year = new Date(this.value).getUTCFullYear();
      var name_date = new Date(this.value).getUTCDate();
      day = day+1;
      month = month+1;
      var excluded_dates = $(".single_page").attr("data-exdates");
      if (excluded_dates) {
        var jsonData_excluded_days = JSON.parse(excluded_dates);
        for (var i = 0; i < jsonData_excluded_days.length; i++) {
          var counter = jsonData_excluded_days[i];
          var ex_day = new Date(counter).getUTCDay();
          var ex_month = new Date(counter).getUTCMonth();
          var ex_year = new Date(counter).getUTCFullYear();
          var ex_year = new Date(counter).getUTCFullYear();
          var ex_name_date = new Date(counter).getUTCDate();
          ex_day = ex_day+1;
          ex_month = ex_month+1;
          console.log(ex_day+' '+ex_month+' '+ex_year);
          if(day==ex_day && month == ex_month && year == ex_year && name_date == ex_name_date){
              e.preventDefault();
              this.value = '';
              alert('Already Booked');
          }          
        }
      }
      console.log(day+' '+month+' '+year)
      
      // if([6,0].includes(day)){
      //   e.preventDefault();
      //   this.value = '';
      //   alert('Weekends not allowed');
      // }
    });
    $('#billing_date_time').change();
  });
}
if(window.location.href.indexOf('checkout')>-1){
  setTimeout(() => {
  $('#payment_method_nodepayment').change();
    
  }, 1000);
}
});