<?php require_once 'header.php';?>
<link rel='stylesheet' id='cf-front-css'  href='<?php echo $theme_settings['site_url'];?>/wp-content/plugins/caldera-forms/assets/build/css/caldera-forms-front.min.css?ver=1.9.6' type='text/css' media='all' />
<link rel='stylesheet' id='cf-render-css'  href='<?php echo $theme_settings['site_url'];?>/wp-content/plugins/caldera-forms/clients/render/build/style.min.css?ver=1.9.6' type='text/css' media='all' />
<div id="page" class="site" style="min-height: 1000px;">
  <?php require_once 'theme-parts/main-menu.php'; 
$page_content = get_field('contact_page');?>
  <!--start page-->
  <div class="site-content contact_us">
        <div class="grid">
             <div class="sec_form">
                <div class="section_title">
                    <div class="sec_img_contact">
                        <img src="<?php echo $page_content['hero_section']['image'];?>" alt="">
                        <div class="hover_box">
                            <div class="box">
                                <h2><?php echo $page_content['hero_section']['title'];?></h2>
                                <p><?php echo $page_content['hero_section']['subtitle'];?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form_content">
                    <h3><?php echo $page_content['hero_section']['form_title'];?></h3>
                    <p><?php echo $page_content['hero_section']['form_subtitle'];?></p>
                    <?php echo do_shortcode('[caldera_form id="CF6241f70f2e55a"]');?>
                </div>
             </div>
            <div class="section_branches">
              <div class="all_branches">
                <div class="branches">
                  <div class="col">
                    <h6><span class="material-icons-sharp">email</span><?php echo $page_content['branches_section']['email_text'];?></h6>
                    <p> <?php echo $page_content['branches_section']['email'];?></p>
                  </div>
                  <div class="col">
                    <h6><span class="material-icons-sharp">phone</span><?php echo $page_content['branches_section']['hotline_text'];?></h6>
                    <p>  <?php echo $page_content['branches_section']['hotline'];?></p>
                  </div>
                  <div class="col">
                    <h6><span class="material-icons-sharp">fmd_good</span><?php echo $page_content['branches_section']['address_text'];?></h6>
                    <p><?php echo $page_content['branches_section']['address_title'];?></p>
                    <a target="_blank" href="<?php echo $page_content['branches_section']['address_url'];?>">
                        <span class="material-icons-sharp">near_me</span>
                        GET DIRECTIONS
                    </a>
                  </div>


                 
                  <div class="single_branche">
                        <h4><span class="material-icons-sharp">home</span><?php echo 'Sales Office';?></h4>  

                        <ul>
                          <?php if($page_content['branches_section']['Offices']): foreach($page_content['branches_section']['Offices'] as $office):?>
                            <li class="pin_drop">
                                <p><?php echo $office['location_text'];?></p>
                                <a target="_blank" href="<?php echo $office['location_url'];?>">
                                    <span class="material-icons-sharp">near_me</span>
                                    GET DIRECTIONS
                                </a>
                            </li>
                          <?php endforeach;endif;?>
                        </ul>
                  </div>      
                
                  </div>
                 
                </div>
                <div class="section_image">
                    <img src="<?php echo $page_content['branches_section']['image'];?>" alt="Almaza Bay" />
                </div>
            </div>
        </div>
  </div>
  <!--end page-->
</div>
<?php require_once 'footer.php';?>
<script type='text/javascript' src='<?php echo $theme_settings['site_url'];?>/wp-content/plugins/caldera-forms/assets/build/js/jquery-baldrick.min.js?ver=1.9.6' id='cf-baldrick-js'></script>
<script type='text/javascript' src='<?php echo $theme_settings['site_url'];?>/wp-content/plugins/caldera-forms/assets/build/js/parsley.min.js?ver=1.9.6' id='cf-validator-js'></script>
<script type='text/javascript' src='<?php echo $theme_settings['site_url'];?>/wp-content/plugins/caldera-forms/clients/render/build/index.min.js?ver=1.9.6' id='cf-render-js'></script>
<script type='text/javascript' src='<?php echo $theme_settings['site_url'];?>/wp-content/plugins/caldera-forms/assets/build/js/caldera-forms-front.min.js?ver=1.9.6' id='cf-form-front-js'></script>
