<?php require_once 'header.php'; global $post; //var_dump($post);?>
<div id="page" class="site">
  <?php require_once 'theme-parts/main-menu.php';?>
  <?php
    $page_content = get_field('page_content');
    ?>
        <div class="site-content page_retreat_list">
            <div class="section_hero">
                <div class="hero_image">
                    <img src="<?php echo $page_content['hero_section']['image'];?>" alt="">
                    <div class="hero_text">
                        <p><?php echo $page_content['hero_section']['title'];?></p>
                        <h1><?php echo $page_content['hero_section']['subtitle'];?></h1>
                    </div>
                </div>
            </div>
            <div class="grid">
                <div class="section_sub_hero">
                    <div class="grid">
                        <h3><?php echo $page_content['escape_section']['title'];?></h3>
                        <p><?php echo $page_content['escape_section']['description'];?></p>
                    </div>
                </div>
                <div class="sec_list">
                    <?php
                        $args = array(
                            'post_type'      => 'page',
                            'posts_per_page' => -1,
                            'post_status' => 'publish',
                            // 'fields' => 'ids',
                            'post_parent'    => get_the_ID(),
                            'order'          => 'ASC',
                            'orderby'        => 'menu_order'
                        );
                        $pages_ids = get_posts($args);
                        if(!empty($pages_ids)){ $count=1;
                            foreach($pages_ids as $page){
                                $page_id = $page->ID;
                                $page_slug = $page->post_name;
                        ?>
                    <div class="single_list <?php echo($count%2==0)?'':'row_reverse';?>">
                        <div class="text">
                            <h3><?php echo get_the_title($page_id);?></h3>
                           <p><?php echo get_the_excerpt($page_id);?></p>
                            <a href="/phase?uid=<?php echo $page_slug;?>" class="link">Explore</a>
                        </div>
                        <div class="img">
                          <img src="<?php echo get_the_post_thumbnail_url($page_id);?>" alt="">
                        </div>
                    </div>
                    <?php $count++; } }?>

                </div>
            </div>
        </div>
</div>
<?php require_once 'footer.php';?>
