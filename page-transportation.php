<?php
require_once 'header.php';
$page_content = get_field('page_content');
?>
<div id="page" class="site">
  <?php require_once 'theme-parts/main-menu.php';?>
 <!--start page-->
 <div class="site-content page_transportation">
            <div class="section_hero">
                <div class="hero_image">
                    <img src="<?php echo $page_content['hero_section']['image'];?>" alt="">
                    <div class="hero_text">
                        <p><?php echo $page_content['hero_section']['title'];?></p>
                        <h1><?php echo $page_content['hero_section']['subtitle'];?></h1>
                    </div>
                </div>
            </div>
            <div class="grid">
                <div class="section_sub_hero">
                        <h3><?php echo $page_content['escape_section']['title'];?></h3>
                        <p><?php echo $page_content['escape_section']['description'];?></p>
                </div>
                <div class="sec_list">
                <?php if($page_content['reservation_section']['reservations']): $count=1; foreach($page_content['reservation_section']['reservations'] as $reservation):?>
                    <div class="single_list <?php echo($count%2==0)?'row_reverse':'';?>">
                        <div class="text">
                            <h3><?php echo $reservation['title'];?></h3>
                           <p><?php echo $reservation['description'];?></p>
                           <?php if($reservation['button']['title']):?>
                           <div class="links">
                                <a href="<?php echo $reservation['button']['url'];?>" class="link "><?php echo $reservation['button']['title'];?></a>
                           </div>
                            <?php endif;?>
                        </div>
                        <div class="img ">
                          <img src="<?php echo $reservation['image'];?>" alt="">
                        </div>
                    </div>
                <?php $count++; endforeach;endif;?>
                </div>
            </div>
        </div>
        <!--end page-->
</div>
<?php require_once 'footer.php';?>
