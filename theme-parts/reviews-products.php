     <div class="section_hotel_reviews section_reviews" style="display: none;">
                <div class="grid">
                    <div class="content">
                        <div class="sec_slider">
                            <div class="slider_img">
                            <?php if($page_content['reviews_section']['gallery']): $count=1; foreach($page_content['reviews_section']['gallery'] as $image):?>
                              <img src="<?php echo $image;?>" alt="">
                              <?php endforeach; endif;?>
                            </div>
                            <div class="text">
                                <h3><?php echo $page_content['reviews_section']['title'];?></h4>
                                <p><?php echo $page_content['reviews_section']['description'];?></p>
                                <?php if($page_content['book_button']['title']): ?>
                                <a class="link" target="_blank" href="<?php echo $page_content['book_button']['url'];?>"><?php echo $page_content['book_button']['title'];?></a>
                                <?php endif;?>
                            </div>
                        </div>
                        <div class="sec_reviews">
                        <?php
                            $five_rating_comments = get_comments(array(
                            'status'     => '1',
                            'post_id'    => get_the_ID(),
                            'meta_query' => array(
                                array(
                                'key' =>'rating',
                                'value' => '5',
                                'compare' => '='
                                )
                            )
                            ));
                            $four_rating_comments = get_comments( array(
                                'status'     => '1',
                                'post_id' => get_the_ID(),
                                'meta_query' => array(
                                    array(
                                    'key' =>'rating',
                                    'value' => '4',
                                    'compare' => '='
                                    )
                                )
                            ) );
                            $three_rating_comments = get_comments( array(
                                'status'     => '1',
                                'post_id' => get_the_ID(),
                                'meta_query' => array(
                                    array(
                                    'key' =>'rating',
                                    'value' => '3',
                                    'compare' => '='
                                    )
                                )
                            ) );
                            $two_rating_comments = get_comments( array(
                                'status'     => '1',
                                'post_id' => get_the_ID(),
                                'meta_query' => array(
                                    array(
                                    'key' =>'rating',
                                    'value' => '2',
                                    'compare' => '='
                                    )
                                )
                            ) );
                            $one_rating_comments = get_comments( array(
                                'status'     => '1',
                                'post_id' => get_the_ID(),
                                'meta_query' => array(
                                    array(
                                    'key' =>'rating',
                                    'value' => '1',
                                    'compare' => '='
                                    )
                                )
                            ) );
                            $all_comments   = get_comments(array('post_id' => get_the_ID(), 'status' => '1'));
                            if(!empty($all_comments)){
                            ?>
                            <div class="all_reviews">
                                <?php
                                foreach($all_comments as $all_comment){
                                ?>
                                <div class="single_reviews">
                                    <div class="info">
                                        <img src="<?php echo get_avatar_url($all_comment->comment_author_email);?>" alt="">
                                        <h5>
                                        <?php echo $all_comment->comment_author; $attended = get_comment_meta( $all_comment->comment_ID, 'attended', true );?>
                                        <?php if($attended):?>
                                            <span><?php echo $attended;?></span>
                                        <?php endif;?>
                                        </h5>
                                    </div>
                                    <div class="review_desc">
                                        <p><?php echo $all_comment->comment_content;?></p>
                                    </div>
                                </div>
                                <?php }?>
                            </div>
                            <?php }?>
                            <div class="new_reviews">
                                <a href="#popup-reviews" class="open-reviews-popup js-popup-opener">Leave Review</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>