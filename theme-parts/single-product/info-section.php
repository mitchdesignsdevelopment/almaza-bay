<?php //$single_product_price = mitch_get_product_price_after_rate($single_product_data['main_data']->get_price());?>
<?php $single_product_price = number_format($single_product_data['main_data']->get_price(),2,'.',','); $product_form = $single_product_data['product_form']->term_id;?>
<div class="sec_info">
      <?php
        //   woocommerce_breadcrumb(array(
        //   'delimiter'   => '',
        //   'wrap_before' => '<ul class="breadcramb">',
        //   'wrap_after'  => '</ul>',
        //   'before'      => '<li>',
        //   'after'       => '</li>',
        //   'home'        => $fixed_string['product_single_breadcrumb']
        // ));
      ?>
      <ul class="breadcramb"><li><a href="<?php echo get_term_link( $single_product_data['product_form'], 'product_form');?>"><?php echo $single_product_data['product_form']->name;?></a></li><li><?php echo $single_product_data['main_data']->name;?></li></ul>
    <div class="top">
        <h3 class="single_title_item">
          <?php echo $single_product_data['main_data']->get_name();?>
        </h3>
        <div class="description">
          <?php echo $single_product_data['main_data']->get_description();?>
        </div>
    </div>
    <div class="<?php echo 'middle';?>">
       <?php
          if($product_form == 78 || $product_form == 80 || $product_form == 79){
            if($product_form == 80):
            ?>
              <div class="col n_people">
                  <p>My Unit Number</p>
                  <input type="text" id="billing_unit_number"  name="unit_number" />
              </div>
            <?php endif;?>
            <?php if(!get_field('hide_date_picker',$single_product_data['main_data']->get_id())):?>
              <div class="col date">
                  <p>Choose Date</p>
                  <input type="date" id="billing_date_time"  name="date">
              </div>
            <?php endif;?>
            <?php
          }
          if($single_product_data['main_data']->get_type() == 'simple'){
            ?>
            <!-- <div class="section_count">
                <button class="increase" id="increase" onclick="increaseValue()" value="Increase Value"></button>
                <input class="number_count" type="number" id="number" value="1" data-min="1" data-max="<?php echo $single_product_data['main_data']->get_max_purchase_quantity();?>" />
                <button class="decrease" id="decrease" onclick="decreaseValue()" value="Decrease Value"></button>
            </div> -->
    </div>
            <div class="min_middle">
            <p class="price"><?php echo $single_product_price;?> <?php echo $theme_settings['current_currency'];?></p>
            <!-- show add to cart when needed -->
             
           
            </div>
            <?php
          }elseif($single_product_data['main_data']->get_type() == 'variable'){
            $default_attributes = $single_product_data['main_data']->get_default_attributes();
            if(empty($default_attributes)){
              $default_attributes = array();
            }
            $variations_attr    = array();
            $product_attributes = $single_product_data['main_data']->get_attributes();
            $product_variations = $single_product_data['main_data']->get_available_variations();
            if(!empty($product_variations)){
              $variation_id_arr = array();
              $regions = array();
              foreach($product_variations as $variation_obj){
                // print_r($variation_obj);
                $variation_id_arr[] = $variation_obj['variation_id'];
                foreach($variation_obj['attributes'] as $var_attr_key => $var_attr_value){
                  $variations_attr[] = mitch_get_product_attribute_name($var_attr_value);
                $regions[$var_attr_value][$variation_obj['attributes']['attribute_pa_time-slots']] = $variation_obj['variation_id'];
                }
              }
            }
            if(!empty($product_attributes)){
              $count=0;
              foreach($product_attributes as $attribute_key => $attribute_arr){
                $attribute_slug = wc_get_product_terms(get_the_ID(), $attribute_key, array('fields' => 'slugs')); // GET ATTRIBUTE SLUG
                
                $varible_id = $available_variation['variation_id'];
                // eb3at el variation id lel function bta3t el get time slots 3ashan t3ml select mn el database bokra el sobh
                $attribute_name = str_replace('pa_', '', $attribute_arr['name']);
                $sessions_chk=false;
                $sesssion_term = '';
                 if($attribute_key == 'pa_pass-type' || $attribute_key == 'pa_session'){ 
                    $class_name = str_replace('pa_','',$attribute_key);
                    if($class_name=='session'): 
                      $sessions_chk=true; 
                    endif;
                     ?>
                  <div class="section_info <?php echo $class_name;?> time">
                    <label><?php echo wc_attribute_label($attribute_key);?></label>
                    <div class="pass">
                      <?php
                      if(!empty($attribute_arr['options'])){
                        $count = 0;
                        $arr = $attribute_arr['options'];
                        sort($arr);
                        foreach($arr as $option_id){
                        
                          $active     = '';
                          $selected     = '';
                          $color_name = mitch_get_product_attribute_name_by_id($option_id);
                          if(isset($default_attributes[$attribute_key])){
                            if($default_attributes[$attribute_key] == sanitize_title($color_name)){
                              $active = 'active';
                              $selected = 'checked';
                            }
                          }
                          if(empty($active) && $count == 0){
                            $active = 'active';
                            $selected = 'checked';
                          }
                          if(in_array($color_name, $variations_attr)){
                            $term = get_term($option_id,$attribute_key);
                            if($sessions_chk && $term->slug!='1-session'):
                              $sesssion_term = $term;
                            endif;
                            if($count==0){
                              $attr_checked = $term->slug; 
                            }
                            ?>
                            <div class="single_radio">
                                <input type="radio" class="variation_option <?php echo $active;?> " id="<?php echo $term->term_id;?>" name="pass-type-radio" value="<?php echo $term->slug;?>" data-value="<?php echo $term->slug;?>" data-key="<?php echo 'attribute_'.$attribute_key;?>" <?php echo $selected;?> >
                                <label for="<?php echo $term->term_id;?>"><?php echo $color_name;?></label>
                            </div>
                            <?php
                            $count++;
                          }
                        }
                      }
                      ?>
                    </div>
                  </div>
                  <?php if($sessions_chk): $terms_rules=get_field('terms_rules',$sesssion_term); if($terms_rules):?>
                  <ul class="terms-rules" style="display: none;">
                    <?php foreach($terms_rules as $term_rule):?>
                      <li>
                        <?php echo $term_rule['rule'];?>
                      </li>
                    <?php endforeach;?>
                  </ul>
                  <?php endif; endif; } elseif($attribute_key == 'pa_time-slots') {
                    if($product_form != 80):
                    ?>
                    <?php
                      $sql = $wpdb->get_var("SELECT date_time from wp_product_date_time WHERE product_id = ".$single_product_data['main_data']->get_id()." limit 0,100");
                      $sql = (strpos($sql, ',') !== false)? explode(",",$sql): $sql;
                      if(is_array($sql)){
                        foreach($sql as $single_id){
                          $date_time[] = $single_id;
                        }
                      }
                      else{
                        $date_time[] = $sql;
                      }
                      $date_time_unique = array_unique($date_time);
                      $today_new_format = date("n-j-Y", strtotime("+2 hour"));
                      ?>
                      <div class="section_info time_slots <?php echo $attribute_name;?>">
    
                        <label><?php echo wc_attribute_label($attribute_key);?></label>
                        <div class="pass">
                            <div class="time-slots-content">
                              <?php
                                if(!empty($attribute_arr['options'])){
                                  $count = 0;
                                  foreach($attribute_arr['options'] as $option_id){
                                    $active     = '';
                                    $selected     = '';
                                    $color_name = mitch_get_product_attribute_name_by_id($option_id);
                                    if(isset($default_attributes[$attribute_key])){
                                      if($default_attributes[$attribute_key] == sanitize_title($color_name)){
                                        $active = 'active';
                                        $selected = 'selected';
                                      }
                                    }
                                    // if(empty($active) && $count == 0){
                                    //   $active = 'active';
                                    //   $selected = 'selected';
                                    // }
                                    $term = get_term($option_id,$attribute_key);
                                    if(isset($regions[$attr_checked][$term->slug])):
                                      $variation_id = $regions[$attr_checked][$term->slug];
                                    endif;
                                    // if($regions): echo 'heeeh';

                                    // foreach($regions as $key => $value):
                                    // print_r($value);
        
                                    // print_r($key);
                                    // endforeach;
                                    // endif;
                                    $date_var = $today_new_format.' '.$term->name;
                                    if(in_array($color_name, $variations_attr) && !in_array($date_var,$date_time_unique)){
                                      if($term->slug!='academy'):
                                      ?>
                                    <div class="single">
                                      <div class="box">
                                        <input type="checkbox"  class="variation_option <?php echo $active;?>" data-variation_id="<?php echo $variation_id;?>" data-name="<?php echo $term->name;?>" id="<?php echo $term->term_id;?>" name="time-slots-radio" value="<?php echo $term->slug;?>" data-value="<?php echo $term->slug;?>" data-key="<?php echo 'attribute_'.$attribute_key;?>" <?php echo $selected;?> >
                                        <label for="<?php echo $term->term_id;?>"><?php echo $color_name;?></label>
                                      </div>
                                    </div>
                                      <?php
                                      endif;
                                      $count++;
                                    }
                                  }
                                }
                                ?>
                            </div>
                        </div>
                      </div>
                   
                      <?php
                  endif;
                  }else{
                      ?>
                      <div class="section_info <?php echo $attribute_name;?>">
                        <label><?php echo wc_attribute_label($attribute_key);?></label>
                        <div class="select_arrow">
                          <select class="info" class="variations-select" id="height_option_<?php echo $attribute_name;?>" onchange="get_availablility_variable_product(<?php echo $single_product_data['main_data']->get_id();?>,<?php echo $single_product_data['product_form']->term_id;?>);">
                            <?php
                            // print_r($attribute_arr);
                            if(!empty($attribute_arr['options'])){
                              $count = 0;
                              $arr = $attribute_arr['options'];
                              sort($arr);
                              foreach($arr as $option_id){
                                $active     = '';
                                $selected     = '';
                                $color_name = mitch_get_product_attribute_name_by_id($option_id);
                                if(isset($default_attributes[$attribute_key])){
                                  if($default_attributes[$attribute_key] == sanitize_title($color_name)){
                                    $active = 'active';
                                    $selected = 'selected';
                                  }
                                }
                                elseif(empty($active) && $count == 0){
                                  $active = 'active';
                                  $selected = 'selected';
                                }
                                // print_r($variations_attr);
                                if(in_array($color_name, $variations_attr)){
                                  $term = get_term($option_id,$attribute_key);
                                  if($count==0){
                                    $attr_checked = $term->slug; 
                                  }
                                  ?>
                                  <option class="single_info variation_option <?php echo $active;?>" data-value="<?php echo $term->slug;?>" data-tmp="<?php echo $term->slug;?>" data-key="<?php echo 'attribute_'.$attribute_key;?>" <?php echo $selected;?> >
                                    <?php echo $color_name;?>
                                  </option>
                                  <?php
                                  $count++;
                                }
                              }
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                      <?php
                  } $count++;} } ?>
             
    </div>
    <div class="min_middle variable_middle">
          <!-- <div class="section_count">
                <button class="increase" id="increase" onclick="increaseValue()" value="Increase Value"></button>
                <input class="number_count" type="number" id="number" value="1" data-min="1" data-max="" />
                <button class="decrease" id="decrease" onclick="decreaseValue()" value="Decrease Value"></button>
            </div> -->
            <p class="price"><?php echo $single_product_price;?> <?php echo $theme_settings['current_currency'];?></p>
            <!-- show add to cart when needed -->
            
            <?php if($product_form == 80):?>
            <p class="note">For Reservation for Hotel Guests, Call <span>16160</span></p>
            <?php endif;?>
    </div>
    <?php }  ?>
</div>
