<?php
if($args){
  $product_data = $args;
}
?>
<li id="product_<?php echo $product_data['product_id'];?>_block" class="product_widget"
<?php if(isset($prod_anim_count)){echo 'data-aos="fade-left" data-aos-duration="'.$prod_anim_count.'"';}?>
  >
  <a href="<?php echo $product_data['product_url'];?>" class="product_widget_box">
      <div class="img">
          <img src="<?php echo $product_data['product_image'];?>" alt="<?php echo $product_data['product_title'];?>">
          <span><?php echo $product_data['product_form'];?></span>
      </div>
      <div class="text">
        <div class="sec_info">
            <h3 class="title">
              <?php echo $product_data['product_title'];?>
              <span>
              <?php if(!empty($product_data['product_country_code'])){ ?>
              <img src="<?php echo $theme_settings['theme_url'];?>/assets/img/flag/<?php echo $product_data['product_country_code'];?>.png" alt="">
              </span>
              <?php }?>
            </h3>
            <?php if($product_data['product_excerpt']):?>
            <div class="desc"><?php echo $product_data['product_excerpt'];?></div>
            <?php endif;?>
            <?php if(!is_tax('product_form')):?>
            <p class="price"><?php echo $product_data['product_price'].' EGP';?></p>
            <?php endif;?>
        </div>
        <div class="open_widget">
          <span></span>
        </div>
      </div>
  </a>
</li>
