
<?php
$is_white_header = (is_page('home')||is_page_template('beach-template.php')||is_page('mivida-spa')||is_page('transportation')||is_page('the-village')||is_page('fine-dining')||is_page('phases')||is_tax('product_form') ||is_page_template('camp-template.php'))?'header_hero':'';
?>
<div class="section_header <?php echo ($is_white_header)? 'header_hero':'';?>">
    <div class="grid">
        <div class="section_header_col_one">
              <div class="section_call">
                  <!-- <a href="">
                      <img src="<?php //echo $theme_settings['theme_url'];?>/assets/img/icons/icon_mail<?php //echo ($is_white_header)? '':'_white';?>.png" alt="">
                  </a> -->
                  <div class="call <?php echo ($is_white_header)? '':'white';?>">
                    <img class="normal" src="<?php echo $theme_settings['theme_url'];?>/assets/img/icons/telephone.png" alt="">
                    <img class="color" src="<?php echo $theme_settings['theme_url'];?>/assets/img/icons/telephone_white.png" alt="">
                    <p>16160</p>
                  </div>
              </div>
              <div class="section_logo">
                    <a href="<?php echo $theme_settings['site_url'];?>">
                      <img class="normal" src="<?php echo $theme_settings['theme_url'];?>/assets/img/logoo.png" alt="">
                      <img class="color" src="<?php echo $theme_settings['theme_url'];?>/assets/img/logoo_white.png" alt="">
                    </a>
              </div>
              <div class="sec_end">
                  <?php if(!is_cart()) { ?>

                    <div class="cart <?php  echo (WC()->cart->get_cart_contents_count() > 0)?'':'hide'; ?>">
                        <!-- <a href="<?php// echo home_url('cart');?>"> -->
                        <a href="#popup-min-cart" class="js-popup-opener">
                            <div class="section_icon_cart">
                              <?php //echo WC()->cart->get_total();?>
                                  <span id="cart_total_count"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
                                  <img class="white" src="<?php echo $theme_settings['theme_url'];?>/assets/img/icons/cart.png" alt="">
                                  <img class="cart_hover" src="<?php echo $theme_settings['theme_url'];?>/assets/img/icons/cart_hover.png" alt="">
                            </div>
                        </a>
                    </div>
                  <?php  } ?>
                  <div class="section_button">
                      <?php
                            // wp_nav_menu(
                            //   array(
                            //     'theme_location' => 'top-nav-menu-'.$theme_settings['current_lang'].'',
                            //     'container'      => 'nav',
                            //   )
                            // );
                        ?>
                        <nav class="menu-top-nav-menu-en-container"><ul id="menu-top-nav-menu-en" class="menu">
                        <li id="menu-item-454" class="header_link home menu-item menu-item-type-custom menu-item-object-custom menu-item-454">
                          <a target="_blank" href="https://almazabay.lodgify.com/">Rent A Home</a></li>
                        <li id="menu-item-455" class="header_link hotel menu-item menu-item-type-custom menu-item-object-custom menu-item-455">
                          <a target="_blank" href="https://www.jazhotels.com/search?nodes=10002r/">Book Hotel</a></li>
                        </ul></nav>
                      <!-- <a href="#" class="header_link home">Rent A Home</a>
                      <a href="#" class="header_link hotel">Book Hotel</a> -->
                  </div>
              </div>
                 
        </div>
        <div class="section_header_col_two">
          <nav class="main-nav">
              <ul class="main-menu">
              <?php
                $header_items = get_field('header_builder_'.$theme_settings['current_lang'], 'options');
                // echo '<pre>';
                // var_dump($header_items);
                // echo '</pre>';
                if(!empty($header_items)){
                  foreach($header_items as $header_item){
                    $item_type = $header_item['item_type'];
                    if($item_type=='list'){
                      $header_item['item_url_page'] = $header_item['item_list'];
                    }
                    if(!$header_item['item_group']['item_has_mega_dropdown']){
                      ?>
                      <li class="single_menu" >
                        <a class="link" href="<?php if($header_item['item_url_page']){echo($item_type=='list')?get_term_link($header_item['item_url_page'],$header_item['item_url_page']->taxonomy):$header_item['item_url_page'];}?>">
                          <?php echo $header_item['item_name'];?>
                        </a>
                      </li>
                      <?php
                    }else{
                      ?>
                  <li class="single_menu has-mega">
                      <a <?php if($header_item['item_url_page']){ echo 'href="';echo($item_type=='list')?get_term_link($header_item['item_url_page'],$header_item['item_url_page']->taxonomy):$header_item['item_url_page']; echo '"';}?> class="link arrow">
                        <?php echo $header_item['item_name'];?> </a>
                      <div class="mega-menu ">
                          <div class="box grid">
                            <div class="list_subcategory">
                              <ul>
                                  <?php
                                    $items_data = $header_item['item_group']['mega_menu_options'];
                                    // $items_data_count = count($items_data['mega_menu_cols']);
                                    $count=0;
                                    foreach($items_data['mega_menu_cols'] as $menu_col){
                                  ?>
                                    <li class="col">
                                      <?php 
                                      if($menu_col['type']=='phases'):
                                      $url = get_the_permalink(992).'?uid='.get_post_field( 'post_name', $menu_col['item'] );
                                      $title = get_the_title($menu_col['item']);
                                    elseif($menu_col['type']=='list'):
                                      $url = get_term_link($menu_col['item_list'],$menu_col['item_list']->taxonomy);
                                      $title = $menu_col['item_list']->name;
                                    else:
                                      $url = get_the_permalink($menu_col['item']);
                                      $title = get_the_title($menu_col['item']);
                                    endif;
                                      ?>
                                    <a class="title" href="<?php echo $url;?>">
                                          <?php echo $title;?>
                                      </a>
                                    </li>
                                   <?php if($count==5):?> </ul><ul> <?php endif; $count++;?>
                                  <?php } ?>
                              </ul>
                            </div>
                            <div class="img_category">
                              <?php //print_r($items_data);?>
                                <!-- <img src="<?php //echo $items_data['mega_menu_image'];?>" alt=""> -->
                                <img src="<?php echo $items_data['mega_menu_image'];?>" alt="">
                                <!-- <img src="<?php //echo $theme_settings['theme_url'];?>/assets/img/dropdwon_menu.png" alt=""> -->
                            </div>
                          </div>
                      </div>
                    
                  </li>
                  <?php } } } ?>
              </ul>
          </nav><!-- .main-nav -->
        </div>
    </div>
</div>
<div class="section_header sticky">
    <div class="grid">
        <div class="section_header_col_two">
          <nav class="main-nav">
              <ul class="main-menu">
              <?php
                $header_items = get_field('header_builder_'.$theme_settings['current_lang'], 'options');
                // echo '<pre>';
                // var_dump($header_items);
                // echo '</pre>';
                if(!empty($header_items)){
                  foreach($header_items as $header_item){
                    $item_type = $header_item['item_type'];
                    if($item_type=='list'){
                      $header_item['item_url_page'] = $header_item['item_list'];
                    }
                    if(!$header_item['item_group']['item_has_mega_dropdown']){
                      ?>
                      <li class="single_menu">
                      <a class="link" href="<?php if($header_item['item_url_page']){echo($item_type=='list')?get_term_link($header_item['item_url_page'],$header_item['item_url_page']->taxonomy):$header_item['item_url_page'];}?>">
                          <?php echo $header_item['item_name'];?>
                        </a>
                      </li>
                      <?php
                    }else{
                      ?>
                  <li class="single_menu has-mega" data-item="<?php echo $header_item['item_url_page'];?>">
                        <a <?php if($header_item['item_url_page']){echo 'href="';echo($item_type=='list')?get_term_link($header_item['item_url_page'],$header_item['item_url_page']->taxonomy):$header_item['item_url_page'];echo '"';}?> class="link arrow">
                        <?php echo $header_item['item_name'];?> </a>
                      <div class="mega-menu ">
                          <div class="box grid">
                            <div class="list_subcategory">
                              <ul>
                                  <?php
                                    $items_data = $header_item['item_group']['mega_menu_options'];
                                    $count=0;
                                    foreach($items_data['mega_menu_cols'] as $menu_col){
                                  ?>
                                      <?php 
                                      if($menu_col['type']=='phases'):
                                      $url = get_the_permalink(992).'?uid='.get_post_field( 'post_name', $menu_col['item'] );
                                      $title = get_the_title($menu_col['item']);
                                    elseif($menu_col['type']=='list'):
                                      $url = get_term_link($menu_col['item_list'],$menu_col['item_list']->taxonomy);
                                      $title = $menu_col['item_list']->name;
                                    else:
                                      $url = get_the_permalink($menu_col['item']);
                                      $title = get_the_title($menu_col['item']);
                                    endif;
                                      ?>
                                    <li class="col">
                                    <a class="title" href="<?php echo $url;?>">
                                          <?php echo $title;?>
                                      </a>
                                    </li>
                                    <?php  if($count==5):?> </ul><ul> <?php endif; $count++;?>
                                  <?php } ?>
                              </ul>
                            </div>
                            <div class="img_category">
                              <?php //print_r($items_data);?>
                                <!-- <img src="<?php //echo $items_data['mega_menu_image'];?>" alt=""> -->
                                <img src="<?php echo $items_data['mega_menu_image'];?>" alt="">
                                <!-- <img src="<?php //echo $theme_settings['theme_url'];?>/assets/img/dropdwon_menu.png" alt=""> -->
                            </div>
                          </div>
                      </div>
                    
                  </li>
                  <?php } } } ?>

                  <li id="menu-item-455" class="header_link hotel menu-item menu-item-type-custom menu-item-object-custom menu-item-455">
                    <a target="_blank" href="https://www.jazhotels.com/search?nodes=10002r">Book Hotel</a>
                  </li>

              </ul>
          </nav><!-- .main-nav -->
        </div>
    </div>
</div>
