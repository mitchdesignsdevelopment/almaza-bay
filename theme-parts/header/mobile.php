    <div class="section_header_mobile">  
        <div class="section_header_col_one sec_end">
            <div class="menu_button_mobile">
                <button type="button" class="menu_mobile_icon open"><i class="material-icons">menu</i></button>
                <button class="menu_mobile_icon close"><i class="material-icons">close</i></button>
            </div>
            <div class="section_logo">
                  <a href="<?php echo $theme_settings['site_url'];?>">
                    <img class="color" src="<?php echo $theme_settings['theme_url'];?>/assets/img/logoo_white.png" alt="">
                  </a>
            </div>
            <?php if(!is_cart()) { ?>
              <div class="cart <?php  echo (WC()->cart->get_cart_contents_count() > 0)?'':'hide'; ?>">
                  <!-- <a href="<?php// echo home_url('cart');?>"> -->
                  <a href="#popup-min-cart" class="js-popup-opener">
                      <div class="section_icon_cart">
                        <?php //echo WC()->cart->get_total();?>
                            <span id="cart_total_count"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
                            <img class="white" src="<?php echo $theme_settings['theme_url'];?>/assets/img/icons/cart.png" alt="">
                            <img class="cart_hover" src="<?php echo $theme_settings['theme_url'];?>/assets/img/icons/cart_hover.png" alt="">
                      </div>
                  </a>
              </div>
              <?php  } ?>
        </div>
        <div class="section_header_col_two">
            <div class="mobile-nav">
                <div class="mobile-menu">
                    <div class="section_button">
                      <?php
                            // wp_nav_menu(
                            //   array(
                            //     'theme_location' => 'top-nav-menu-'.$theme_settings['current_lang'].'',
                            //     'container'      => 'nav',
                            //   )
                            // );
                        ?>
                        <nav class="menu-top-nav-menu-en-container"><ul id="menu-top-nav-menu-en" class="menu">
                        <li id="menu-item-454" class="header_link home menu-item menu-item-type-custom menu-item-object-custom menu-item-454">
                          <a target="_blank" href="https://almazabay.lodgify.com/">Rent A Home</a></li>
                        <li id="menu-item-455" class="header_link hotel menu-item menu-item-type-custom menu-item-object-custom menu-item-455">
                          <a target="_blank" href="https://www.jazhotels.com/search?nodes=10002r/">Book Hotel</a></li>
                        </ul></nav>
                        <!-- <a href="#" class="header_link home">Rent A Home</a>
                        <a href="#" class="header_link hotel">Book Hotel</a> -->
                    </div>
                    <nav class="main-navigation">
                        <nav class="menu-main-nav-container">
                            <ul class="menu">
                                <?php
                                    $header_items = get_field('header_builder_'.$theme_settings['current_lang'], 'options');
                                    // echo '<pre>';
                                    // var_dump($header_items);
                                    // echo '</pre>';
                                    if(!empty($header_items)){
                                      foreach($header_items as $header_item){
                                        $item_type = $header_item['item_type'];
                                        if($item_type=='list'){
                                          $header_item['item_url_page'] = $header_item['item_list'];
                                        }
                                        if(!$header_item['item_group']['item_has_mega_dropdown']){
                                ?>
                                <li class="menu-item">
                                  <a href="<?php if($header_item['item_url_page']){echo($item_type=='list')?get_term_link($header_item['item_url_page'],$header_item['item_url_page']->taxonomy):$header_item['item_url_page'];}?>">
                                    <?php echo $header_item['item_name'];?>
                                  </a>
                                </li>
                                    <?php
                                  }else{
                                    ?>
                                <li class="menu-item menu-item-has-children">
                                    <a class="menu-trigger" href=""><?php echo $header_item['item_name'];?></a>
                                    <div class="list_subcategory_mobile">
                                    <?php
                                      $items_data = $header_item['item_group']['mega_menu_options'];
                                      $count=0;
                                      foreach($items_data['mega_menu_cols'] as $menu_col){ 
                                      if($menu_col['type']=='phases'):
                                      $url = get_the_permalink(992).'?uid='.get_post_field( 'post_name', $menu_col['item'] );
                                      $title = get_the_title($menu_col['item']);
                                    elseif($menu_col['type']=='list'):
                                      $url = get_term_link($menu_col['item_list'],$menu_col['item_list']->taxonomy);
                                      $title = $menu_col['item_list']->name;
                                    else:
                                      $url = get_the_permalink($menu_col['item']);
                                      $title = get_the_title($menu_col['item']);
                                    endif;
                                      ?>
                                    <a class="col" href="<?php echo $url;?>">
                                          <?php echo $title;?>
                                      </a>
                                      <?php
                                      }
                                      ?>
                                    </div>
                                </li>
                                <?php $count++; if($count==4):?> </ul><ul> <?php endif;?>
                                <?php } } }?> 
                            </ul>
                        </nav>
                    </nav>
                </div>
            </div>
        </div>
    </div>
