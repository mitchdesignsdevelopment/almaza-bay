<div class="myaccount_menu">
    <ul>
        <li class="<?php echo mitch_get_active_page_class('my-account');?>"><a href="<?php echo home_url('my-account');?>"><?php echo $fixed_string['myaccount_page_sidebare_home'];?><span class="border"></span></a></li>
        <li class="<?php echo mitch_get_active_page_class('orders-list');?>"><a href="<?php echo home_url('my-account/orders-list');?>"><?php echo $fixed_string['myaccount_page_sidebare_orders'];?><span class="border"></span></a></li>
        <li class="<?php echo mitch_get_active_page_class('addresses');?>"><a href="<?php echo home_url('my-account/addresses');?>"><?php echo $fixed_string['myaccount_page_sidebare_address'];?><span class="border"></span></a></li>
        <li class="<?php echo mitch_get_active_page_class('profile');?>"><a href="<?php echo home_url('my-account/profile');?>"><?php echo $fixed_string['myaccount_page_sidebare_profile'];?><span class="border"></span></a></li>
        <li><a href="<?php echo wp_logout_url(home_url());?>"><?php echo $fixed_string['myaccount_page_sidebare_logout'];?><span class="border"></span></a></li>
    </ul>
</div>
