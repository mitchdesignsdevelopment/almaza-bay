<header>
    <div class="section_header checkout">
        <div class="grid">
            <div class="section_header_col_one">
                <div class="section_call"></div>
                <div class="section_logo">
                    <a href="<?php echo $theme_settings['site_url'];?>">
                        <img class="normal" src="<?php echo $theme_settings['theme_url'];?>/assets/img/logoo.png" alt="">
                        <img class="color" src="<?php echo $theme_settings['theme_url'];?>/assets/img/logoo_white.png" alt="">
                    </a>
                </div>
                <div class="sec_end">
                    <?php if(!is_cart()){ ?>
                        <div class="cart <?php  echo (WC()->cart->get_cart_contents_count() > 0)?'':'hide'; ?>">
                            <!-- <a href="<?php// echo home_url('cart');?>"> -->
                            <a href="#popup-min-cart" class="js-popup-opener">
                            <div class="section_icon_cart">
                            <?php //echo WC()->cart->get_total();?>
                                <span id="cart_total_count"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
                                <img class="white" src="<?php echo $theme_settings['theme_url'];?>/assets/img/icons/cart.png" alt="">
                                <img class="cart_hover" src="<?php echo $theme_settings['theme_url'];?>/assets/img/icons/cart_hover.png" alt="">
                            </div>
                            </a>
                        </div>
                    <?php  } ?>
                </div>
            </div>
        </div>
       
    </div>
</header>

