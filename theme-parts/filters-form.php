<form action="" class="section_filter">
    <div class="section_ranking">
        <label for="">ترتيب</label>
        <div class="select_arrow">
        <select class="sorting sort" data-slug="<?php echo (is_shop())? '':$term_slug; ?>" data-type="<?php echo (is_shop())?'shop':$term->taxonomy; ?>">
            <option value="popularity" class="sortby<?php echo (isset($_GET['orderby']) && $_GET['orderby']=='popularity')? ' active':''; ?>" selected><?php echo ($language == 'en')? 'Popularity' : 'الاكثر طلباً' ?></option>
            <option value="date" class="sortby<?php echo (isset($_GET['orderby']) && $_GET['orderby']=='date')? ' active':''; ?>"><?php echo ($language == 'en')? 'New Arrivals' : 'وصل حديثاً' ?></option>
            <option value="stock" class="sortby<?php echo (isset($_GET['orderby']) && $_GET['orderby']=='stock')? ' active':''; ?>" ><?php echo ($language == 'en')? 'Availability' : 'المتاح أولاً' ?></option>
            <option value="price" class="sortby<?php echo (isset($_GET['orderby'])&&$_GET['orderby']=='price')? ' active':''; ?>"> <?php echo ($language == 'en')? 'Low To High Price' : 'السعر الأقل أولاً' ?></option>
            <option value="price-desc" class="sortby<?php echo (isset($_GET['orderby'])&&$_GET['orderby']=='price-desc')? ' active':''; ?>"> <?php echo ($language == 'en')? 'High To Low Price' : 'السعر الأعلي أولاً' ?> </option>
        </select>
        </div>
    </div>
    <div class="list_filter">
    <?php
    $labels = get_terms(array('taxonomy' => 'product_cat','parent' => 0 ) );
                            $count=0;
                            foreach($labels as $label){
                                $boolv=false;
                                $products_in_brand = count(mitch_get_products_by_category($label->term_id,'filter'));
                                if ($products_in_brand>0){
                                    $boolv=true;
                                }
                                if ($products_in_brand){
                                $childs = get_terms(  array( 'taxonomy' => 'product_cat', 'parent' => $label->term_id,'hide_empty' => true ) );
                                if($childs):
                                ?>
                                    <?php //if(!$id == $label->term_id || $id == $label->term_id || $term->taxonomy == 'brand'): ?>
                                        <div class="form-checkbox <?php echo ($id == $label->term_id)?'open':'';?>">
                                                <h3><?php echo 'جميع منتجات '.$label->name ; ?></h3>
                                                    <?php 
                                                        $counter=0;
                                                        $child_count = count($childs);
                                                        foreach($childs as $child){
                                                        $products_in_brand = count(mitch_get_products_by_category($label->term_id,'filter'));   
                                                        ?>   
                                                    <div class="form-checkbox-content">
                                                        <input
                                                            type="checkbox"
                                                            class="filled-in filter_input filter-cat checkbox-box"   
                                                            value="<?php echo $child->slug;?>"
                                                            id="checkbox-cat-<?php echo $child->term_id;?>"
                                                            data-target="checkbox-cat-<?php echo $child->slug;?>"
                                                            <?php echo (isset($_GET['product_cat'])&&in_array($child->slug, explode(",",$_GET['product_cat'])))?'checked':''; ?>
                                                        />
                                                        <label class="checkbox-label" for="checkbox-cat-<?php echo $child->term_id;?>">
                                                            <?php echo $child->name;?>
                                                        </label>
                                                     </div>
                                                    <?php  $counter++; } ?>         
                                        </div>
                                    <?php //endif ?>   
                            <?php endif; } } ?>
    </div>
</form>
