<?php if($page_content['has_related_section']):?>
<div class="product related">
    <div class="grid">
        <div class="section_title">
            <h2><?php echo ($page_content['related_section']['title'])?$page_content['related_section']['title']:'More Activites'; //if(isset($new_related_title)){echo $new_related_title;}else{echo $fixed_string['product_related_section_title'];}?></h2>
            <p><?php echo ($page_content['related_section']['subtitle'])?$page_content['related_section']['subtitle']:'Explore more from here';?></p>
        </div>
        <div class="product_container">
            <ul class="products_list">
            <?php
            $exclude_ids = array();
            if(!isset($product_id)){
              $product_id = get_the_id();
            }
            $related_products_ids = wc_get_related_products($product_id, 3, $exclude_ids);
            if(!empty($related_products_ids)){
              foreach($related_products_ids as $r_product_id){
                $product_data = mitch_get_short_product_data($r_product_id);
                include 'product-widget.php';
              }
            }
            ?>
            </ul>
        </div>
    </div>
</div>
<?php endif;?>
<?php if($page_content['has_explore_more_section']):?>
<div class="product related bk">
    <div class="grid">
        <div class="section_title">
            <h2><?php echo $page_content['explore_more_section']['title'];?></h2>
            <p><?php echo $page_content['explore_more_section']['subtitle'];?></p>
        </div>
        <div class="product_container">
            <ul class="products_list">
            <?php if($page_content['explore_more_section']['widgets']): foreach($page_content['explore_more_section']['widgets'] as $widget):?>
              <li class="product_widget">
                <a href="<?php echo $widget['url'];?>" class="product_widget_box">
                  <div class="img">
                      <img src="<?php echo $widget['image'];?>" alt="<?php echo $widget['title'];?>">
                      <span><?php echo $widget['text_image'];?></span>
                  </div>
                  <div class="text">
                    <div class="sec_info">
                        <h3 class="title"><?php echo $widget['title'];?></h3>
                        <div class="desc"><?php echo $widget['description'];?></div>
                    </div>
                  </div>
                </a>
            </li>
            <?php endforeach; endif;?>
            </ul>
        </div>
    </div>
</div>
<?php endif;?>