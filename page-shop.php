<?php require_once 'header.php';?>
<div id="page" class="site">
  <?php require_once 'theme-parts/main-menu.php';?>
  <!--start page-->
  <div class="site-content page_list">
    <div class="grid">
      <div class="list_content">
        <?php include_once 'theme-parts/filters-form.php';?>
        <div class="product list">
              <div class="grid">
                  <div class="section_title">
                      <img class="naseej_icon_page" src="<?php echo $theme_settings['theme_favicon'];?>" alt="" width='50'>
                      <h2><?php echo $fixed_string['shop_page_title'];?></h2>
                      <p>
                        <?php echo $fixed_string['shop_page_descreption'];?>
                      </p>
                  </div>
                  <div class="product_container">
                      <ul class="products_list">
                      <?php
                      if(isset($_GET['collection']) && $_GET['collection'] == 'best_selling'){
                        $products_ids = mitch_get_best_selling_products_ids(-1);
                      }elseif(isset($_GET['collection']) && $_GET['collection'] == 'new_arrival'){
                        $products_ids = mitch_get_new_arrival_products_ids(-1);
                      }else{
                        $products_ids = mitch_get_products_list();
                      }
                      if(!empty($products_ids)){
                        foreach($products_ids as $product_id){
                          $product_data = mitch_get_short_product_data($product_id);
                          include 'theme-parts/product-widget.php';
                        }
                      }
                      ?>
                      </ul>
                  </div>
                  <!-- <div class="section_loader">
                    <div class="loader"></div>
                  </div> -->
              </div>
          </div>
      </div>
    </div>
  </div>
  <!--end page-->
</div>
<?php require_once 'footer.php';?>
