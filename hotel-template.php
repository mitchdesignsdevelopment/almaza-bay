<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package almaza
 * 
 * Template Name: Hotel
 *
 */
require_once 'header.php'; global $post;
$page_content = get_field('single_hotel');
//var_dump($post);?>
<div id="page" class="site">
  <?php require_once 'theme-parts/main-menu.php';?>
      <!--start page-->
      <div class="site-content page_single_hotel">
            <div class="grid">
                <div class="section_title_hero">
                    <h1><?php echo get_the_title();?></h1>
                    <span class="subtitle">Hotel</span>
                    <a class="link" target="_blank" href="<?php echo $page_content['book_button']['url'];?>"><?php echo $page_content['book_button']['title'];?></a>
                </div>
                <div class="content">
                <?php $number_of_cols = count( $page_content['hero_section']['banners']); ?>
                    <div class="hero_single_img <?php echo ($number_of_cols > 1) ? ' hero_single_img_slick_hotel' : ''; ?>">
            <?php if($page_content['hero_section']['banners']): foreach($page_content['hero_section']['banners'] as $banner):?>
                        <img src="<?php echo $banner['banner'];?>" alt="">
            <?php endforeach; endif;?>
                    </div>
                    <div class="section_hotel_info">
                        <div class="img">
                          <img src="<?php echo $page_content['hero_section']['section_under_hero_image'];?>" alt="">
                        </div>
                        <div class="text">
                          <h3><?php echo $page_content['hero_section']['section_under_hero_title'];?></h3>
                          <p><?php echo $page_content['hero_section']['section_under_hero_description'];?></p>
                          <a class="link" target="_blank" href="<?php echo $page_content['book_button']['url'];?>"><?php echo $page_content['book_button']['title'];?></a>
                        </div>
                    </div>
                    <div class="room_gallery">
                        <div class="section_title">
                            <p><?php echo $page_content['room_types_section']['title'];?></p>
                            <h4><?php echo $page_content['room_types_section']['subtitle'];?></h4>
                        </div>
                        <div class="room_content">
                            <div class="nav_menu">
                          <?php if($page_content['room_types_section']['types']): $count=1; foreach($page_content['room_types_section']['types'] as $types):?>
                                <a href="#single_room_<?php echo $count;?>" class="nav_single_title <?php echo($count==1)?'active':'';?>" ><?php echo $types['title'];?></a>
                          <?php $count++; endforeach; endif;?>
                            </div>
                            <div class="section_room">
                            <?php if($page_content['room_types_section']['types']): $count=1; foreach($page_content['room_types_section']['types'] as $types):?>
                                <div id="single_room_<?php echo $count;?>" class="single_room <?php echo($count==1)?'active':'';?>">
                                    <div class="content room">
                                        <h5><?php echo $types['title'];?></h5>
                                        <div class="sec_room_gallery">
                                      <?php if($types['image']): foreach($types['image'] as $image):?>
                                            <img src="<?php echo $image;?>" alt="">
                                      <?php endforeach; endif;?>
                                        </div>
                                        <div class="room_info">
                                            <h5><?php echo $types['subtitle'];?></h5>
                                            <p><?php echo $types['description'];?></p>
                                        </div>
                                        <div class="facilities">
                                          <ul>
                                      <?php if($types['facilities']): foreach($types['facilities'] as $facility):?>
                                            <li><img src="<?php echo $facility['icon'];?>" alt="Almaza" /><?php echo $facility['text'];?></li>
                                      <?php endforeach; endif;?>
                                          </ul>
                                        </div>
                                    </div>
                                </div>
                          <?php $count++; endforeach; endif;?>
                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
    <?php include_once 'theme-parts/reviews-products.php';?>
            <div class="grid">
                <div class="section_hotel_info row_reverse">
                    <div class="img">
                      <img src="<?php echo $page_content['food_&_breverge_section']['image'];?>" alt="">
                    </div>
                    <div class="text">
                      <h3><?php echo $page_content['food_&_breverge_section']['title'];?></h3>
                      <p><?php echo $page_content['food_&_breverge_section']['description'];?></p>
                    </div>
                </div>
                <div class="section_phases">
                    <div class="grid">
                        <div class="section_title">
                            <h3><?php echo $page_content['other_hotels_section']['title'];?></h3>
                            <p><?php echo $page_content['other_hotels_section']['subtitle'];?></p>
                        </div>
                        <div class="all_phases">
                          <?php
                           $args = array(
                            'post_type' => 'page',//it is a Page right?
                            'post_per_page' => 4,
                            'post__not_in' => array(get_the_ID()),
                            'post_status' => 'publish',
                            'meta_query' => array(
                                array(
                                    'key' => '_wp_page_template',
                                    'value' => 'hotel-template.php', // template name as stored in the dB
                                )
                            )
                        );
                        $result = get_posts($args);
                        if($result):
                          foreach($result as $post):
                            setup_postdata($post);
                          ?>
                            <div class="single_phases">
                                <a href="<?php echo get_the_permalink();?>">
                                    <div class="box">
                                        <img src="<?php echo get_the_post_thumbnail_url();?>" alt="">
                                        <div class="info">
                                            <p class="title_phases"><?php echo get_the_title();?></p>
                                            <p class="position_phases">Explore</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                          <?php endforeach; wp_reset_postdata(); endif;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end page-->
</div>
<?php require_once 'footer.php';?>
<script>
function select_star(start_value){
  $("#rating").val(start_value);
}
</script>