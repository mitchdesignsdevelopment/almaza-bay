<?php
require_once 'includes/global-functions.php';
require_once 'includes/translate-functions.php';
require_once 'includes/products-functions.php';
require_once 'includes/wishlist-functions.php';
require_once 'includes/cart-functions.php';
require_once 'includes/checkout-functions.php';
require_once 'includes/myaccount-functions.php';
require_once 'includes/wpadmin-functions.php';
require_once 'includes/pages-functions.php';
// require_once 'includes/cities.php';

add_action('init', 'almazabay_ui');
function almazabay_ui()
{
        wp_enqueue_script(
                'almaza-ui',
                get_stylesheet_directory_uri() . '/almazabay-ui/build/index.js',
		['wp-element'],
                rand(),
                true
        );
	wp_enqueue_style(
		'almazabay-ui-toastify',
		get_stylesheet_directory_uri() . '/almazabay-ui/node_modules/react-toastify/dist/ReactToastify.css'
	);
	wp_enqueue_style(
		'almazabay-ui-datepicker',
		get_stylesheet_directory_uri() . '/almazabay-ui/node_modules/react-datepicker/dist/react-datepicker.css'
	);
}
