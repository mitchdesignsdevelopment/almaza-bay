<?php ob_start(); require_once preg_replace('/wp-content.*$/','',__DIR__).'wp-load.php'; // $theme_settings = mitch_theme_settings();?>
<!doctype html>
<html dir="rtl" lang="ar">
  <head>
    <?php if(is_tax('product_form')): $cate = get_queried_object();?>
    <title><?php echo $cate->name;?> - <?php echo get_bloginfo('name'); ?></title>
    <meta name="description" content="<?php echo get_field('rank_math_description', get_the_ID()); ?>">
    <meta name="keywords" content="<?php echo get_field('rank_math_focus_keyword', get_the_ID()); ?>">
    <?php else:?>
    <title><?php echo get_field('rank_math_title', get_the_ID()); ?> - <?php echo get_bloginfo('name'); ?></title>
    <meta name="description" content="<?php echo get_field('rank_math_description', get_the_ID()); ?>">
    <meta name="keywords" content="<?php echo get_field('rank_math_focus_keyword', get_the_ID()); ?>">
    <?php endif;?>
    <meta charset="<?php echo get_bloginfo('charset'); ?>">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="<?php echo $theme_settings['theme_url'];?>/assets/sass/main.css" rel="stylesheet">
    <!-- <link href="<?php //echo $theme_settings['theme_url'];?>/assets/sass/main.rtl.css" rel="stylesheet"> -->
    <link href="<?php echo $theme_settings['theme_url'];?>/style.css" rel="stylesheet">
    <link rel="shortcut icon" type="image/png" href="<?php echo $theme_settings['theme_favicon'];?>"/>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Barlow+Semi+Condensed:wght@100;200;300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons|Material+Icons+Outlined&display=swap" rel="stylesheet"  defer>
    <link rel="stylesheet" type="text/css" media="all" href="<?php echo $theme_settings['site_url'];?>/wp-content/plugins/instagram-feed/css/sbi-styles.min.css">
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-P5JJS49');</script>
        <!-- End Google Tag Manager -->
            <?php if(is_checkout()):?>
          <?php wp_head();?>
          <?php endif;?>
  </head>
  <body
        data-mitch-ajax-url="<?php echo admin_url('admin-ajax.php');?>"
        data-mitch-logged-in="<?php if(is_user_logged_in()){echo 'yes';}else{echo 'no';}?>"
        data-mitch-current-lang="<?php echo $theme_settings['current_lang'];?>"
        data-mitch-home-url="<?php echo home_url();?>">
        <!-- Google Tag Manager (noscript) -->
      <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P5JJS49"
      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
      <!-- End Google Tag Manager (noscript) -->
        <div id="ajax_loader" style="display:none;">
            <div class="loader"></div>
        </div>
        <div class="web sticky_logo sticky_logo">
          <img src="<?php echo $theme_settings['theme_url'];?>/assets/img/icons/TP-logo.png" alt="">
        </div>
