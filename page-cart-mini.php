<?php require_once 'header.php';?>
<?php
$items  = WC()->cart->get_cart();
if(empty($items)){
  wp_redirect(home_url());
  exit;
}
?>
<div id="page" class="site">
  <?php require_once 'theme-parts/main-menu.php';?>
  <!--start page-->
  <div class="page_cart">
      <div class="cart">
          <div class="grid">
              <div class="sectio_title_cart">
                  <h2>
                    <p class="cart_subtitle">
                    <?php echo $fixed_string['cart_sub_title'];?>
                    </p>
                    <?php echo $fixed_string['cart_main_title'];?>
                  </h2>
              </div>
              <div class="section_cart">
                <?php
                mitch_get_cart_content(); //@ includes/cart-functions.php
                ?>
              </div>
          </div>
      </div>

  </div>
  <!--end page-->
</div>
<?php require_once 'footer.php';?>
<script>
function cart_remove_item(cart_item_key, product_id){
  $('#ajax_loader').show();
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: '<?php echo admin_url('admin-ajax.php');?>',
    data: {
      action: "cart_remove_item",
      cart_item_key: cart_item_key,
      product_id: product_id
    },
    success: function (data) {
      if(product_id){
        $('#'+product_id).remove();
      }
      if(cart_item_key){
        $('#'+cart_item_key).remove();
      }
      $('#cart_total_count').html(data.cart_count);
      // $('#cart_total').html(data.cart_total);
          $('.cart_total').each(function (){
            $(this).html(data.cart_total);
          });
      // alert(data.result);
      if(data.cart_count == 0){
        // Simulate an HTTP redirect:
        window.location.replace("<?php echo home_url();?>");
      }
      // alert('تم حذف المنتج من سلة المنتجات بنجاح.');
      $('#ajax_loader').hide();
      Swal.fire({
        title: 'Success',
        text: 'حذف المنتج من سلة المنتجات',
        icon: 'success',
        showConfirmButton: false,
        timer: 1500
      });
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      // alert("Error:" + errorThrown); //"Status: " + textStatus +
      $('#ajax_loader').hide();
      Swal.fire({
        title: 'Sorry, Something went wrong',
        text: errorThrown,
        icon: 'error',
        showConfirmButton: false,
        timer: 1500
      });
    }
  });
}

function update_cart_items(cart_item_key){
  $('#ajax_loader').show();
  $.ajax({
    type: 'POST',
    dataType: 'JSON',
    url: '<?php echo admin_url('admin-ajax.php');?>',
    data: {
      action: "update_cart_items",
      cart_item_key: cart_item_key,
      quantity_number: $('#'+cart_item_key+' .number_count').val(),
      //product_id: product_id
    },
    success: function (data) {
      $('#cart_total_count').html(data.cart_count);
      // $('#cart_total').html(data.cart_total);
      $('.cart_total').each(function (){
            $(this).html(data.cart_total);
          });
      $('#line_subtotal_'+cart_item_key).html(data.item_total);
      // alert('تم تعديل سلة المنتجات بنجاح!');
      $('#ajax_loader').hide();
      Swal.fire({
        title: 'Success',
        text: 'Cart Updated',
        icon: 'success',
        showConfirmButton: false,
        timer: 1500
      });
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      // alert("Error:" + errorThrown); //"Status: " + textStatus +
      $('#ajax_loader').hide();
      Swal.fire({
        title: 'Sorry, Something went wrong',
        text: errorThrown,
        icon: 'error',
        showConfirmButton: false,
        timer: 1500
      });
    }
  });
}

// var links = document.getElementsByTagName('a');
// // alert(links.length);
// for(var i = 0; i< links.length; i++){
//   alert(links[i].href);
// }

$('#apply_coupon').on('click', function () {
  $('#ajax_loader').show();
  var coupon_code = $('#coupon_code').val();
  if(coupon_code){
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: '<?php echo admin_url('admin-ajax.php');?>',
      data: {
        action: "mitch_apply_coupon",
        coupon_code: coupon_code,
        coupon_from: 'cart'
      },
      success: function (data) {
        //alert('form was submitted');
        $('#ajax_loader').hide();
        if(data.status == 'success'){
          $("body").trigger("updated_cart_totals");
          $("body").trigger("update_checkout");
          Swal.fire({
            title: 'Success',
            text: 'Coupon Applied',
            icon: 'success',
            showConfirmButton: false,
            timer: 1500
          });
          if(data.cart_total){
            // $('#cart_total').html(data.cart_total);
            $('.cart_total').each(function (){
            $(this).html(data.cart_total);
          });
          }
          if(data.redirect_to){
            window.location.replace(data.redirect_to);
          }
        } else if(data.status == 'error'){
          if(data.code == 401){
            Swal.fire({
              title: 'Sorry',
              text: data.msg,
              icon: 'error',
              showConfirmButton: true,
              // timer: 1500
            });
          }else{
            Swal.fire({
              title: 'Sorry',
              text: data.msg,
              icon: 'error',
              showConfirmButton: false,
              timer: 1500
            });
          }
        }
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        // alert("Error:" + errorThrown); //"Status: " + textStatus +
        $('#ajax_loader').hide();
        Swal.fire({
          title: 'Sorry, Something went wrong',
          text: errorThrown,
          icon: 'error',
          showConfirmButton: false,
          timer: 1500
        });
      }
    });
  }else{
    $('#ajax_loader').hide();
    Swal.fire({
      title: 'Sorry',
      text: 'Enter a valid coupon',
      icon: 'error',
      showConfirmButton: false,
      timer: 1500
    });
  }
});

$('#remove_coupon').on('click', function () {
  $('#ajax_loader').show();
  var coupon_code = $('#coupon_code').val();
  if(coupon_code){
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: '<?php echo admin_url('admin-ajax.php');?>',
      data: {
        action: "mitch_remove_coupon",
        coupon_code: coupon_code,
        coupon_from: 'cart'
      },
      success: function (data) {
        //alert('form was submitted');
        $('#ajax_loader').hide();
        if(data.status == 'success'){
          $('#apply_coupon').show();
          $("body").trigger("updated_cart_totals");
          $("body").trigger("update_checkout");
          $('#remove_coupon').hide();
          $('.list_pay.discount').hide();
          document.getElementById('coupon_code').value = '';
          Swal.fire({
            title: 'Success',
            text: 'Coupon Removed',
            icon: 'success',
            showConfirmButton: false,
            timer: 1500
          });
          if(data.cart_total){
            // $('#cart_total').html(data.cart_total);
            $('.cart_total').each(function (){
            $(this).html(data.cart_total);
          });
          }
          if(data.redirect_to){
            window.location.replace(data.redirect_to);
          }
        } else if(data.status == 'error'){
          if(data.code == 401){
            Swal.fire({
              title: 'Sorry',
              text: data.msg,
              icon: 'error',
              showConfirmButton: true,
              // timer: 1500
            });
          }else{
            Swal.fire({
              title: 'Sorry',
              text: data.msg,
              icon: 'error',
              showConfirmButton: false,
              timer: 1500
            });
          }
        }
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        // alert("Error:" + errorThrown); //"Status: " + textStatus +
        $('#ajax_loader').hide();
        Swal.fire({
          title: 'Sorry, Something went wrong',
          text: errorThrown,
          icon: 'error',
          showConfirmButton: false,
          timer: 1500
        });
      }
    });
  }else{
    $('#ajax_loader').hide();
    Swal.fire({
      title: 'Sorry',
      text: 'No discount available',
      icon: 'error',
      showConfirmButton: false,
      timer: 1500
    });
  }
});
</script>
