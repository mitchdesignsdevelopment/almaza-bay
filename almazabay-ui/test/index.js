const axios = require('axios')
const express = require('express')
const cors = require('cors')

const API_KEY = 'S7ZcIm59tPM6zMn9bRfovlZX2W9yByoMai6OxXe2X/GJi16HIizHrI7p2nfNCYMs'
const BASE_URL = 'https://api.lodgify.com'
const PORT = 3001

const lodgify = axios.create({
  baseURL: BASE_URL,
  headers: {
    'X-ApiKey': API_KEY
  }
})

const app = express()

app.use(cors())
app.use(express.json())

app.use('/almz/lodgify-passthrough/:url(*)', (req, res) => {
  const method = req.method;
  const url = req.params.url;
  const query = req.query;
  const body = req.body;

  let options = {
      method,
      url,
      params: query,
  };

  if (body && Object.keys(body).length) {
    options.data = body;
  }

  console.log(`[${new Date()}]`, options)
  
  lodgify(options)
    .then((response) => res.json(response.data))
    .catch((error) => res.status(error.response?.status || 500).json(error.response?.data || { message: error.message }))
})

app.listen(PORT, () => console.log('Listining on ' + PORT))
