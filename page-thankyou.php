<?php require_once 'header.php';?>
<?php
$order_id = intval($_GET['order_id']);
if(empty($order_id)){
  wp_redirect(home_url());
  exit;
}
$order_obj = wc_get_order($order_id);
?>
<div id="page" class="site">
  <?php require_once 'theme-parts/main-menu.php';?>
  <!--start page-->
  <div class="site-content page_thanks">
      <div class="grid">
          <div class="section_title">
              <img src="<?php echo $theme_settings['theme_url'];?>/assets/img/icon_thanks.png" alt="">
              <h3><?php echo $fixed_string['thankyou_title1'];?><span>#<?php echo $order_obj->get_id();?></span></h3>
              <p><?php echo $fixed_string['thankyou_title2'];?></p>
          </div>
          <div class="section_item">
              <div class="item_list">
              <?php
              foreach($order_obj->get_items() as $key => $values){
                $order_item_data = mitch_get_short_product_data($values['product_id']);
                ?>
                <div class="single_item">
                    <div class="sec_item">
                        <div class="img">
                            <img style="width: 100px;height: 100px;" src="<?php echo $order_item_data['product_image'];?>" alt="<?php echo $order_item_data['product_title'];?>">
                        </div>
                        <div class="info">
                            <div class="text">
                                <a href="<?php echo $order_item_data['product_url'];?>"><h4><?php echo $order_item_data['product_title'];?></h4></a>
                                <?php
                                if(!empty($values['custom_cart_data'])){
                                  ?>
                                  <ul>
                                  <?php
                                  foreach($values['custom_cart_data']['attributes_vals'] as $attr_val){
                                    ?>
                                    <li><?php echo mitch_get_product_attribute_name($attr_val);?></li>
                                    <?php
                                  }
                                  ?>
                                  </ul>
                                <?php }?>
                            </div>
                            <p><?php echo $values['line_total'] / $values['quantity'];?> <?php echo $theme_settings['current_currency'];?></p>
                        </div>
                    </div>
                    <p class="count"><?php echo $fixed_string['checkout_form_number'];?>: <?php echo $values['quantity'];?></p>
                    <p class="total_price"><?php echo $values['line_total'];?> <?php echo $theme_settings['current_currency'];?></p>
                </div>
                <?php
              }
              ?>
              </div>
          </div>
          <div class="section_info">
              <div class="info">
                  <h5><?php echo $fixed_string['thankyou_details'];?></h5>
                  <p><?php echo $order_obj->get_billing_first_name().' '.$order_obj->get_billing_last_name();?></p>
                  <p><?php echo $order_obj->get_billing_phone();?></p>
                  <p><?php echo $order_obj->get_billing_email();?></p>
              </div>
              <div class="info">
                  <h5><?php echo $fixed_string['thankyou_shipping'];?></h5>
                  <p><?php echo $order_obj->get_billing_address_1().', '.$order_obj->get_billing_city().', '.$order_obj->get_billing_country();?></p>
              </div>
              <div class="info">
                  <h5><?php echo $fixed_string['thankyou_payment'];?></h5>
                  <p><?php echo $fixed_string[$order_obj->get_payment_method_title()]; ?></p>
              </div>
          </div>

      </div>

  </div>
  <!--end page-->
</div>
<?php require_once 'footer.php';?>
