    <!--start footer-->
    <footer>
        <div class="section_footer">
            <div class="grid">
                <div class="all_section">
                    <div class="section_footer_col_one">
                        <div class="section_logo">
                            <a href="<?php echo $theme_settings['site_url']; ?>">
                                <img src="<?php echo $theme_settings['theme_url']; ?>/assets/img/logoo_white.png"
                                    alt="">
                            </a>
                            <p>A destination designed for you to enjoy the simplest things in life.</p>
                            <div class="call">
                                <img src="<?php echo $theme_settings['theme_url']; ?>/assets/img/icons/telephone.png"
                                    alt="">
                                <p class="num">16160</p>
                            </div>
                        </div>
                        <div class="company_social">
                        <!-- <div><form class="js-cm-form" id="subForm" action="https://www.createsend.com/t/subscribeerror?description=" method="post" data-id="5B5E7037DA78A748374AD499497E309E0506B8AEAD93A49F2D86569C1FCB400D66E6553818DB50098D34B7CCF7CFFE817CA73EC461C65BD68EDAB7E8D4944F22"><div><div><label>Email </label><input autocomplete="Email" aria-label="Email" class="js-cm-email-input qa-input-email" id="fieldEmail" maxlength="200" name="cm-biytiij-biytiij" required="" type="email"></div></div><button type="submit">Subscribe</button></form></div><script type="text/javascript" src="https://js.createsend1.com/javascript/copypastesubscribeformlogic.js"></script> -->
                            <h5>Sign up to our newsletter</h5>
                            <span>For our newsletters and Offers</span>
                            <form class="js-cm-form form" id="subForm" class="js-cm-form"
                                action="https://www.createsend.com/t/subscribeerror?description=" method="post"
                                data-id="5B5E7037DA78A748374AD499497E309E0506B8AEAD93A49F2D86569C1FCB400D66E6553818DB50098D34B7CCF7CFFE817CA73EC461C65BD68EDAB7E8D4944F22">
                                <div size="base" class="sc-jzJRlG bMslyb">
                                    <div size="small" class="sc-jzJRlG liOVdz">
                                        <div>
                                        <input autocomplete="Email" aria-label="Email" class="js-cm-email-input qa-input-email" id="fieldEmail" maxlength="200" name="cm-biytiij-biytiij" required="" type="email">
                                        </div>
                                    </div>
                                    <div size="base" class="sc-jzJRlG bMslyb"></div>
                                </div>
                                <button type="submit"
                                    class="js-cm-submit-button sc-iAyFgw efTFaG btn"><?php echo 'Sign up'; ?></button>
                            </form>
                            <script type="text/javascript" src="https://js.createsend1.com/javascript/copypastesubscribeformlogic.js"></script>
                            <!-- <h5>Sign up to our newsletter</h5>
                            <span>For our newsletters and Offers</span>
                            <form id="subForm" action="" method="post">
                                <input type="email" placeholder="Enter your email address" required />
                                <button class="btn" type="submit">Sign up</button>
                            </form> -->

                        </div>
                    </div>

                    <div class="section_footer_col_two">
                        <?php
              $footer_items = get_field('footer_builder_' . $theme_settings['current_lang'], 'options');
              if (!empty($footer_items)) {
                foreach ($footer_items as $footer_item) {
              ?>
                        <ul class="list_menu">
                            <li>
                                <h4><?php echo $footer_item['title']; ?></h4>
                            </li>
                            <?php
                    if (!empty($footer_item['items'])) {
                      foreach ($footer_item['items'] as $sub_item) {

                        if ($sub_item['item_type'] == 'page') {
                    ?>
                            <li><a
                                    href="<?php echo get_the_permalink($sub_item['item_page']->ID); ?>"><?php echo $sub_item['item_page']->post_title; ?></a>
                            </li>
                            <?php
                        } elseif ($sub_item['item_type'] == 'category') {
                        ?>
                            <li><a
                                    href="<?php echo get_term_link($sub_item['item_category']->term_id, 'product_cat'); ?>"><?php echo $sub_item['item_category']->name; ?></a>
                            </li>
                            <?php
                        } elseif ($sub_item['item_type'] == 'custom') {
                        ?>
                            <li><a
                                    href="<?php echo $sub_item['item_custom']['url']; ?>"><?php echo $sub_item['item_custom']['title']; ?></a>
                            </li>
                            <?php
                        } elseif ($sub_item['item_type'] == 'phase') {
                        ?>
                            <li><a
                                    href="<?php echo get_the_permalink(992) . '?uid=' . $sub_item['item_page']->post_name; ?>"><?php echo $sub_item['item_page']->post_title; ?></a>
                            </li>
                            <?php
                        }
                      }
                    }
                    ?>
                        </ul>
                        <?php }
              } ?>
                    </div>
                    <div class="section_footer_col_three">
                        <div class="section_insta">
                            <!-- <div class="title">
                            <img src="<?php echo $theme_settings['theme_url']; ?>/assets/img/icons/insta_icon.png" alt="" width="20">
                            <h4> Instagram</h4>
                            <span>@almazabay</span>
                        </div> -->

                            <div class="insta_gallery">
                                <?php echo do_shortcode("[instagram-feed feed=1]"); ?>
                                <!-- <a href="">
                                  <img src="<?php //echo $theme_settings['theme_url'];
                                            ?>/assets/img/insta_01.png" alt="">
                              </a>
                              <a href="">
                                  <img src="<?php //echo $theme_settings['theme_url'];
                                            ?>/assets/img/insta_02.png" alt="">
                              </a>
                              <a href="">
                                  <img src="<?php //echo $theme_settings['theme_url'];
                                            ?>/assets/img/insta_03.png" alt="">
                              </a>
                              <a href="">
                                  <img src="<?php //echo $theme_settings['theme_url'];
                                            ?>/assets/img/insta_04.png" alt="">
                              </a>
                              <a href="">
                                  <img src="<?php //echo $theme_settings['theme_url'];
                                            ?>/assets/img/insta_05.png" alt="">
                              </a>
                              <a href="">
                                  <img src="<?php //echo $theme_settings['theme_url'];
                                            ?>/assets/img/insta_06.png" alt="">
                              </a>
                              <a href="">
                                  <img src="<?php //echo $theme_settings['theme_url'];
                                            ?>/assets/img/insta_07.png" alt="">
                              </a>
                              <a href="">
                                  <img src="<?php //echo $theme_settings['theme_url'];
                                            ?>/assets/img/insta_08.png" alt="">
                              </a>
                              <a href="">
                                  <img src="<?php //echo $theme_settings['theme_url'];
                                            ?>/assets/img/insta_09.png" alt="">
                              </a> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom_footer">
            <div class="grid">
                <p class="copy_right">© <?php echo date('Y');?> Almaza Bay</p>
                <a href="https://www.mitchdesigns.com" target="_blank" class="powerd">Powered by MitchDesigns</a>
            </div>

        </div>
    </footer>
    <!--end footer-->





    <div id="overlay" class="overlay"></div>
    <?php if (!is_page('phase')) : ?>
    <div id="popup-reviews" class="popup reviews">
        <div class="popup__window reviews">
            <button type="button" class="popup__close material-icons js-popup-closer">close</button>
            <div class="form-content">
                <p>Thank You For Being Almaza Bay Guest</p>
                <h3>Share Your Experience</h3>
                <div id="reviews_form_alerts" class="ajax_alerts"></div>
                <form id="reviews_form">
                    <div id="reviews_form_alerts" class="ajax_alerts"></div>
                    <div class="field half">
                        <label>First Name</label>
                        <input type="text" name="fname">
                    </div>
                    <div class="field half">
                        <label>Last Name</label>
                        <input type="text" name="lname">
                    </div>
                    <div class="field ">
                        <label for="attended">Share Your Attended</label>
                        <!-- <select >
                            <option value="Attended Summer’17 and Summer’19"><?php echo 'Attended Summer’17 and Summer’19'; ?></option>
                            <option value="Attended Summer’17 and Summer’19"><?php echo 'Attended Summer’17 and Summer’19'; ?></option>
                            <option value="Attended Summer’17 and Summer’19"><?php echo 'Attended Summer’17 and Summer’19'; ?></option>
                            <option value="Attended Summer’17 and Summer’19"><?php echo 'Attended Summer’17 and Summer’19'; ?></option>
                        </select> -->
                        <input type="text" name="attended" id="attended" />
                    </div>
                    <?php if (is_page_template('hotel-template.php')) : ?>
                    <div class="field select_arrow">
                        <label for="rating">Share Your Experience</label>
                        <select name="num" id="num">
                            <option value="<?php echo get_the_ID(); ?>"><?php echo get_the_title(); ?></option>
                            <?php
                    $args = array(
                      'post_type' => 'page', //it is a Page right?
                      'post_per_page' => -1,
                      'post__not_in' => array(get_the_ID()),
                      'post_status' => 'publish',
                      'meta_query' => array(
                        array(
                          'key' => '_wp_page_template',
                          'value' => 'hotel-template.php', // template name as stored in the dB
                        )
                      )
                    );
                    $result = get_posts($args);
                    if ($result) :
                      foreach ($result as $post) :
                        setup_postdata($post);
                    ?>
                            <option value="<?php echo get_the_ID(); ?>"><?php echo get_the_title(); ?></option>
                            <?php endforeach;
                      wp_reset_postdata();
                    endif; ?>
                        </select>
                    </div>
                    <?php endif; ?>
                    <div class="field">
                        <label>Email Address</label>
                        <input type="email" name="email">
                    </div>
                    <div class="field">
                        <label>Phone Number</label>
                        <input type="number" name="number">
                    </div>
                    <div class="field">
                        <label for="">Share Your Experience</label>
                        <textarea name="comment" rows="4"></textarea>
                    </div>
                    <div class="field select_stars">
                        <label for="rating">Share Your Experience</label>
                        <div class="rate">
                            <input type="radio" id="star5" name="rate" value="5" />
                            <label onclick="select_star(5);" for="star5" title="Excellent">5 stars</label>
                            <input type="radio" id="star4" name="rate" value="4" />
                            <label onclick="select_star(4);" for="star4" title="Good">4 stars</label>
                            <input type="radio" id="star3" name="rate" value="3" />
                            <label onclick="select_star(3);" for="star3" title="Moderate">3 stars</label>
                            <input type="radio" id="star2" name="rate" value="2" />
                            <label onclick="select_star(2);" for="star2" title="Not Bad">2 stars</label>
                            <input type="radio" id="star1" name="rate" value="1" />
                            <label onclick="select_star(1);" for="star1" title="Bad">1 star</label>
                        </div>
                        <select name="rating" id="rating" style="display:none">
                            <option value="">Rate</option>
                            <option value="5">Perfect</option>
                            <option value="4">Good</option>
                            <option value="3">Average</option>
                            <option value="2">Not that bad</option>
                            <option value="1">Very poor</option>
                        </select>
                    </div>

                    <input type="hidden" name="product_id" value="<?php echo get_the_ID(); ?>">
                    <button class="btn btn-primary">Leave A Review</button>
                </form>
            </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if (is_page('phase') || is_front_page()) : ?>
    <div id="popup-phase" class="popup phase">
        <div class="popup__window phase">
            <button type="button" class="popup__close material-icons js-popup-closer">close</button>
            <div class="form-content">
                <div class="title">
                    <p>Book a Site Tour</p>
                    <h3>Meet our Sales Team</h3>
                </div>
                <form id="phases_form">
                    <div id="phases_form_alerts" class="ajax_alerts"></div>
                    <div class="fade-content">
                        <div class="field radio_button">
                            <div class="form-checkbox-content">
                                <input type="radio" class="checkbox-box" name="role" id="mr" value="mr" checked>
                                <label for="mr">Mr.</label>
                            </div>
                            <div class="form-checkbox-content">
                                <input type="radio" class="checkbox-box" name="role" id="ms" value="ms">
                                <label for="ms">Ms.</label>
                            </div>
                            <div class="form-checkbox-content">
                                <input type="radio" class="checkbox-box" name="role" id="mrs" value="mrs">
                                <label for="mrs">Mrs.</label>
                            </div>
                            <div class="form-checkbox-content">
                                <input type="radio" class="checkbox-box" name="role" id="dr" value="dr">
                                <label for="Dr">Dr.</label>
                            </div>
                        </div>
                        <div class="field half">
                            <div class="col">
                                <label>First Name</label>
                                <input type="text" name="fname">
                            </div>
                            <div class="col">
                                <label>Last Name</label>
                                <input type="text" name="lname">
                            </div>
                        </div>
                        <div class="field select_arrow">
                            <label>Interested In</label>
                            <select id="interested_in">
                                <?php
                    $args = array(
                      'post_type'      => 'page',
                      'posts_per_page' => -1,
                      'post_status' => 'publish',
                      // 'fields' => 'ids',
                      'post_parent'    => 947,
                      'order'          => 'ASC',
                      'orderby'        => 'menu_order'
                    );
                    $pages_ids = get_posts($args);
                    if (!empty($pages_ids)) {
                      $count = 1;
                      foreach ($pages_ids as $page) {
                        $page_id = $page->ID;
                        $page_slug = $page->post_name;
                        $phase_content = get_field('page_content', $page_id);
                        $selected = '';
                        if ($page_slug == $_GET['uid']) {
                          $selected = 'selected';
                          $phase_product_id = $phase_content['product'];
                        }
                    ?>
                                <option
                                    value="<?php echo ($phase_content['product']) ? $phase_content['product'] : $page_id; ?>"
                                    <?php echo $selected; ?>><?php echo get_the_title($page_id); ?></option>
                                <?php }
                    } ?>
                            </select>
                        </div>
                        <div class="field">
                            <label>Email Address</label>
                            <input type="email" name="email">
                        </div>
                        <div class="field">
                            <label>Phone Number</label>
                            <input type="number" name="number">
                        </div>
                        <div class="field date">
                            <label>Choose Date</label>
                            <input type="date" id="billing_date_time" name="date">
                        </div>
                        <?php if ($phase_product_id) : ?>
                        <div class="field time-slots time_slots">
                            <label>Choose Time Slot</label>
                            <div class="time-slots-content">
                                <?php
                      $product_obj     = wc_get_product($phase_product_id);
                      if (!empty($product_obj->get_available_variations())) {
                        $time_slots_html = '';
                        foreach ($product_obj->get_available_variations() as $variation_obj) {
                          // var_dump($variation_obj['attributes']);
                          $term = get_term_by('slug', $variation_obj['attributes']['attribute_pa_time-slots'], 'pa_time-slots');
                          $variation_id = $variation_obj['variation_id'];
                          $time_slots_html .= ' <div class="single_radio">
                                              <input type="radio" class="variation_option new" data-pid="' . $phase_product_id . '" data-variation_id="' . $variation_id . '" data-name="' . $term->name . '" id="' . $term->term_id . '" name="time-slots-radio" value="' . $term->slug . '" data-value="' . $variation_id . '" >
                                              <label for="' . $term->term_id . '">' . $term->name . '</label>
                                              </div>';
                        }
                      }
                      echo $time_slots_html;
                      ?>
                            </div>
                        </div>
                        <?php endif; ?>
                        <button class="btn btn-primary">Book a Site Tour</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php endif; ?>
    <div id="popup-min-cart" class="popup min_cart">
        <div class="popup__window min_cart">
            <button type="button" class="popup__close material-icons js-popup-closer">close</button>
            <div id="side_mini_cart_content" class="form-content">
                <?php echo mitch_get_cart_content(); //@ includes/cart-functions.php 
          ?>
            </div>
        </div>
    </div>

    <script src="<?php echo $theme_settings['theme_url']; ?>/assets/js/jquery-3.2.1.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="<?php echo $theme_settings['theme_url']; ?>/assets/js/main.js"></script>
    <!-- <script   src="<?php //echo get_stylesheet_directory_uri(); 
                        ?>/assets/js/aos.js"></script> -->
    <script>
// AOS.init();
if ($('body').hasClass('rtl')) {
    var is_ar = false;
} else {
    var is_ar = true;
}
    </script>
    <?php if (is_product_category()) : ?>
    <script>
jQuery(function($) {
    $(window).bind('load', function() {
        get_products_ajax_count();
    });
});
    </script>
    <?php endif; ?>
    <script src="<?php echo $theme_settings['theme_url']; ?>/assets/js/slick.min.js" defer></script>
    <?php if (is_singular('product')) : ?>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" type="text/javascript"></script> -->
    <!-- <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" /> -->
    <!-- <script defer src="https://c0.wp.com/c/5.4.4/wp-includes/js/jquery/ui/datepicker.min.js" ></script> -->
    <?php endif; ?>
    <script>
var mitch_ajax_url = '<?php echo admin_url('admin-ajax.php'); ?>';
    </script>
    <script src="<?php echo $theme_settings['theme_url']; ?>/backend_functions.js"></script>
    <script src="<?php echo $theme_settings['theme_url']; ?>/local/products_json.js"></script>

    <script type="text/javascript">
$(document).ready(function() {
    $('#gsearch').keyup(function() {
        var searchField = $(this).val();
        if (searchField === '') {
            $('#search_results').html('');
            $('#search_results').hide('slow');
            return;
        }
        $('#search_results').show('slow');
        var regex = new RegExp(searchField, "i");
        var output = '<div class="row">';
        var count = 1;

        $.each(data, function(key, val) {
            if ((val.sku.search(regex) != -1) || (val.product_name.search(regex) != -1)) {
                output += '<a href="' + val.product_url + '"><div class="col-md-6 well">';
                output +=
                    '<div class="col-md-3"><img class="img-responsive" width="80px" src="' + val
                    .product_image + '" alt="' + val.product_name + '" /></div>';
                output += '<div class="col-md-7">';
                output += '<h5>' + val.product_name + '</h5><h6>' + val.product_price + '</h6>';
                output += '</div>';
                output += '</div></a>';
                if (count % 2 == 0) {
                    output += '</div><div class="row">'
                }
                count++;
            }
        });
        if (count == 1) {
            output += '<div class="not_results">عفوا لا يوجد نتيجة!</div>';
        }
        output += '</div>';
        $('#search_results').html(output);
    });
});
var divToHide = document.getElementById('search_results');
if (divToHide) {
    document.onclick = function(e) {
        if (e.target.id !== 'search_results') {
            //element clicked wasn't the div; hide the div
            divToHide.style.display = 'none';
        }
    };
}
    </script>
    </body>

    </html>
    <?php if (is_checkout()) : ?>
    <script>
jQuery(function($) {

    jQuery(document).on('submit', '.checkout_coupon', function(e) {
        // Get the coupon code
        var code = jQuery('#coupon_code').val();
        var data = {
            coupon_code: code,
            security: '<?php echo wp_create_nonce("apply-coupon") ?>'
        };
        $.ajax({
            method: 'post',
            url: '/?wc-ajax=apply_coupon',
            data: data,
            success: function(data) {
                jQuery('.woocommerce-notices-wrapper').html(data);
                jQuery(document.body).trigger('update_checkout');
            }
        });
        e.preventDefault(); // prevent page from redirecting
    });
});
    </script>
    <?php wp_footer(); ?>
    <?php endif; ?>
    <?php //wp_footer();
    ?>
