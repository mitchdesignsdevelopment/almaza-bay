<?php
require_once 'header.php';
$faq_items = get_field('faq_items', get_the_id());
?>
<div id="page" class="site">
  <?php require_once 'theme-parts/main-menu.php';?>
  <!--start page-->
  <div class="site-content style_page_form">
    <div class="grid">
      <div class="page_faq">
        <div class="hero-section">
          <div class="section_title">
            <span><?php echo get_field('hero_title');?></span>
            <h1><?php echo get_field('hero_subtitle');?></h1>
          </div>
          <div class="section_image">
            <img src="<?php echo get_field('hero_image');?>" alt="ALMAZA-BAY" />
          </div>
        </div>
          <div class="faq_content">
            <?php if(!empty($faq_items)){  $count=1;?>
                <div class="nav_menu">
                  <h5>Table of Contents</h5>
                  <h3 class="nav_single_title all_faq active" >All</h3>

                  <?php  foreach($faq_items as $faq_item){ ?>
                      <a href="#single_faq_<?php echo $count; ?>" class="nav_single_title" ><?php echo $faq_item['title'];?></a>
                      <?php $count++;  } ?>
                </div>
            <?php } ?>

            <div class="section_faq">
              <?php if(!empty($faq_items)) { $count=1;
                    foreach($faq_items as $faq_item){ ?>
                  <div id="single_faq_<?php echo $count; ?>" class="single_faq active">
                      <!-- <h3 class="title_faq"><?php// echo $faq_item['title'];?></h3> -->
                      <?php if($faq_item['questions_&_answers']): foreach($faq_item['questions_&_answers'] as $qa):?>
                      <div class="content faq">
                        <h4 class="question">
                          <?php echo $qa['question'] ?>
                        </h4>
                        <p class="answer">
                          <?php echo $qa['answer'] ?>
                        </p>
                      </div>
                      <?php endforeach; endif;?>
                  </div>
              <?php $count++; } } ?>
            </div>
          </div>
      </div>
    </div>
  </div>
  <!--end page-->
</div>
<?php require_once 'footer.php';?>
