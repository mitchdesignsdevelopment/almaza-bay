<?php
require_once 'header.php';
mitch_validate_logged_in();
//repeat order process
mitch_repeat_order();
?>
<div id="page" class="site">
  <?php require_once 'theme-parts/main-menu.php';?>
  <!--start page-->
  <div class="site-content page_myaccount">
    <div class="grid">
      <div class="section_title">
          <h1><?php echo $fixed_string['myaccount_page_title'];?></h1>
      </div>
      <div class="page_content">
        <?php include_once 'theme-parts/myaccount-sidebar.php';?>
        <div class="dashbord">
        <?php
        if(!isset($_GET['order_id'])){
          ?>
          <div class="table_order">
          <?php
          $user_orders = mitch_get_myorders_list();
          if(!empty($user_orders)){
            ?>
            <table>
              <tr>
                <th><?php echo $fixed_string['myaccount_page_orders_orderno'];?></th>
                <th><?php echo $fixed_string['myaccount_page_orders_date'];?></th>
                <th><?php echo $fixed_string['myaccount_page_orders_status'];?></th>
                <th><?php echo $fixed_string['myaccount_page_orders_total'];?></th>
                <th></th>
              </tr>
              <?php
              foreach($user_orders as $order_obj){
                ?>
                <tr>
                  <td>#<?php echo $order_obj->get_id();?></td>
                  <td><?php echo $order_obj->get_date_created()->date("F j, Y");//, g:i:s A T?></td>
                  <td><?php echo $fixed_string['order_status_'.$order_obj->get_status()];?></td>
                  <td>
                    <?php echo $order_obj->get_total().' '.$theme_settings['current_currency'];?>
                    <?php echo $fixed_string['myaccount_page_orders_for'];?>
                    <?php echo count($order_obj->get_items());?>
                    <?php echo $fixed_string['myaccount_page_orders_items'];?>
                  </td>
                  <td class="table_action">
                    <a href="<?php echo home_url('my-account/orders-list/?order_id='.$order_obj->get_id().'');?>">
                    <button class="show" type="button"><?php echo $fixed_string['myaccount_page_orders_show'];?></button>
                    </a>
                    <?php if($order_obj->get_status() == 'processing'){ ?>
                    <a id="cancel_order_button" data-order-id="<?php echo $order_obj->get_id();?>" href="#popup-remove-order" class="remove js-popup-opener" ><?php echo $fixed_string['myaccount_page_orders_cancel'];?></a>
                    <?php }?>
                  </td>
                </tr>
                <?php
              }
              ?>
            </table>
            <?php
          }else{
            ?>
            <div class="alert emty_order_list">
              <p><?php echo $fixed_string['alert_myaccount_page_orders_empty'];?></p>
            </div>
            <?php
          }
          ?>
          </div>
          <?php
        }else{
          $order_id  = intval($_GET['order_id']);
          if(get_post_meta($order_id, '_customer_user', true) != get_current_user_id()){
            wp_redirect(home_url('my-account/orders-list'));
            exit;
          }
          $order_obj = wc_get_order($order_id);
          ?>
          <div class="order_expanded">
               <div class="section_title">
                   <h2 class="title"><?php echo $fixed_string['myaccount_page_orders_order_no2'];?> <?php echo $order_obj->get_id();?>
                       <p><?php echo $fixed_string['myaccount_page_orders_order_at'];?>
                         <?php echo $order_obj->get_date_created()->date("F j, Y");//, g:i:s A T?> <?php echo $fixed_string['myaccount_page_orders_status_now'];?>
                         <?php echo $fixed_string['order_status_'.$order_obj->get_status()];?></p>
                       </h2>
                   <p class="total">
                     <?php echo $fixed_string['myaccount_page_orders_total'];?>
                     <span>
                       <?php echo $order_obj->get_total().' '.$theme_settings['current_currency'];?>
                       <?php echo $fixed_string['myaccount_page_orders_for'];?>
                       <?php echo count($order_obj->get_items());?>
                       <?php echo $fixed_string['myaccount_page_orders_items'];?>
                     </span>
                   </p>
               </div>
               <div class="section_item">
                 <div class="item_list">
                   <?php
                   foreach($order_obj->get_items() as $key => $values){
                     $order_item_data = mitch_get_short_product_data($values['product_id']);
                     ?>
                     <div class="single_item">
                         <div class="sec_item">
                             <div class="img">
                                 <img style="width: 100px;height: 100px;" src="<?php echo $order_item_data['product_image'];?>" alt="<?php echo $order_item_data['product_title'];?>">
                             </div>
                             <div class="info">
                                 <div class="text">
                                     <h4><?php echo $order_item_data['product_title'];?></h4>
                                     <?php
                                     if(!empty($values['custom_cart_data'])){
                                       ?>
                                       <ul>
                                       <?php
                                       foreach($values['custom_cart_data']['attributes_vals'] as $attr_val){
                                         ?>
                                         <li><?php echo mitch_get_product_attribute_name($attr_val);?></li>
                                         <?php
                                       }
                                       ?>
                                       </ul>
                                     <?php }?>
                                 </div>
                                 <p><?php echo $values['line_total'] / $values['quantity'];?> <?php echo $theme_settings['current_currency'];?></p>
                             </div>
                         </div>
                         <p class="count"><?php echo $fixed_string['checkout_form_number'];?>: <?php echo $values['quantity'];?></p>
                         <p class="total_price"><?php echo $values['line_total'];?> <?php echo $theme_settings['current_currency'];?></p>
                     </div>
                     <?php
                   }
                   ?>
                 </div>
                  <form action="#" method="post">
                    <!-- <input type="hidden" name="action" value="repeat_order">
                    <input type="hidden" name="order_id" value="<?php //echo $order_obj->get_id();?>"> -->
                    <!-- <button type="submit">تكرار الطلب</button> -->
                    <a href="#popup-repeat-order" class="js-popup-opener">تكرار الطلب</a>
                 </form>
               </div>
               <div class="section_info">
                   <div class="info">
                       <h5><?php echo $fixed_string['myaccount_page_orders_mydetails'];?></h5>
                       <p><?php echo $order_obj->get_billing_first_name().' '.$order_obj->get_billing_last_name();?></p>
                       <p><?php echo $order_obj->get_billing_phone();?></p>
                       <p><?php echo $order_obj->get_billing_email();?></p>
                   </div>
                   <div class="info">
                       <h5><?php echo $fixed_string['thankyou_shipping'];?></h5>
                       <p><?php echo $order_obj->get_billing_address_1().', '.$order_obj->get_billing_city().', '.$order_obj->get_billing_country();?></p>
                   </div>
                   <div class="info">
                       <h5><?php echo $fixed_string['thankyou_payment'];?></h5>
                       <p><?php echo $fixed_string[$order_obj->get_payment_method_title()]; ?></p>
                   </div>
                   <div class="info total">
                       <h5><?php echo $fixed_string['checkout_form_total'];?></h5>
                       <p><?php echo $order_obj->get_total().' '.$theme_settings['current_currency'];?></p>
                   </div>
                   <div class="info notes">
                     <?php
                     $customer_comments = mitch_get_user_comments($order_obj->get_id());
                     $customer_notes    = get_post_meta($order_obj->get_id(), 'customer_notes', true);
                     if(!empty($customer_comments) || !empty($customer_notes)){
                       ?>
                       <h5><?php echo $fixed_string['checkout_form_order_notes'];?></h5>
                       <?php
                       if(!empty($customer_notes)){
                         echo '<p>'.$customer_notes.'</p>';
                       }
                       if(!empty($customer_comments)){
                         foreach($customer_comments as $comment_obj){
                           echo '<p>'.$comment_obj->comment_content.' -- <span class="comment-date">'.$comment_obj->comment_date.'</span></p>';
                         }
                       }
                     }
                     ?>
                   </div>
               </div>
          </div>
          <?php
        }
        ?>
        </div>
      </div>
    </div>
  </div>
  <!--end page-->
  <!-- <div id="overlay" class="overlay"></div> -->

</div>
<?php require_once 'footer.php';?>
<script>
$('#cancel_order_button').on('click', function () {
  var order_id     = parseInt($(this).data('order-id'));
  var proceed_url  = '<?php echo home_url('my-account/cancel-order/');?>?order_id='+order_id;
  var tracking_url = '<?php echo home_url('my-account/tracking-order/');?>?order_id='+order_id;
  $('#cancel_order_proceed_button').attr('href', proceed_url);
  $('#cancel_order_track_button').attr('href', tracking_url);
});
</script>
