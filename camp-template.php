<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package almaza
 * 
 * Template Name: Camp
 *
 */
require_once 'header.php'; global $post;
$page_content = get_field('single_camp');
//var_dump($post);?>
<div id="page" class="site">
  <?php require_once 'theme-parts/main-menu.php';?>
    <!--start page-->
    <div class="site-content camp_landing">
            <div class="section_hero">
                <a target="_blank" href="<?php echo $page_content['hero_section']['image_url'];?>" class="hero_image">
                    <img src="<?php echo $page_content['hero_section']['image'];?>" alt="">
                </a>
            </div>
            <div class="grid">
                <div class="section_sub_hero">
                    <div class="grid">
                        <img src="<?php echo $page_content['annual_section']['image'];?>" alt="">
                        <h3><?php echo $page_content['annual_section']['title'];?></h3>
                        <p><?php echo $page_content['annual_section']['description'];?></p>
                    </div>
                </div>
                <div class="video-box js-videoWrapper">
                    <div class="bg" style="background-image: url(<?php echo $page_content['annual_section']['video_thumbnail'];?>);"><button class="player js-videoPlayer"></button></div>
                    <div class="youtube-video">
                      <?php echo $page_content['annual_section']['video'];?>
                    </div>
                </div>
                <div class="section_pdf">
                    <p><?php echo $page_content['annual_section']['pdf_text'];?></p>
                    <a class="link" target="_blank" href="<?php echo $page_content['annual_section']['pdf_url'];?>">DOWNLOAD PDF</a>
                </div>
                <div class="section_info">
                    <div class="text">
                       <h3><?php echo $page_content['confidence_section']['title'];?></h3>
                        <?php echo $page_content['confidence_section']['description'];?>
                        <!-- <a class="link" href="<?php //echo $page_content['book_button']['url'];?>"><?php //echo $page_content['book_button']['title'];?></a> -->
                    </div>
                    <div class="img">
                        <!-- <div class="half_img">
                            <img src="./assets/img/half_03.png" alt="">
                            <img src="./assets/img/half_04.png" alt="">
                        </div>
                        <div class="full_img">
                            <img src="./assets/img/full_02.png" alt="">
                        </div> -->
                        <div class="all_img">
                            <?php if($page_content['confidence_section']['gallery']): foreach($page_content['confidence_section']['gallery'] as $image):?>
                                <img src="<?php echo $image;?>" alt="">
                            <?php endforeach; endif;?>
                        </div>
                      
                    </div>
                </div>
            </div>
            <?php include_once 'theme-parts/reviews-products.php';?>
            <div class="section_activity_program grid">
                <div class="section_title">
                    <p><?php echo $page_content['program_section']['title'];?></p>
                    <h3><?php echo $page_content['program_section']['subtitle'];?></h3>
                </div>
                <div class="section_table">
                    <table>
                        <tr>
                            <th></th>
                            <?php if($page_content['program_section']['table']['head']): foreach($page_content['program_section']['table']['head'] as $head):?>
                            <th><?php echo $head['day'];?></th>
                            <?php endforeach; endif;?>
                        </tr>
                        <?php if($page_content['program_section']['table']['rows']): foreach($page_content['program_section']['table']['rows'] as $row):?>
                        <tr>
                          <td><?php echo $row['time'];?></td>
                            <?php if($row['content']): foreach($row['content'] as $row_content):
                            ?>
                            <td>
                                <?php if( $row_content['target'] ): ?>
                                    <a <?php echo($row_content['target'])?'href="'.$row_content['target'].'"':'';?>>
                                        <?php echo $row_content['text'];?>
                                    </a>
                                <?php else: ?>
                                    <?php echo $row_content['text'];?>
                                <?php endif ?>
                        
                            </td>
                        <?php endforeach; endif;?>
                        </tr>
                        <?php endforeach; endif;?>
                    </table>   
                </div>
            </div>
            <!-- <div class="section_bk">
                <div class="grid">
                    <div class="section_title">
                        <h3><?php //echo $page_content['camp_dates_section']['title'];?></h3>
                        <p><?php //echo $page_content['camp_dates_section']['subtitle'];?></p>
                    </div>
                    <?php //$camp_product = $page_content['camp_product']; if($camp_product):?>
                    <div class="content_dates">
                    <?php
                    //$taxonomy = 'pa_weeks'; // The taxonomy
                    // $query_args = array(
                    //     'fields'  => 'names',
                    //     'orderby' => 'meta_value_num', 
                    //     'meta_query' => array( array(
                    //         'key' => 'order_' . $taxonomy, 
                    //         'type' => 'NUMERIC'
                    //      ) )
                    // );
                    
                    // $term_names = wp_get_post_terms( $camp_product, $taxonomy, $query_args );
                    // $term_names = wp_get_post_terms( $camp_product, $taxonomy );
                    // if($term_names):
                    //     foreach($term_names as $term):
                    ?>
                       <div class="single">
                            <div class="info">
                                <h5><?php //echo get_field('listing_title',$term);?></h5>
                                <p><?php //echo get_field('listing_subtitle',$term);?></p>
                                <a class="link" href="<?php //echo get_the_permalink($camp_product).'?attribute_pa_weeks='.$term->slug;?>">Book</a>
                            </div>
                       </div>
                    <?php //endforeach; endif;?>           
                    </div>
                    <?php //endif;?>
                </div>
            </div> -->
            <div class="grid">
                <div class="section_info row_reverse">
                    <div class="img">
                        <img src="<?php echo $page_content['sea_gift_section']['image'];?>" alt="">
                    </div>
                    <div class="text">
                        <h3><?php echo $page_content['sea_gift_section']['title'];?></h3>
                       <?php echo $page_content['sea_gift_section']['description'];?>
                        <!-- <a class="link" href="<?php //echo $page_content['book_button']['url'];?>"><?php //echo $page_content['book_button']['title'];?></a> -->
                    </div>
                </div>
                <div class="section_box">
                    <div class="section_title">
                        <h3><?php echo $page_content['activities_section']['title'];?></h3>
                        <p><?php echo $page_content['activities_section']['subtitle'];?></p>
                    </div>
                    <div class="all">
                        <?php if($page_content['activities_section']['activities']): $count=1; foreach($page_content['activities_section']['activities'] as $activity):?>
                        <div class="single" id="<?php echo $activity['id'];?>">
                            <a>
                                <div class="box">
                                    <div class="img">
                                        <img src="<?php echo $activity['image'];?>" alt="">
                                        <span><?php echo $activity['category'];?></span>
                                    </div>
                                    <div class="info">
                                        <p class="title"><?php echo $activity['title'];?></p>
                                        <p class="desc"><?php echo $activity['description'];?></p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <?php $count++; endforeach; endif;?>
                    </div>
                </div>
            </div>
            <div class="section_bk">
                <div class="grid">
                    <div class="section_title">
                        <h3><?php echo $page_content['unique_camps_section']['title'];?></h3>
                        <p><?php echo $page_content['unique_camps_section']['subtitle'];?></p>
                    </div>
                    <div class="content">
                    <?php if($page_content['unique_camps_section']['camps']): $count=1; foreach($page_content['unique_camps_section']['camps'] as $camp):?>
                       <div class="single">
                            <div class="box">
                                <div class="img">
                                <?php //echo $camp['url'];?>
                                    <img src="<?php echo $camp['image'];?>" alt="">
                                </div>
                                <div class="info">
                                    <h5><?php echo $camp['title'];?></h5>
                                    <p><?php echo $camp['description'];?></p>
                                </div>
                            </div>
                       </div>
                    <?php $count++; endforeach; endif;?>
                    </div>
                </div>
            </div>
            <div class="section_Brought_By grid">
                <?php if($page_content['sponsors_section']['sponsors']): $count=1; foreach($page_content['sponsors_section']['sponsors'] as $sponsor):?>
                    <div class="col">
                        <p><?php echo $sponsor['title'];?></p>
                        <div class="img">
                        <?php if($sponsor['gallery']): foreach($sponsor['gallery'] as $image):?>
                            <img src="<?php echo $image;?>" alt="">
                        <?php endforeach; endif;?>
                        </div>
                    </div>
                <?php $count++; endforeach; endif;?>
            </div>
        </div>
        <!--end page-->
</div>
<?php require_once 'footer.php';?>