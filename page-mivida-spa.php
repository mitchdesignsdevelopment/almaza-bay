<?php
require_once 'header.php';
$page_content = get_field('page_content');
?>
<div id="page" class="site">
  <?php require_once 'theme-parts/main-menu.php';?>
 <!--start page-->
 <div class="site-content mividia_spa">
            <div class="section_hero">
                <div class="hero_image">
                    <img src="<?php echo $page_content['hero_section']['image'];?>" alt="">
                    <div class="hero_text">
                        <p><?php echo $page_content['hero_section']['title'];?></p>
                        <h1><?php echo $page_content['hero_section']['subtitle'];?></h1>
                    </div>
                </div>
            </div>
            <div class="grid">
                <div class="section_sub_hero">
                    <h3><?php echo $page_content['escape_section']['title'];?></h3>
                    <p><?php echo $page_content['escape_section']['description'];?></p>
                </div>
                <div class="sec_list">
                    <div class="single_list">
                        <div class="text">
                            <h3><?php echo $page_content['reservation_section']['title'];?></h3>
                           <p><?php echo $page_content['reservation_section']['description'];?></p>
                           <div class="links">
                                <!-- <a href="<?php //echo $page_content['reservation_section']['reservation_button']['url'];?>" class="link reservation"><?php //echo $page_content['reservation_section']['reservation_button']['title'];?></a> -->
                                <a href="<?php echo $page_content['reservation_section']['call_button']['url'];?>" class="link call"><?php echo $page_content['reservation_section']['call_button']['title'];?></a>
                           </div>
                            
                        </div>
                        <div class="img ">
                          <img src="<?php echo $page_content['reservation_section']['image'];?>" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end page-->
</div>
<?php require_once 'footer.php';?>
