<?php
require_once 'header.php';
$page_content = get_field('home_page');
?>
<div id="page" class="site">
  <?php require_once 'theme-parts/main-menu.php';?>
  <!--start page-->
  <div class="site-content home">
        <div class="page_home">
            <div class="section_hero_home">
                <?php if($page_content['hero_section']['banners']): foreach($page_content['hero_section']['banners'] as $banner):?>
                    <div class="hero_image">
                        <img src="<?php echo $banner['banner'];?>" alt="">
                        <div class="hero_text">
                            <p><?php echo $banner['title1'];?></p>
                            <h1><?php echo $banner['title2'];?></h1>
                        </div>
                    </div>
                <?php endforeach;endif;?>
            </div>
            <div class="section_sub_hero">
                <div class="grid">
                    <h3><?php echo $page_content['hotels_section']['title'];?></h3>
                    <p><?php echo $page_content['hotels_section']['description'];?></p>
                </div>
            </div>
            <div class="section_phases">
                <div class="grid">
                    <div class="section_title">
                        <h3><?php echo $page_content['phases_section']['title'];?></h3>
                        <!-- <p><?php// echo $page_content['phases_section']['subtitle'];?></p> -->
                    </div>
                    <div class="all_phases">
                    <?php
                           $args = array(
                            'post_type'      => 'page',
                            'posts_per_page' => -1,
                            'post_status' => 'publish',
                            // 'fields' => 'ids',
                            'post_parent'    => 947,
                            'order'          => 'ASC',
                            'orderby'        => 'menu_order'
                        );
                        $pages_ids = get_posts($args);
                        ?>
                    <?php if($pages_ids): foreach($pages_ids as $page):
                        $phase_id = $page->ID;
                        $page_slug = $page->post_name
                        ?>
                        <div class="single_phases">
                            <a href="/phase?uid=<?php echo $page_slug;?>">
                                <div class="box">
                                    <!-- <img src="<?php //echo get_field('home_list_image',get_field('page_content',$phase_id)['product']);?>" alt=""> -->
                                    <img src="<?php echo get_field('page_content',$phase_id)['home_page_image'];?>" alt="">
                                    <div class="info">
                                        <p class="title_phases"><?php echo get_the_title($phase_id);?></p>
                                        <p class="position_phases"><?php echo 'EXPLORE';?></p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <?php endforeach;endif;?>
                    <!-- <div class="single_phases">
                            <a href="#popup-phase" class="link js-popup-opener" data-id="0">
                                    <div class="box">
                                    <img src="<?php // echo $theme_settings['theme_url'];?>/assets/img/Need-help.png" alt="Almaza Bay">
                                    <div class="info">
                                        <p class="title_phases">Need Help?</p>
                                        <p class="position_phases"><?php //echo 'Book A Site Tour';?></p>
                                    </div>
                                </div>
                            </a>
                        </div> -->
                    </div>
                </div>
            </div>
            <div class="section_Activities">
                <div class="grid">
                    <div class="side_left">
                        <div class="sec_image">
                            <img src="<?php echo $page_content['things_to_do_section']['image'];?>" alt="">
                        </div>
                        <div class="text sticky_text">
                            <span><?php echo $page_content['things_to_do_section']['title'];?></span>
                            <h3><?php echo $page_content['things_to_do_section']['subtitle'];?></h3>
                            <p><?php echo $page_content['things_to_do_section']['description'];?></p>
                        </div>
                    </div>
                    <div class="side_right">
                        <div class="all_activity">
                        <?php if($page_content['things_to_do_section']['widgets']): foreach($page_content['things_to_do_section']['widgets'] as $widget):?>
                            <div class="single_activity">
                                <a href="<?php echo $widget['button']['url'];?>" class="box">
                                    <div class="image">
                                        <img src="<?php echo $widget['image'];?>" alt="">
                                        <div class="title_activity">
                                            <h4><?php echo $widget['text'];?></h4>
                                        </div>
                                        
                                    </div>
                                    <div class="desc">
                                        <!-- <p><?php //echo $widget['description'];?></p> -->
                                        <span class="link"><?php echo $widget['button']['title'];?></span>
                                    </div>
                                </a>
                            </div>
                        <?php endforeach;endif;?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section_hotels">
                <div class="grid">
                    <div class="col_hotels">
                        <?php $number_of_cols = count( $page_content['hotels_section']['banners'] ); ?>
                        <div class="sec_img_hotel <?php echo ($number_of_cols > 1) ? 'sec_img_hotel_slick' : ''; ?>">
                            
                            <?php  if($page_content['hotels_section']['banners']): foreach($page_content['hotels_section']['banners'] as $banner):?>
                                <div class="item">
                                    <img src="<?php echo $banner['banner'];?>" alt="">
                                    <div class="hover_box">
                                        <div class="box">
                                            <h6><?php echo $banner['title1'];?></h6>
                                            <p><?php echo $banner['title2'];?></p>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; endif;?>
                        </div>
                        <?php if($page_content['hotels_section']['hotels']): ?>
                        <div class="sec_info_hotels">
                            <h4><?php echo $page_content['hotels_section']['hotels_title'];?></h4>
                            <p><?php echo $page_content['hotels_section']['hotels_subtitle'];?></p>
                            <ul>
                            <?php foreach($page_content['hotels_section']['hotels'] as $hotel_id):?>
                                <li>
                                    <a href="<?php echo get_the_permalink($hotel_id)?>"><?php echo get_the_title($hotel_id);?></a>
                                </li>
                            <?php endforeach;?>
                            </ul>
                        </div>
                        <?php endif;?>
                    </div>
                </div>
            </div>
            <?php if($page_content['dinning_section']['banners']):  $count_img = 1; 
            $number_of_cols = count( $page_content['dinning_section']['banners'] );
            ?>
            <div class="section_banner <?php echo ($number_of_cols > 1) ? 'section_banner_slick' : ''; ?>">
                <?php foreach($page_content['dinning_section']['banners'] as $banner):?>
                    <div class="single_banner">
                        <div class="sec_img">
                            <img src="<?php echo $banner['banner'];?>" alt="">
                        </div>
                        <div class="text">
                            <span><?php echo $banner['title'];?></span>
                            <h4><?php echo $banner['subtitle'];?></h4>
                            <!-- <p><?php //echo $banner['description'];?></p> -->
                            <a href="<?php echo $banner['button']['url'];?>"><?php echo $banner['button']['title'];?></a>
                        </div>
                    </div>
                <?php  endforeach;?>
              
            </div>
            <?php  $count_img++; endif;?>
            <div class="section_news">
                <div class="grid">
                    <div class="section_title">
                        <h3><?php echo $page_content['news_section']['title'];?></h3>
                        <p><?php echo $page_content['news_section']['subtitle'];?></p>
                    </div>
                    <div class="all_news">
                        <?php
                         $result = get_posts(array(
                            'post_type'=>'post',
                            'posts_per_page' => 2,
                            "orderby" =>"rand",
                            "order"=>"ASC",
                            'suppress_filters' => false,
                            'tax_query' => array(
                                'relation' => 'OR',
                                array(
                                    'taxonomy' => 'category',
                                    'field' => 'slug',
                                    'terms' => "news"
                                ),
                                array(
                                    'taxonomy' => 'category',
                                    'field' => 'slug',
                                    'terms' => "events"
                                )
                            ),
                            ));
                        if($result):
                            foreach($result as $post):
                                setup_postdata($post);
                        ?>
                        <div class="single_news">
                            <a href="<?php echo get_the_permalink();?>">
                                <div class="box">
                                    <div class="img">
                                        <img src="<?php echo get_the_post_thumbnail_url();?>" alt="">
                                        <span><?php echo get_the_terms(get_the_ID(),'category')[0]->name;?></span>
                                    </div>
                                    <div class="info">
                                        <p class="title_news"><?php echo get_the_title();?></p>
                                        <p class="date_news"><?php echo get_the_date('d F Y');?></p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <?php endforeach; wp_reset_postdata(); endif;?>
                    </div>
                </div>
            </div>
        </div>
  </div>
  <!--end page-->
</div>
<?php require_once 'footer.php';?>
