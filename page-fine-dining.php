<?php
require_once 'header.php';
$page_content = get_field('page_content');
?>
<div id="page" class="site">
  <?php require_once 'theme-parts/main-menu.php';?>
        <!--start page-->
        <div class="site-content fine_dining_list">
            <div class="section_hero">
                <div class="hero_image">
                    <img src="<?php echo $page_content['hero_section']['image'];?>" alt="">
                    <div class="hero_text">
                        <p><?php echo $page_content['hero_section']['title'];?></p>
                        <h1><?php echo $page_content['hero_section']['subtitle'];?></h1>
                    </div>
                </div>
            </div>
            <div class="grid">
                <div class="section_sub_hero">
                        <h3><?php echo $page_content['escape_section']['title'];?></h3>
                        <p><?php echo $page_content['escape_section']['description'];?></p>
                </div>
                <div class="sec_list">
                <?php if($page_content['dining_section']['places']): $count=1; foreach($page_content['dining_section']['places'] as $place):?>
                    <div class="single_list <?php echo($count%2==0)?'':'row_reverse';?>">
                        <div class="text">
                            <h3><?php echo $place['title'];?></h3>
                           <p><?php echo $place['description'];?></p>
                           <div class="links">
                               <?php if($place['reserve_button']['title']):?>
                                <a href="<?php echo $place['reserve_button']['url'];?>" class="link reservation"><?php echo $place['reserve_button']['title'];?></a>
                                <?php endif; ?>
                               <?php if($place['call_button']['title']):?>
                                <a href="<?php echo $place['call_button']['url'];?>" class="link call"><?php echo $place['call_button']['title'];?></a>
                                <?php endif; ?>
                           </div>   
                        </div>
                        <div class="img ">
                          <img src="<?php echo $place['image'];?>" alt="">
                        </div>
                    </div>
                <?php $count++; endforeach;endif;?>
                </div>
            </div>
            <div class="banner_fine_dining">
                <img src="<?php echo $page_content['banner_section']['banner'];?>" alt="">
                <div class="text">
                    <h4><?php echo $page_content['banner_section']['text'];?></h4>
                </div>
            </div>
        </div>
        <!--end page-->
<?php require_once 'footer.php';?>
