<?php
require_once 'header.php';
global $post;
$post_details = get_field('post_details', $post->ID);
?>
<div id="page" class="site">
  <?php require_once 'theme-parts/main-menu.php';?>
  <!--start page-->
  <div class="site-content blog">
    <div class="section_single_blog">
      <div class="grid">
          <div class="section_title">
            <ul class="breadcramb">
              <li>
                <a href="">All News</a>
              </li>
              <li>
                <a href="">/ <?php echo $post->post_title;?></a>
              </li>
            </ul>
            <!-- <p class="date"><?php //echo date('F j, Y', strtotime($post->post_date));?></p> -->
            <h2 class="title"><?php echo $post->post_title;?></h2>
          </div>
          <img src="<?php echo get_the_post_thumbnail_url($post_obj->ID,'full');?>" alt="<?php echo $post_obj->post_title;?>">
      </div>
      
        <div class="content_single_blog">
          <div class="grid">
            <div class="content">
              <div class="content">
                <?php echo $post_details['content_section_1'];?>
              </div>
              <img src="<?php echo $post_details['second_image'];?>" alt="">
              <div class="content">
                <?php echo $post_details['content_section_2'];?>
              </div>
              <div class="video-box js-videoWrapper">
                <div class="bg" style="background-image: url('<?php echo $post_details['video_image'];?>');"><button class="player js-videoPlayer"></button></div>
                <div class="youtube-video">
                  <iframe class="videoIframe js-videoIframe" src="<?php echo $post_details['video_url'];?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
              </div>
              <div class="content">
                <?php echo $post_details['content_section_3'];?>
              </div>
            </div>
          </div>
        </div>
    </div>
  </div>
  <!--end page-->
</div>
<?php require_once 'footer.php';?>
