<?php require_once 'header.php';?>
<?php
$items  = WC()->cart->get_cart();
if(empty($items)){
  wp_redirect(home_url());
  exit;
}
if(!empty(WC()->cart->applied_coupons)){
  $coupon_code    = WC()->cart->applied_coupons[0];
  $active         = 'active';
  $dis_form_style = 'display:block;';
  $dis_abtn_style = 'display:none;';
  $dis_rbtn_style = 'display:block;';
}else{
  $coupon_code    = '';
  $active         = '';
  $dis_form_style = '';
  $dis_abtn_style = 'display:block;';
  $dis_rbtn_style = 'display:none;';
}
// $new_order = wc_get_order(200);
// $new_order->calculate_totals();
// global $wpdb;
// $wpdb->query("DELETE FROM wp_woocommerce_order_itemmeta WHERE order_item_id = 39 AND (meta_key = '_line_subtotal' OR meta_key = '_line_total');");
?>
<div id="page" class="site">
  <?php require_once 'theme-parts/main-menu.php';?>
  <!--start page-->
  <div class="page_cart">
      <div class="cart">
          <div class="grid">
              <div class="section_title_cart">

                <h2>Your Cart <span class="count" id="cart_total_count"><?php echo WC()->cart->get_cart_contents_count();?></span> </h2>
                  <!-- <h2>
                    <p class="cart_subtitle">
                    <?php //echo $fixed_string['cart_sub_title'];?>
                    </p>
                    <?php //echo $fixed_string['cart_main_title'];?>
                  </h2> -->
              </div>
              <div class="section_cart">
                  <div class="cart_list">
                  <?php
                  $products_ids = array();
                  if(!empty($items)){
                    foreach($items as $item => $values){
                      $products_ids[]    = $values['product_id'];
                      $line_subtotal = number_format($values['line_subtotal'],2,'.',',');
                      $cart_product_data = mitch_get_short_product_data($values['product_id']);
                      ?>
                      <div id="cart_page_<?php echo $item;?>" class="single_item">
                          <div class="sec_item">
                              <div class="img">
                                  <img height="100px" src="<?php echo $cart_product_data['product_image'];?>" alt="<?php echo $cart_product_data['product_title'];?>">
                              </div>
                              <div class="info">
                                  <div class="text">
                                      <a class="title_link" href="<?php echo $cart_product_data['product_url'];?>"><h4><?php echo $cart_product_data['product_title'];?></h4></a>
                                      <?php if(!empty($values['billing_date_time'])){
                                        ?>
                                        <ul>
                                          <li data-date="<?php echo date("d-m-Y g:i A",strtotime($values['billing_date_time']));?>"><?php echo $values['billing_date_time'];?></li>
                                        </ul>
                                      <?php }?>
                                      <?php if(!empty($values['billing_unit_number'])){
                                        ?>
                                        <ul>
                                          <li data-date="<?php echo date("d-m-Y g:i A",strtotime($values['billing_unit_number']));?>"><?php echo $values['billing_unit_number'];?></li>
                                        </ul>
                                      <?php }?>
                                     <?php
                                      if(!empty($values['custom_cart_data'])){
                                        ?>
                                        <ul>
                                        <?php
                                        foreach($values['custom_cart_data']['attributes_vals'] as $attr_val){
                                          ?>
                                          <li><?php echo mitch_get_product_attribute_name($attr_val);?></li>
                                          <?php
                                        }
                                        ?>
                                        </ul>
                                      <?php }elseif(!empty($values['variation'])){
                                        ?>
                                        <ul>
                                        <?php
                                        foreach($values['variation'] as $key => $value){
                                          $tax = str_replace('attribute_','',$key);
                                          if(isset($values['variation']['attribute_pa_tennis-academy-groups']) && $values['variation']['attribute_pa_tennis-academy-groups']!=='private-sessions-adults-only' && $value=='1-on-1'){
                                          }
                                          else{
                                            ?>
                                          <li><?php echo get_term_by('slug',$value,$tax)->name;?></li>
                                            <?php
                                          }
                                        }
                                        ?>
                                        </ul>
                                      <?php }?>
                                      <a class="remove_page_cart" href="javascript:void(0);" onclick="cart_remove_item('<?php echo $item;?>', '');"><?php echo 'Remove';//$fixed_string['cart_delete_title'];?></a>
                                  </div>
                                  <p>
                                    <?php
                                    if(!empty($values['custom_cart_data'])){
                                      echo $values['line_subtotal'] / $values['quantity'];
                                    }else{
                                      echo $line_subtotal;
                                    }
                                    ?>
                                    <?php echo $theme_settings['current_currency'];?>
                                  </p>
                              </div>
                          </div>
                          <?php if(!isset($values['billing_date_time'])):?>
                          <!-- <div class="section_count">
                            <select name="quantity" id="quantity">
                              <option value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                              <option value="4">4</option>
                            </select> -->
                              <!-- <button class="increase" id="increase" onclick="increaseValueByID('number_<?php //echo $item;?>');update_cart_items('<?php //echo $item;?>', 'cart_page');" value="Increase Value"></button>
                              <input class="number_count" type="number" id="number_<?php //echo $item;?>" value="<?php //echo $values['quantity'];?>" />
                              <button class="decrease" id="decrease" onclick="decreaseValueByID('number_<?php //echo $item;?>');update_cart_items('<?php //echo $item;?>', 'cart_page');" value="Decrease Value"></button> -->
                          <!-- </div> -->
                          <?php endif;?>
                          <p class="total_price"><span id="line_subtotal_<?php echo $item;?>"><?php echo number_format($values['line_subtotal'],2,'.',',');?></span> <?php echo $theme_settings['current_currency'];?></p>
                      </div>
                      <?php
                    }
                  }
                  ?>
                  </div>
                 
                  <div class="cart_action">
                      <div class="left">
                        <div class="coupon_cart">
                            <h4 class="open-coupon <?php echo $active;?>">Do you have a promocode?<?php //echo $fixed_string['cart_promocode_title1'];?></h4>
                            <div class="discount-form <?php echo $active;?>" style="<?php echo $dis_form_style;?>">
                                <button class="close-coupon"><i class="material-icons">close</i></button>
                                <div class="coupon">
                                  <label for="coupon_code">Enter Promocode<?php // echo $fixed_string['cart_promocode_title2'];?></label>
                                  <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="<?php echo $coupon_code;?>" placeholder="<?php echo 'Code';//  $fixed_string['cart_promocode_title1'];?>" />
                                  <button style="<?php echo $dis_abtn_style;?>" id="apply_coupon" type="submit" class="button btn">
                                    <?php echo 'Apply';// echo $fixed_string['cart_promocode_button'];?>
                                    
                                  </button>
                                  <button style="<?php echo $dis_rbtn_style;?>" id="remove_coupon" type="submit" class="button btn">
                                    <?php echo 'Remove Coupon';//$fixed_string['cart_promocode_remove_button'];?>
                                    
                                  </button>
                                  <input type="hidden" name="lang" id="lang" value="">
                                </div>
                            </div>
                        </div>
                      </div>
                      <div class="right">
                        <div class="total_price">
                          <p>Total</p>
                          <div class="sec_price">
                            <span class="cart_total" id="cart_total"><?php echo number_format(WC()->cart->cart_contents_total,2,'.',',');?> <?php echo $theme_settings['current_currency'];?></span> 
                            <!-- <span id="cart_total" class="cart_total discount"><?php //echo WC()->cart->cart_contents_total;?> <?php //echo $theme_settings['current_currency'];?></span>  -->
                          </div>
                        
                        </div>
                        <a href="<?php echo home_url('checkout');?>">
                          <button type="button">Checkout</button>
                        </a>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <?php
      shuffle($products_ids);
      $product_id        = $products_ids[0];
      // mitch_test_vars(array($product_id));
      $new_related_title = $fixed_string['cart_related_products_title'];
      include_once 'theme-parts/related-products.php';
      ?>
  </div>
  <!--end page-->
</div>
<?php require_once 'footer.php';?>
