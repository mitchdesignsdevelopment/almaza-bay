<?php
require_once 'header.php';
$terms_items = get_field('terms_items', get_the_id());
?>
<div id="page" class="site" style="min-height: 1000px;">
  <?php require_once 'theme-parts/main-menu.php';?>
  <!--start page-->
  <div class="site-content style_page_form">
    <div class="grid">
      <div class="section_title">
        <img src="<?php echo $theme_settings['theme_url'];?>/assets/img/icons/almaza_icon.png" alt="" width="60">
        <h1><?php echo $fixed_string['terms_page_title'];?></h1>
      </div>
      <div class="section privacy_and_terms">
        <div class="content">
          <div class="page-content">
            <?php echo get_the_content();?>
          </div>
          <?php
          if(!empty($terms_items)){
            foreach($terms_items as $term_item){
              ?>
              <div class="min_box">
                <h3><?php echo $term_item['title'];?></h3>
                <div class="term-content">
                  <?php echo $term_item['content'];?>
                </div>
              </div>
              <?php
            }
          }
          ?>
        </div>
      </div>
    </div>
  </div>
  <!--end page-->
</div>
<?php require_once 'footer.php';?>
