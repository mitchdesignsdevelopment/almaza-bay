<?php
require_once 'header.php';
mitch_validate_logged_in();
?>
<div id="page" class="site" style="min-height: 1000px;">
  <?php require_once 'theme-parts/main-menu.php';?>
  <!--start page-->
  <div class="site-content page_myaccount">
    <div class="grid">
      <div class="section_title">
        <h1><?php echo $fixed_string['myaccount_page_title'];?></h1>
      </div>
      <div class="page_content">
        <?php include_once 'theme-parts/myaccount-sidebar.php';?>
        <div class="dashbord">
        <?php
        $order_id = intval($_GET['order_id']);
        if(get_post_status($order_id) == 'wc-cancelled' || get_post_meta($order_id, '_customer_user', true) != get_current_user_id()){
          wp_redirect(home_url('my-account/orders-list'));
          exit;
        }
        if(!empty($order_id)){
          if(!isset($_GET['cancel_done'])){
            ?>
            <div class="form_cancel">
              <div class="section_title">
                <h2><?php echo $fixed_string['myaccount_page_orders_cancel_p_tit'];?> <?php echo $order_id;?></h2>
                <p><?php echo $fixed_string['myaccount_page_orders_cancel_p_des'];?></p>
              </div>
              <form id="cancel_order_form" action="#" method="post">
                <div class="field">
                <label for=""><?php echo $fixed_string['checkout_form_phone'];?></label>
                <input type="text" name="phone_number" required>
                </div>
                <div class="field select_arrow">
                <label for=""><?php echo $fixed_string['myaccount_page_cancel_order_reason'];?></label>
                <select name="cancel_reason" required>
                  <option value=""><?php echo $fixed_string['myaccount_page_order_choose'];?></option>
                  <?php
                  $cancel_reasons = mitch_get_order_cancel_reasons();
                  if(!empty($cancel_reasons)){
                    foreach($cancel_reasons as $cancel_reason){
                      echo '<option value="'.$cancel_reason->post_title.'">'.$cancel_reason->post_title.'</option>';
                    }
                  }
                  ?>
                </select>
                </div>
                <input type="hidden" name="order_id" value="<?php echo $order_id;?>">
                <button type="submit" value=""><?php echo $fixed_string['myaccount_page_cancel_order_button'];?></button>
              </form>
              <div class="more_action">
                <a class="order expanded" href="<?php echo home_url('my-account/orders-list/?order_id='.$order_id);?>">
                  <?php echo $fixed_string['myaccount_page_order_details_show'];?>
                </a>
                <a class="order track" href="<?php echo home_url('tracking-order');?>">
                  <?php echo $fixed_string['myaccount_page_order_track_other'];?>
                </a>
              </div>
            </div>
            <?php
          }else{
            ?>
            <div class="form_cancel">
              <div class="section_title">
                <h3><?php echo $fixed_string['myaccount_page_order_cancel_done'];?> <?php echo $order_id;?></h3>
                <p><?php echo $fixed_string['myaccount_page_order_cancel_done_t'];?></p>
              </div>
              <!-- <p class="remove_order_done">BHG7687</p> -->
            </div>
            <?php
          }
        }else{
          ?>
          <div class="alert alert-danger"><?php echo $fixed_string['alert_global_error'];?></div>
          <?php
        }
        ?>
        </div>
      </div>
    </div>
  </div>
  <!--end page-->
</div>
<?php require_once 'footer.php';?>
