<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
?>

<?php

global $language;
if(isset($_GET['search'])){
    ?>
    <!-- <script>
    		jQuery(function($) {
			fbq('track', 'Search', {
				content_name: '<?php //the_title(); ?>',
				content_category:  <?php //echo json_encode($terms_names); ?>,
				content_ids: ['<?php //echo get_the_ID()?>'],
				content_type: 'product',
				value: <?php //echo json_encode($product->get_price()); ?>,
				currency: 'EGP'
			},
			{
				eventID: 'gc.3-'+'<?php //echo $event_id."-".get_the_ID(); ?>'
			}
			);
        });   
    </script> -->
    <?php
    global $wp_query;
    $s = urldecode($_GET['search']);
        $args = array(
            'posts_per_page'  => 100,
            'post_type'       => 'product',
            's'    => $s,
            'post_status'     => 'publish',
            'meta_key' => '_stock_status',
            'orderby' => 'meta_value',
            'order' => 'ASC',
            'tax_query'=>array(
                'relation' => 'AND',
                array(
                    'taxonomy' => 'product_visibility',
                    'field'    => 'name',
                    'terms'    => 'exclude-from-catalog',
                    'operator' => 'NOT IN',
                ),
            ),
        );
        $new_prods1 =  new WP_Query($args);
        $restOfProd = 100 - (int)$new_prods1->post_count;
        if($restOfProd>0){
            $args = array(
                'posts_per_page'  => $restOfProd,
                'post_type'       => 'product',
                'post_status'     => 'publish',
                'meta_key' => '_stock_status',
                'orderby' => 'meta_value',
                'order' => 'ASC',
                'meta_query' => array(
                    'relation'=>'OR',
                    array(
                        'key' => '_sku',
                        'value' => $s,
                        'compare' => 'LIKE'
                    ),
                ),
                'tax_query'=>array(
                    'relation' => 'AND',
                    array(
                        'taxonomy' => 'product_visibility',
                        'field'    => 'name',
                        'terms'    => 'exclude-from-catalog',
                        'operator' => 'NOT IN',
                    ),
                ),
            );
            $new_prods2 = new WP_Query($args);
            $new_prods1->post_count = $new_prods1->post_count + $new_prods2->post_count;
            $restOfProd = 100 - (int)$new_prods1->post_count;
            $new_prods1->posts = array_unique(array_merge($new_prods1->posts,$new_prods2->posts),SORT_REGULAR);
            if($restOfProd>0){
                $args = array(
                    'posts_per_page'  => $restOfProd,
                    'post_type'       => 'product',
                    'post_status'     => 'publish',
                    'meta_key' => '_stock_status',
                    'orderby' => 'meta_value',
                    'order' => 'ASC',
                    'meta_query' => array(
                        'relation'=>'OR',
                            array(
                            'key' => 'post_content',
                            'value' => $s, 
                            'compare' => 'LIKE'
                        ),
                    ),
                    'tax_query'=>array(
                        'relation' => 'AND',
                        array(
                            'taxonomy' => 'product_visibility',
                            'field'    => 'name',
                            'terms'    => 'exclude-from-catalog',
                            'operator' => 'NOT IN',
                        ),
                    ),
                );
                $new_prods3 = new WP_Query($args);
                $new_prods1->post_count = $new_prods1->post_count + $new_prods2->post_count + $new_prods3->post_count;
                $restOfProd = 100 - (int)$new_prods1->post_count;
                $new_prods1->posts = array_unique(array_merge($new_prods1->posts,$new_prods2->posts,$new_prods3->posts),SORT_REGULAR);
                if($restOfProd>0){
                    $args = array(
                        'posts_per_page'  => $restOfProd,
                        'post_type'       => 'product',
                        'post_status'     => 'publish',
                        'meta_key' => '_stock_status',
                        'orderby' => 'meta_value',
                        'order' => 'ASC',
                        'tax_query' => array(
                            'relation' => 'AND',
                                array(
                                    'taxonomy' => 'product_visibility',
                                    'field'    => 'name',
                                    'terms'    => 'exclude-from-catalog',
                                    'operator' => 'NOT IN',
                                ),                              
                                array(
                                'taxonomy' => 'brand',
                                'field' => 'name',
                                'terms' => array($s),
                                'compare' => 'LIKE',
                                ),
                            ),
                    );
                    $new_prods4 = new WP_Query($args);
                    $new_prods1->post_count = $new_prods1->post_count + $new_prods2->post_count + $new_prods3->post_count + $new_prods4->post_count;
                    $restOfProd = 100 - (int)$new_prods1->post_count;
                    $new_prods1->posts = array_unique(array_merge($new_prods1->posts,$new_prods2->posts,$new_prods3->posts,$new_prods4->posts),SORT_REGULAR);
                    if($restOfProd>0){
                        $args = array(
                            'posts_per_page'  => $restOfProd,
                            'post_type'       => 'product',
                            'post_status'     => 'publish',
                            'meta_key' => '_stock_status',
                            'orderby' => 'meta_value',
                            'order' => 'ASC',
                            'tax_query' => array(
                                'relation' => 'AND',
                                    array(
                                        'taxonomy' => 'product_visibility',
                                        'field'    => 'name',
                                        'terms'    => 'exclude-from-catalog',
                                        'operator' => 'NOT IN',
                                    ),
                                    array(
                                    'taxonomy' => 'product_tag',
                                    'field' => 'name',
                                    'terms' => array($s),
                                    'compare' => 'LIKE',
                                    ),
                                ),
                        );
                        $new_prods5 = new WP_Query($args);
                        $new_prods1->post_count = $new_prods1->post_count + $new_prods2->post_count + $new_prods3->post_count + $new_prods4->post_count + $new_prods5->post_count;
                        $restOfProd = 100 - (int)$new_prods1->post_count;
                        $new_prods1->posts = array_unique(array_merge($new_prods1->posts,$new_prods2->posts,$new_prods3->posts,$new_prods4->posts,$new_prods5->posts),SORT_REGULAR);
                        if($restOfProd>0){
                            $args = array(
                                'posts_per_page'  => $restOfProd,
                                'post_type'       => 'product',
                                'post_status'     => 'publish',
                                'meta_key' => '_stock_status',
								'orderby' => 'meta_value',
								'order' => 'ASC',
                                'meta_query' => array(
                                        array(
                                            'key' => 'page_title_en',
                                            'value' => $s, 
                                            'compare' => 'LIKE'
                                        ),
                                    ),
                                'tax_query'=>array(
                                    'relation' => 'AND',
                                    array(
                                        'taxonomy' => 'product_visibility',
                                        'field'    => 'name',
                                        'terms'    => 'exclude-from-catalog',
                                        'operator' => 'NOT IN',
                                    ),
                                ),
                            );
                            $new_prods6 = new WP_Query($args);
                            $new_prods1->post_count = $new_prods1->post_count + $new_prods2->post_count + $new_prods3->post_count + $new_prods4->post_count + $new_prods5->post_count + $new_prods6->post_count;
                            $restOfProd = 100 - (int)$new_prods1->post_count;
                            $new_prods1->posts = array_unique(array_merge($new_prods1->posts,$new_prods2->posts,$new_prods3->posts,$new_prods4->posts,$new_prods5->posts,$new_prods6->posts),SORT_REGULAR);
                            if($restOfProd>0){
                                $term_args = array('taxonomy' => 'brand');
                                    $terms = get_terms( $term_args );
                                    $term_ids = array();
                                    if ( ! empty( $terms ) && ! is_wp_error( $terms ) )
                                    {
                                        foreach( $terms as $term ) 
                                        {
                                            $val = get_term_meta( $term->term_id, 'en_title', true);
                                            if( strcasecmp($val, $s) == 0 ) {
                                                $term_ids[] = $term->term_id;
                                            }
                                        }
                                    }
                                $args = array(
                                    'posts_per_page'  => $restOfProd,
                                    'post_type'       => 'product',
                                    'post_status'     => 'publish',
                                    'meta_key' => '_stock_status',
                                    'orderby' => 'meta_value',
                                    'order' => 'ASC',
                                    'tax_query' => array(
                                        array(
                                        'taxonomy' => 'brand',
                                        'field'    => 'ID',
                                        'terms'    => $term_ids,
                                        'operator' => 'IN',
                                        ),
                                    ),
                                );
                                $new_prods7 = new WP_Query($args);
                                $new_prods1->post_count = $new_prods1->post_count + $new_prods2->post_count + $new_prods3->post_count + $new_prods4->post_count + $new_prods5->post_count + $new_prods6->post_count + $new_prods7->post_count;
                                $restOfProd = 100 - (int)$new_prods1->post_count;
                                $new_prods1->posts = array_unique(array_merge($new_prods1->posts,$new_prods2->posts,$new_prods3->posts,$new_prods4->posts,$new_prods5->posts,$new_prods6->posts,$new_prods7->posts),SORT_REGULAR);
                            }
                        }
                    }
                }
            }
        }
}
if(isset($_GET['search'])) {
    $products = $new_prods1;
    $allproducts = $products->post_count;
}
?>
<?php require_once 'header.php';?>
<div id="page" class="site">
  <?php require_once 'theme-parts/main-menu.php';?>
<?php if ( !$_GET['search'] ): ?>
  	<?php else : global $wp_query; ?>
<?php if ($products->post_count==0){ ?>
    <div class="grid">
        <div class="search-no-result">
            <p><?php echo ($language == 'en')? 'No Result' : 'لا توجد نتائج' ?></p>
        </div>
    </div>	
<?php
	}
endif;
?>

<div class="site-content page_list">
	<div class="grid">
		<div class="list_content search">
			<div class="product list">   
				<div class="grid">
                        <div class="page-intro section_title">
                          <h2><?php echo ($language == 'en')?'Search Results For: '.$_GET['search']:'بحث نتائج عن: '.$_GET['search']; ?></h1>
                        </div>
                        <div class="product_container">
                        <ul class="products-list products <?php echo (empty($_COOKIE['gridView']) )? 'big' :''?> <?php echo (isset($_COOKIE['gridView']) && $_COOKIE['gridView'] == 'small' )?'small':''?> <?php echo (isset($_COOKIE['gridView']) && $_COOKIE['gridView'] == 'big' )?'big':''?> <?php echo (isset($_COOKIE['gridView']) && $_COOKIE['gridView'] == 'large' )?'large':''?>" data-slug="<?php echo (is_shop())? '':$cat_slug; ?>" data-type="<?php echo (is_shop())?'shop':$term->taxonomy; ?>" data-count="20" data-page="1" data-posts="<?php echo $allproducts; ?>" data-search="<?php echo $_GET['search']?>" data-sort="<?php echo (isset($_GET['orderby']))? $_GET['orderby'] :'desc'; ?>" data-lang="<?php echo $language; ?>">
                            <?php
                            if($_GET['search']){
                                $products_query = $products;
                                if($products_query->have_posts()):
                                    while ($products_query->have_posts()) :
                                        $products_query->the_post(); 
                                        $product_data = mitch_get_short_product_data(get_the_ID());
                                        // include 'theme-parts/product-widget.php';
                                        // $product = wc_get_product( get_the_ID() ); 
                                    //     global $product;
                                    //     $args_param = array( 
                                    //         'type' => 'total-sales',
                                    //         'single' => 'yes',
                                    //     );
                                    wc_get_template( '../theme-parts/product-widget.php', $product_data);
                                    //wc_get_template_part( 'content', 'product' );
                                    endwhile; wp_reset_postdata(); endif;
                            }
                            ?>
                        </ul>
                        </div>

                    <?php 
                        //if($allproducts > $posts_per_page):
                    ?>
                    <!-- <a  href="#" class="load-more-products load-more"
                        data-slug="<?php echo (is_shop())? '':$cat_slug; ?>"
                        data-type="<?php echo (is_shop())?'shop':$term->taxonomy; ?>"
                        data-count="20" data-page="1" data-posts="<?php echo $allproducts; ?>"
                        data-sort="<?php echo (isset($_GET['orderby']))? $_GET['orderby'] :'date'; ?>"
                        style="display:none"><?php echo ($language == 'en')? 'Load More' : 'اعرض المزيد' ?></a>

                    <div class="spinner <?php echo (empty($_COOKIE['gridView']) )? 'big' :''?> <?php echo (isset($_COOKIE['gridView']) && $_COOKIE['gridView'] == 'small' )?'small':''?> <?php echo (isset($_COOKIE['gridView']) && $_COOKIE['gridView'] == 'big' )?'big':''?> <?php echo (isset($_COOKIE['gridView']) && $_COOKIE['gridView'] == 'large' )?'large':''?>" data-slug="<?php echo (is_shop())? '':$cat_slug; ?>" 
                        data-type="<?php echo (is_shop())?'shop':$term->taxonomy; ?>" 
                        data-count="20" data-page="1" data-posts="<?php echo $allproducts; ?>" 
                        data-sort="<?php echo (isset($_GET['orderby']))? $_GET['orderby'] :'date'; ?>">

                            <div class="widget">
                                <div class="image"></div>
                                <div class="content"></div>
                                <div class="content"></div>
                            </div>
                            <div class="widget">
                                <div class="image"></div>
                                <div class="content"></div>
                                <div class="content"></div>
                            </div>
                            <div class="widget">
                                <div class="image"></div>
                                <div class="content"></div>
                                <div class="content"></div>
                            </div>
                            <div class="widget">
                                <div class="image"></div>
                                <div class="content"></div>
                                <div class="content"></div>
                            </div>
                            <div class="widget">
                                <div class="image"></div>
                                <div class="content"></div>
                                <div class="content"></div>
                            </div>
                    </div> -->
                    <?php //endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<?php
	/**
	 * Hook: woocommerce_sidebar.
	 *
	 * @hooked woocommerce_get_sidebar - 10
	 */
	// do_action( 'woocommerce_sidebar' );

require_once 'footer.php';?>