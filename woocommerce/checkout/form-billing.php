<?php
/**
 * Checkout billing information form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-billing.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.9
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $language;
/** @global WC_Checkout $checkout */

?>
<div class="woocommerce-billing-fields">
	<?php do_action( 'woocommerce_before_checkout_billing_form', $checkout ); ?>
	<div class="woocommerce-billing-fields__field-wrapper<?php echo(is_user_logged_in())? ' user-logged-in' : ''; ?>">
		<div class="billing-fileds-container">
			<!-- <h2>Checkout As A Guest</h2> -->
			<h3><?php echo($language == '_en')? 'My Information' : 'Your Info'; ?></h3>
				<?php
					$fields = $checkout->get_checkout_fields( 'billing' );
					$mybillingfields=array(
						"billing_first_name",
						"billing_last_name",
						"billing_email",
						"billing_phone",
						// "billing_notes",
					);
					foreach ($mybillingfields as $key) {
						woocommerce_form_field( $key, $checkout->checkout_fields['billing'][$key], $checkout->get_value( $key ) );
					}
				?>
		</div>
	</div>

	<?php do_action( 'woocommerce_after_checkout_billing_form', $checkout ); ?>
</div>

