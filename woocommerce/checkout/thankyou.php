<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;
?>

<div class="woocommerce-order">

	<?php
	if ( $order ) :

		do_action( 'woocommerce_before_thankyou', $order->get_id() );
		?>

		<?php if ( $order->has_status( 'failed' ) ) : ?>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed"><?php esc_html_e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce' ); ?></p>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions">
				<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php esc_html_e( 'Pay', 'woocommerce' ); ?></a>
				<?php if ( is_user_logged_in() ) : ?>
					<a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay"><?php esc_html_e( 'My account', 'woocommerce' ); ?></a>
				<?php endif; ?>
			</p>

		<?php else : ?>
			<div class="thanks_page">
				<div class="left">
					<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received title"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', esc_html__( 'Thank you', 'woocommerce' ), $order ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>
					<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received subtitle"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', esc_html__( 'Your booking has been successfully placed', 'woocommerce' ), $order ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>
					<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received desc"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', esc_html__( 'If you have questions about your booking, send an email to info@almazabay.com with your booking number in the subject line or call 16160', 'woocommerce' ), $order ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>
					<div class="woocommerce-customer-details">
						<div class="row">
							<label for="">Your Name</label>
							<p><?php echo $order->get_billing_first_name().' '.$order->get_billing_last_name();?></p>
						</div>
						<div class="row">
							<label for="">E-mail Address</label>
							<p><?php echo $order->get_billing_email();?></p>
						</div>
						<div class="row">
							<label for="">Mobile Number</label>
							<p><?php echo $order->get_billing_phone();?></p>
						</div>
					</div>
					<?php //do_action( 'woocommerce_thankyou', $order->get_id() ); ?>
				</div>
				<div class="order_review-content">
					<div class="box-con sticky-box">
						<div id="order_review_thanks" class="woocommerce-checkout-review-order">
							<div class="order-title">
								<h3>BOOKING #<?php echo $order->get_order_number();?></h3>
								<?php if($order->get_payment_method() != 'cod'):?>
								<a>PAID</a>
								<?php endif;?>
							</div>
							<table class="shop_table woocommerce-checkout-review-order-table">
								<tbody>
									<tr class="cart_item">
									<?php $order_items = $order->get_items();
									foreach ( $order_items as $item_id => $item ) {
										$product = $item->get_product();
										$price = $product->get_price();
										$product_id = $item->get_product_id();
										$data = $item->get_data();
										// $order_items_data = array_map( );
										// $custom_field = get_post_meta( $product_id, '_tmcartepo_data', true); 
										// print_r($data);
										?>
										<td class="product-name">
											<div class="product-thumb-name">
												<a href="<?php echo get_the_permalink($product_id);?>" class="cart_pic"><img width="300" height="300" src="<?php echo get_the_post_thumbnail_url($product_id)?>" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" sizes="(max-width: 300px) 100vw, 300px"></a>
												<div class="product-name-container">
													<div class="right">
														<h3 class="product-title"><?php echo $product->get_name();?></h3>
														<?php
															if( $product->is_type('variation') ){
																// Get the variation attributes
															?>
															<dl class="variation">
															<?php
															$date_time_meta     = $item->get_meta('Date-Time',true);
															$variation_attributes = $product->get_variation_attributes();
															// Loop through each selected attributes
																foreach($variation_attributes as $attribute_taxonomy => $term_slug ){
																	// Get product attribute name or taxonomy
																	$taxonomy = str_replace('attribute_', '', $attribute_taxonomy );
																	// The label name from the product attribute
																	$attribute_name = wc_attribute_label( $taxonomy, $product );
																	// The term name (or value) from this attribute
																	print_r($term_slug);
																	echo $taxonomy; echo $term_slug;
																	if( taxonomy_exists($taxonomy) ) {
																		$attribute_value = get_term_by( 'slug', $term_slug, $taxonomy )->name;
																	} else {
																		$attribute_value = $term_slug; // For custom product attributes
																	}
																	?>
																	<dt class="variation-Fields"><?php echo $attribute_name;?>:</dt>
																		<dd class="variation-Fields"><p><?php echo $attribute_value;?></p>
																	</dd>
																	<?php
																}
															?>
														</dl>
														<?php
															}
														?>
													</div>
													<div class="left">
														<span class="woocommerce-Price-amount amount"><bdi><?php echo $price;?><span class="woocommerce-Price-currencySymbol">EGP</span></bdi></span>
													</div>
														<!-- <strong class="product-quantity">×&nbsp;1</strong> -->
												</div>
											</div>
										</td>
									<?php } ?>
										<!-- <td class="product-name">
											<div class="product-thumb-name">
												<a href="http://139.162.130.202:7000/product/almaza-bay-single-retreat-copy-copy/?attribute_pa_fields=football-field-2&amp;attribute_pa_time-slots=1000-am" class="cart_pic"><img width="300" height="300" src="http://139.162.130.202:7000/wp-content/uploads/2022/04/sport_03-300x300.png" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" loading="lazy" srcset="http://139.162.130.202:7000/wp-content/uploads/2022/04/sport_03-300x300.png 300w, http://139.162.130.202:7000/wp-content/uploads/2022/04/sport_03-150x150.png 150w, http://139.162.130.202:7000/wp-content/uploads/2022/04/sport_03-100x100.png 100w" sizes="(max-width: 300px) 100vw, 300px"></a>
												<div class="product-name-container">
													<div class="right">
														<h3 class="product-title">Almaza Bay Single Sport&nbsp;</h3>
														<dl class="variation">
															<dt class="variation-Fields">Fields:</dt>
																<dd class="variation-Fields"><p>Football Field 2</p>
															</dd>
															<dt class="variation-TimeSlots">Time Slots:</dt>
																<dd class="variation-TimeSlots"><p>10:00 AM</p>
															</dd>
														</dl>
													</div>
													<div class="left">
														<span class="woocommerce-Price-amount amount"><bdi>430,00&nbsp;<span class="woocommerce-Price-currencySymbol">EGP</span></bdi></span>
													</div>
														<strong class="product-quantity">×&nbsp;1</strong>
												</div>
											</div>
										</td> -->
									</tr>
								</tbody>

								<tfoot>
									<tr class="order-total">
										<th>Total</th>
										<td>
											<?php echo $order->get_formatted_order_total();?>
										</td>
									</tr>
								</tfoot>
							</table>
						</div>		
					</div>
				</div>
			</div>

	
			<!-- <ul class="woocommerce-order-overview woocommerce-thankyou-order-details order_details">

				<li class="woocommerce-order-overview__order order">
					<?php //esc_html_e( 'Order number:', 'woocommerce' ); ?>
					<strong><?php //echo $order->get_order_number(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
				</li>

				<li class="woocommerce-order-overview__date date">
					<?php //esc_html_e( 'Date:', 'woocommerce' ); ?>
					<strong><?php //echo wc_format_datetime( $order->get_date_created() ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
				</li>

				<?php //if ( is_user_logged_in() && $order->get_user_id() === get_current_user_id() && $order->get_billing_email() ) : ?>
					<li class="woocommerce-order-overview__email email">
						<?php //esc_html_e( 'Email:', 'woocommerce' ); ?>
						<strong><?php //echo $order->get_billing_email(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
					</li>
				<?php // endif; ?>

				<li class="woocommerce-order-overview__total total">
					<?php //esc_html_e( 'Total:', 'woocommerce' ); ?>
					<strong><?php //echo $order->get_formatted_order_total(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
				</li>

				<?php //if ( $order->get_payment_method_title() ) : ?>
					<li class="woocommerce-order-overview__payment-method method">
						<?php //esc_html_e( 'Payment method:', 'woocommerce' ); ?>
						<strong><?php //echo wp_kses_post( $order->get_payment_method_title() ); ?></strong>
					</li>
				<?php //endif; ?>

			</ul> -->

		<?php endif; ?>


	<?php else : ?>

		<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', esc_html__( 'Thank you. Your order has been received.', 'woocommerce' ), null ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>

	<?php endif; ?>

</div>


	
