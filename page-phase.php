<?php require_once 'header.php'; global $post; //var_dump($post);?>
<link rel='stylesheet' id='cf-front-css'  href='<?php echo $theme_settings['site_url'];?>/wp-content/plugins/caldera-forms/assets/build/css/caldera-forms-front.min.css?ver=1.9.6' type='text/css' media='all' />
<link rel='stylesheet' id='cf-render-css'  href='<?php echo $theme_settings['site_url'];?>/wp-content/plugins/caldera-forms/clients/render/build/style.min.css?ver=1.9.6' type='text/css' media='all' />
<div id="page" class="site">
  <?php require_once 'theme-parts/main-menu.php';?>
  <?php
  if(isset($_GET['uid'])):
    $new_str = str_replace(' ', '',$_GET['uid']);
    $args = array(
        'post_type'      => 'page',
        'posts_per_page' => 1,
        'post_status' => 'publish',
        'name' => $new_str,
        'post_parent'    => 947,
        'order'          => 'ASC',
        'orderby'        => 'menu_order'
    );

    $current_page = get_posts($args);
    $current_page = $current_page[0];
    $page_content = get_field('page_content',$current_page->ID);
  endif;
  $main_count=0; $count=1;
    ?>
        <!--start page-->
        <div class="site-content page_single_phases single_page" data-id="<?php echo $page_content['product'];?>">
            <div class="phases_content">
                <div class="nav_menu grid_phases">
                    <?php
                        $args = array(
                        'post_type'      => 'page',
                        'posts_per_page' => -1,
                        'post_status' => 'publish',
                        // 'fields' => 'ids',
                        'post_parent'    => 947,
                        'order'          => 'ASC',
                        'orderby'        => 'menu_order'
                    );
                    $pages_ids = get_posts($args);
                    if(!empty($pages_ids)){ $count=1;
                        foreach($pages_ids as $page){
                            $page_id = $page->ID;
                            $page_slug = $page->post_name;
                    ?>
                    <a href="#single_phases_<?php echo $page_id;?>" data-pid="<?php echo get_field('page_content',$page_id)['product'];?>" data-target="<?php echo $page_slug;?>" data-page="<?php echo $page_id;?>" class="nav_single_phases <?php echo($_GET['uid']==$page_slug)?'active':'';?>" ><?php echo get_the_title($page_id);?></a>
                    <?php } }?>
                </div>

                <div class="section_phases">
                    <?php
                        if(!empty($pages_ids)){ 
                        foreach($pages_ids as $page){
                            $page_id = $page->ID;
                            $page_slug = $page->post_name;
                            $page_field = get_field('page_content',$page_id);
                    ?>
                    <div id="single_phases_<?php echo $page_id;?>" data-main-ctr="<?php echo $main_count;?>" class="single_phases <?php echo($_GET['uid']==$page_slug)?'active':'';?>">
                        <div class="content phases">
                            <div class="section_title_hero grid_phases">
                                <img src="<?php echo $page_field['logo'];?>" alt="">
                                <h1><?php echo get_the_title($page_id);?></h1>
                                <span class="subtitle">Phase</span>
                                <a href="#popup-phase"  class="link js-popup-opener" data-id="<?php echo $page_id;?>">Book a Site Tour</a>
                            </div>
                            <?php $number_of_cols = count( $page_field['hero_gallery'] ); ?>
                            <div data-page="<?php echo $page_id;?>" class="hero_single_img <?php echo ($number_of_cols > 1) ? 'hero_single_img_slick hero_single_img_slick'.$page_id : ''; ?> grid_phases">
                                <?php if($page_field['hero_gallery']): foreach($page_field['hero_gallery'] as $image):?>
                                <img src="<?php echo $image;?>" alt="">
                                <?php endforeach; endif;?>
                            </div>
                            <div class="section_pdf">
                                <p class="pdf"><?php echo $page_field['pdf_text'];?></p>
                                <a class="link" target="_blank" href="<?php echo $page_field['pdf_url'];?>">DOWNLOAD PDF</a>
                            </div>

                            <div class="room_gallery">
                                <div class="section_title">
                                    <p><?php echo $page_field['rooms_title'];?></p>
                                    <h4><?php echo $page_field['rooms_subtitle'];?></h4>
                                </div>
                                <div class="room_content">
                                    <div class="nav_menu">
                                    <?php if($page_field['rooms']): $count=0; foreach($page_field['rooms'] as $room):?>
                                        <a href="#single_room_<?php echo $main_count;?>" id="single_room_ctr<?php echo $main_count;?>" class="nav_single_title <?php echo($count==0)?'active':'';?>" ><?php echo $room['title'];?></a>
                                    <?php $main_count++; $count++; endforeach; endif;?>
                                    </div>
                        
                                    <div class="section_room">
                                        <?php if($page_field['rooms']): $main_count = $main_count-count($page_field['rooms']); $count=1; foreach($page_field['rooms'] as $room):?>
                                            <div id="single_room_<?php echo $main_count;?>" class="single_room <?php echo($count==1)?'active':'';?>">
                                            <?php if($room['room_sections']): foreach($room['room_sections'] as $room_section):?>
                                                <div class="content room">
                                                    <h5><?php echo $room_section['title'];?></h5>
                                                    <div class="sec_room_gallery">
                                                    <?php if($room_section['gallery']): foreach($room_section['gallery'] as $image):?>
                                                        <img src="<?php echo $image;?>" alt="">
                                                    <?php endforeach;endif;?>
                                                    </div>
                                                    <div class="img">
                                                        <img src="<?php echo $room_section['room_image'];?>" alt="">
                                                    </div>
                                                </div>
                                              <?php endforeach;endif;?>
                                                <p class="pdf"><?php echo $page_field['pdf_text'];?></p>
                                                <a class="link" target="_blank" href="<?php echo $page_field['pdf_url'];?>">DOWNLOAD PDF</a>                                                  
                                            </div>  
                                        <?php $main_count++; $count++; endforeach;endif;?>
                                    </div>
                                </div>
                            </div>

                            <div class="section_hero_about bottom">
                                <div class="hero_image">
                                    <img src="<?php   echo $page_field['bottom_banner_section']['image']; ?>" alt="">
                                    <div class="hero_text">
                                        <p class="subtitle" ><?php echo $page_field['bottom_banner_section']['subtext']; ?></p>
                                        <h1 class="title"><?php echo $page_field['bottom_banner_section']['text']; ?></h1>
                                        <a href="#popup-phase"  class="link js-popup-opener" data-id="<?php echo $page_id;?>">Book a Site Tour</a>
                                        <!-- <a class="link_hero" href="<?php  //echo $page_field['bottom_banner_section']['button']['url']; ?>"><?php   //echo $page_field['bottom_banner_section']['button']['title']; ?></a> -->
                                    </div>
                                </div>
                            </div>
                            <div class="grid_phases gift-section">
                                <?php if($page_field['sea_gift_section']['sections']): $sec_count=1; foreach($page_field['sea_gift_section']['sections'] as $section):?>
                                    <div class="section_info <?php echo($sec_count%2==0)?'row_reverse':'';?>">
                                        <div class="text">
                                            <h3><?php echo $section['title'];?></h3>
                                            <p><?php echo $section['description'];?></p>
                                        </div>
                                        <div class="img">
                                            <img src="<?php echo $section['image'];?>" alt="">
                                        </div>
                                        
                                    </div>
                                <?php $sec_count++; endforeach;endif;?>
                            </div>

                        </div>
                    </div>
                    <?php } }?>
                </div>
            </div>
        </div>
        <!--end page-->
</div>
<?php require_once 'footer.php';?>


